<%@page session="false"%>
<!DOCTYPE HTML>
<html lang="it-IT">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="0; url=/MovieXStore_war_exploded/Dispatcher?controllerAction=AdminHomeManagement.adminHomeView">
    <script type="text/javascript">
        function onLoadHandler() {
            window.location.href = "/MovieXStore_war_exploded/Dispatcher?controllerAction=AdminHomeManagement.adminHomeView";
        }
        window.addEventListener("load", onLoadHandler);
    </script>
    <title>Page Redirection</title>
</head>
<body>
If you are not redirected automatically, follow the <a href='/MovieXStore_war_exploded/Dispatcher?controllerAction=AdminHomeManagement.adminHomeView'>link</a>
</body>
</html>
