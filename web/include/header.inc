


<script>
    function headerOnLoadHandler() {
        <%if(!loggedOn) { %>
        var usernameTextField = document.querySelector("#username");
        var usernameTextFieldMsg = "Lo username \xE8 obbligatorio.";
        var passwordTextField = document.querySelector("#password");
        var passwordTextFieldMsg = "La password \xE8 obbligatoria.";

        if (usernameTextField !== undefined && passwordTextField !== undefined) {
            usernameTextField.setCustomValidity(usernameTextFieldMsg);
            usernameTextField.addEventListener("change", function () {
                this.setCustomValidity(this.validity.valueMissing ? usernameTextFieldMsg : "");
            });
            passwordTextField.setCustomValidity(passwordTextFieldMsg);
            passwordTextField.addEventListener("change", function () {
                this.setCustomValidity(this.validity.valueMissing ? passwordTextFieldMsg : "");
            });
        }
        <% } %>
    }
</script>

<style>

    .dropdown {
        display: none;
        position: absolute;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 10;
        margin-top: 6px;
        border: 1px solid activecaption;
        border-top: 0px;

    }

    .dropdown a {
        background-color: black;
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .button_drop:hover .dropdown {
        display: grid;
    }

    .dropdown li {
        display: block;
        float: none;
    }

    #searchSelect {
        margin-right: -4px;
        /* border-radius: 4px; */
        height: 20px;
        margin-top: 2px;
        color: #3F51B5;
    }

    #adminButton {
        float: right;
        margin-top: 30px;
        margin-right: 50px;
        padding-left: 900px;
    }



</style>


<header>

    <div id="header-top" class="clearfix">

        <a href="Dispatcher">
            <img src="images/logo.png" alt="Homepage" height=170 width=170 id="logo">
        </a>


        <div id="adminButton">
            <a href="Dispatcher?controllerAction=AdminHomeManagement.adminHomeView"><button class="userbutton click">Admin Area</button></a>
        </div>

        <div id="header-top-right">


            <% if (!loggedOn) { %>

            <section id="login" class="clearfix">

                <form name="logonForm" action="Dispatcher" method="post">
                    <label for="username" class="label-login">Username</label>
                    <input type="text" id="username" name="username" maxlength="40" required="">
                    <label for="password" class="label-login" style="margin-left: 10px">Password</label>
                    <input type="password" id="password" name="password" maxlength="40" required="">
                    <input type="hidden" name="controllerAction" value="HomeManagement.logon">
                    <input type="submit" class="login_Botton" value="Log In">
                    <a href="Dispatcher?controllerAction=HomeManagement.subscribeView" class="login_Botton">Sign
                        In</a>
                </form>
            </section>

            <% } else { %>

            <section id="userinfo">
                <span id="userinfo_span"
                      style="font-size: 20px;text-transform:capitalize;padding-top: 10px;">Welcome <%=loggedUser.getFirstname()%> <%=loggedUser.getSurname()%></span>
                <a href="Dispatcher?controllerAction=InfoUserManagement.profileUser" class="userbutton click">
                    <i class="fa fa-user"></i><span>Change your info</span> </a>

                <a href="Dispatcher?controllerAction=HomeManagement.logout" class="userbutton click">
                    <i class="fa  fa-sign-out"></i><span>Logout <span style="visibility: hidden"> "..."</span></span>
                </a>
            </section>
            <div id="cart">

                <a href="Dispatcher?controllerAction=CartManagement.cartView">
                    <i class="fa fa-shopping-cart click icon">
                        <span id="cart_count"><%=loggedUser.getItemsNumberInCart()%>
                            <span style="font-variant: small-caps"><%=loggedUser.getItemsNumberInCart() == 1 ? "item" : "items"%></span></span>
                    </i>
                </a>
            </div>
            <% } %>


        </div>

    </div>

    <!--barra di navigazione -->
    <nav>
        <div id="sub-navigation">
            <ul>
                <li><a href="Dispatcher">HOME</a></li>
                <li class="button_drop"><a href="Dispatcher?controllerAction=CatalogManagement.catalogView">CATALOG</a>
                    <ul class="dropdown">
                        <li>
                            <a href="Dispatcher?controllerAction=CatalogManagement.catalogView&typeFilter=Movie">FILM</a>
                        </li>
                        <li><a href="Dispatcher?controllerAction=CatalogManagement.catalogView&typeFilter=TV series">TV
                            SERIES</a></li>
                        <li><a href="Dispatcher?controllerAction=CatalogManagement.catalogView&formatFilter=DVD">DVD</a>
                        </li>
                        <li><a href="Dispatcher?controllerAction=CatalogManagement.catalogView&formatFilter=Blu-ray">BLU-RAY</a>
                        </li>
                        <li><a href="Dispatcher?controllerAction=CatalogManagement.catalogView&formatFilter=UHD">4K</a>
                        </li>
                    </ul>
                </li>
                <%if(loggedOn) {%>
                <li><a href="Dispatcher?controllerAction=WishlistManagement.wishlistView"><i class="fa-heart fa"></i> WISHLIST</a></li>
                <li><a href="Dispatcher?controllerAction=InfoUserManagement.ordersInfoView">MY ORDERS</a></li>
                <% } %>
            </ul>
            <div id="search">
                <form action="Dispatcher" method="get" accept-charset="utf-8">
                    <select id="searchSelect" name="entityFilter">
                        <option>Homevideo</option>
                        <option>Work</option>
                    </select>
                    <label for="search-field" style="color: #ccc">SEARCH</label>
                    <input type="text" name="searchString" placeholder="Insert title,actor or director"
                           id="search-field" title="Enter search here" class="blink search-field">
                    <button type="submit" class="icon"><i class="fa fa-search click"></i></button>
                    <input type="hidden" name="controllerAction" value="CatalogManagement.catalogView">
                </form>
            </div>
        </div>
    </nav>
    <%@include file="popup.inc"%>
</header>

