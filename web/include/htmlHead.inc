<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MovieXStore</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<% String completedOperation = (String) request.getAttribute("completedOperationMessage"); %>

<script>
    var applicationMessage;
    <%if (applicationMessage != null) {%>
        applicationMessage="<%=applicationMessage%>";
    <%}%>
    var completedOperation;
    <%if(completedOperation!=null){ %>
    completedOperation = "<%=completedOperation%>";
    <% } %>
    function onLoadHandler() {
        headerOnLoadHandler();
        try { mainOnLoadHandler(); } catch (e) {}
        if (applicationMessage!==undefined) popup(applicationMessage);
     if (completedOperation!==undefined)
      showCompletedOperationPopup(completedOperation);
    }

    window.addEventListener("load", onLoadHandler);
</script>