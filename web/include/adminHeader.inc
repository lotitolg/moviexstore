<script>
    function headerOnLoadHandler() {
        <%if(!loggedOn) { %>
        var usernameTextField = document.querySelector("#username");
        var usernameTextFieldMsg = "Lo username \xE8 obbligatorio.";
        var passwordTextField = document.querySelector("#password");
        var passwordTextFieldMsg = "La password \xE8 obbligatoria.";

        if (usernameTextField !== undefined && passwordTextField !== undefined) {
            usernameTextField.setCustomValidity(usernameTextFieldMsg);
            usernameTextField.addEventListener("change", function () {
                this.setCustomValidity(this.validity.valueMissing ? usernameTextFieldMsg : "");
            });
            passwordTextField.setCustomValidity(passwordTextFieldMsg);
            passwordTextField.addEventListener("change", function () {
                this.setCustomValidity(this.validity.valueMissing ? passwordTextFieldMsg : "");
            });
        }
        <% } %>
    }
</script>

<style>

    .dropdown {
        display: none;
        position: absolute;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 10;
        margin-top: 6px;
        border: 1px solid activecaption;
        border-top: 0px;

    }

    .dropdown a {
        background-color: black;
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .button_drop:hover .dropdown {
        display: grid;
    }

    .dropdown li {
        display: block;
        float: none;
    }

    #adminButton {
        float: right;
        margin-top: 30px;
        margin-right: 50px;
        padding-left: 850px;
    }

    .box {
        float: none;
        width: auto;
    }

    .mytext {
        font-size: 20px;
        margin-top: 20px;
        margin-left: 10px;
    }

    .newButton {
        background-color: cadetblue;
    }

    .newButton:hover {
        color: #042293;
    }


</style>


<header>

    <div id="header-top" class="clearfix">

        <a href="Dispatcher">
            <img src="images/logo.png" alt="Homepage" height=170 width=170 id="logo">
        </a>


        <div id="adminButton">
            <a href="Dispatcher?controllerAction=HomeManagement.view">
                <button class="userbutton click">Customer Area</button>
            </a>
        </div>

        <div id="header-top-right">


            <% if (!loggedOn) { %>

            <section id="login" class="clearfix">

                <form name="logonForm" action="Dispatcher" method="post">
                    <label for="username" class="label-login">Username</label>
                    <input type="text" id="username" name="username" maxlength="40" required="">
                    <label for="password" class="label-login" style="margin-left: 10px">Password</label>
                    <input type="password" id="password" name="password" maxlength="40" required="">
                    <input type="hidden" name="controllerAction" value="AdminHomeManagement.logon">
                    <input type="submit" class="login_Botton" value="Log In">
                    <a href="Dispatcher?controllerAction=AdminHomeManagement.subscribeView" class="login_Botton">Sign
                        In</a>
                </form>
            </section>

            <% } else { %>

            <section id="userinfo">
                <span id="userinfo_span"
                      style="font-size: 20px;text-transform:capitalize;padding-top: 10px;">Admin <%=loggedAdmin.getFirstname()%> <%=loggedAdmin.getSurname()%></span>
                <a href="Dispatcher?controllerAction=AdminHomeManagement.modifyProfileView" class="userbutton click">
                    <i class="fa fa-user"></i><span>Change your info</span> </a>

                <a href="Dispatcher?controllerAction=AdminHomeManagement.logout" class="userbutton click">
                    <i class="fa  fa-sign-out"></i><span>Logout <span style="visibility: hidden"> "..."</span></span>
                </a>
            </section>
            <% } %>


        </div>

    </div>

    <!--barra di navigazione -->
    <nav>
        <div id="sub-navigation" class="clearfix">
            <ul>
                <li><a href="Dispatcher?controllerAction=AdminHomeManagement.adminHomeView">HOME</a></li>
                <%if (loggedOn) {%>
                <li class="button_drop"><a href="#">USERS MANAGEMENT</a>
                    <ul class="dropdown">
                        <li>
                            <a href="Dispatcher?controllerAction=AdminUserManagement.customerView">
                                CUSTOMERS MANAGEMENT</a>
                        </li>
                        <li>
                            <a href="Dispatcher?controllerAction=AdminUserManagement.adminView">
                                ADMINS MANAGEMENT</a>
                        </li>

                    </ul>
                </li>
                <li class="button_drop"><a href="#">CATALOG MANAGEMENT</a>
                    <ul class="dropdown">
                        <li>
                            <a href="Dispatcher?controllerAction=AdminCatalogManagement.HomevideoManagementView">
                                HOMEVIDEOS MANAGEMENT</a>
                        </li>
                        <li>
                            <a href="Dispatcher?controllerAction=AdminCatalogManagement.AWManagementView">
                                WORKS MANAGEMENT</a>
                        </li>
                        <li>
                            <a href="Dispatcher?controllerAction=AdminCatalogManagement.genreManagementView">
                                GENRES MANAGEMENT</a>
                        </li>
                    </ul>
                </li>
                <li><a href="Dispatcher?controllerAction=AdminCouponManagement.couponViews">COUPON MANAGEMENT</a></li>
                <li><a href="Dispatcher?controllerAction=AdminTrackingManagement.trackingView">TRACKING MANAGEMENT</a></li>
                <li><a href="Dispatcher?controllerAction=ConfigurationManagement.view">CONFIG</a></li>
                <% } %>
            </ul>
        </div>
    </nav>
    <%@include file="popup.inc" %>
</header>

