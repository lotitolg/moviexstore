
<script>

    function popup(applicationMessage) {
        var pop = document.getElementById("popupMessage");
        var contentPop = document.getElementById("contentPopupMessage");
        contentPop.textContent = applicationMessage;
        contentPop.style.display = 'block';
        pop.style.display = 'block';
    }

    function removePopup() {
        var pop = document.getElementById("popupMessage");
        pop.style.display = 'none';
    }
    function showCompletedOperationPopup(s) {
        var pop = document.getElementById("completedMessageDiv");
        var contentPop = document.getElementById("completedMessage");
        contentPop.textContent = s;
        pop.style.display = 'block';
    }

    function removeCompletedOperationPopup(event) {
        var pop = document.getElementById('completedMessageDiv');
        if (event.target != pop && event.target.parentNode != pop) {
            pop.style.display = 'none';
        }

    }

    window.addEventListener('mouseup', function (event) {
        var pop = document.getElementById('completedMessageDiv');
        if (event.target != pop || event.target === pop || event.target.parentNode == pop || event.target.parentNode == pop) {
            pop.style.display = 'none';
        }
    });

    window.addEventListener('mouseup', function (event) {
        var pop = document.getElementById('popupMessage');
        if (event.target != pop || event.target === pop || event.target.parentNode == pop || event.target.parentNode == pop) {
            pop.style.display = 'none';
        }
    });

</script>

<div id="popupMessage" class="popup">
    <h2><i class="fa fa-warning" style="margin-right: 10px"></i>Application Message</h2>
    <a class="close" href=javascript:removePopup()>&times;</a>
    <div id="contentPopupMessage" class="contentPopup"></div>
</div>

<div id="completedMessageDiv" class="popup">
    <i class="fa fa-check" style="color: green;font-size: 45px"></i>
    <span id="completedMessage" class="contentPopup"></span>
    <a class="close" href=javascript:removeCompletedOperationPopup()>&times;</a>
</div>