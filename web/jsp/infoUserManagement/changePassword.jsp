<%@page import="model.session.mo.LoggedUser"%>
<%@page import="model.mo.Customer"%>

<%-- 
    Document   : changePassword
    Created on : 12-lug-2019, 23.58.39
    Author     : simon70
--%>

<%@page session="false"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/include/htmlHead.inc"%>  

        <style>
            .aside_body_a ul li{

                margin: 5px;
            }

            .User_info_box{

                height: max-content;

            }

            #body_Info{

                margin: 20px;

            }


            .multi-field div{

                margin-bottom: 10px;
                display: inline-block;
                margin-right: 25px;
            }

            .multi-field div h3{

                color: #0066cc;
            }

            .aside_body_a li :hover{
                color: red;
            }

            .multi-field div input{

                font-size: 16px;
                border-radius: 10px;
                padding: 2px;
                margin-top: 6px;

            }

        </style>
        <script>
            var check = function () {
                if (document.getElementById('password').value ==
                    document.getElementById('confirm_password').value) {
                    document.getElementById('message').style.color = 'green';
                    document.getElementById('message').innerHTML = 'matching';
                    document.getElementById("confirm_Button").disabled = false;
                } else {
                    document.getElementById('message').style.color = 'red';
                    document.getElementById('message').innerHTML = 'not matching';
                    document.getElementById("confirm_Button").disabled = true;
                }
            }
        </script>

    </head>
    <body>
    <div class="portal">
        <%@include file="/include/header.inc"%>

        <%@include file="/include/aside.inc"%>

        <div id="principle" style="margin-top: 20px;border: 1px white solid;width: 73%;float: right;margin-right: 20px;">

            <div class="User_info_box portal">
                <div class="head">
                    <h3 style="">User Info</h3>
                    <p class="text-right" style=" padding-right: 20px;"></p>
                </div>

                <div id="body_Info" style=" overflow: hidden;">

                    <div class="multi-field" style="float:left">


                        <form id="PasswordChange" style=" display: none;" method="post" action="Dispatcher?controllerAction=InfoUserManagement.changePassword">
                            <%-- il contenuto e distribuito su tutto multi div della classe multi-field --%> 

                        </form>

                        <div style="">
                            <div class="name-field">

                                <h3 for="" class="">
                                    new Password:
                                </h3>

                                <input id="password" name="password" maxlength="50" id="" class="" type="password" form="PasswordChange" onkeyup='check();' style="" value="">

                            </div>

                            <div class="Lastname-field">

                                <h3  class="">Conferm Password:</h3>

                                <input id="confirm_password" name="confirmPassword" maxlength="50" id="lastname" type="password" form="PasswordChange" onkeyup='check();' style="" value="">
                                <span style="font-size:15px" id='message'></span>

                            </div>
                        </div>



                    </div>

                    <div style="float: right; margin-right: 30px">

                        <input class="login_Botton" id="confirm_Button" 
                               style="margin-top: 20px; margin-left: 0px;"
                               type="submit" value="Submit" form="PasswordChange"
                               >


                        <input class="login_Botton" 
                               style="margin-top: 20px; margin-left: 10px;"
                               type="reset" value="Cancel" form="PasswordChange"
                               >
                    </div>

                </div>

            </div>


        </div>


        <!--footer -->
        <%@include file="/include/footer.inc"%>

    </body>
    </div>
</html>


