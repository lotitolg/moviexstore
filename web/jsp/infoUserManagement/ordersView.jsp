<%@page import="model.mo.Order" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.mo.ProductInCart" %>
<%@page import="java.util.List" %>
<%@ page import="model.session.mo.LoggedUser" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Collections" %>

<%-- 
    Document   : ordersInfo
    Created on : 13-lug-2019, 19.54.03
    Author     : simon70
--%>

<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");


    List<Order> orderList = (ArrayList<Order>) request.getAttribute("orderList");


    int numberOfProducts;

    int iterator = orderList.size();

    for (Order order : orderList) {
        numberOfProducts = 0;
        for (ProductInCart productInCart : order.getCart().getProductInCart()) {
            numberOfProducts = numberOfProducts + productInCart.getItemQuantity();
        }
        order.setItemNumber(numberOfProducts);
    }
    Collections.reverse(orderList);
%>


<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <title>ordersInfo</title>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .User_info_box {

            height: max-content;

        }


        #body_Info {

            margin: 20px;

        }

        .OrdersDiv div {

            margin-bottom: 10px;
        }

        .OrdersDiv div > h3 {
            float: left;
            color: #0066cc;
        }

        .OrdersDiv div p {
            color: whitesmoke;
            font-size: 17px;
        }

        .aside_body_a li :hover {
            color: red;
        }

        .class1 {
            float: left;
            display: flex;
            margin-top: 20px;
            width: 50%;
        }


    </style>
</head>
<body>
<div class="portal clearfix">
    <%@include file="/include/header.inc" %>

    <%-- asid bar with all the modifications--%>

    <%@include file="/include/aside.inc" %>

    <div id="principle" style="margin-top: 20px;border: 1px white solid;width: 73%;float: right;margin-right: 20px;">

        <div class="OrdersDiv portal ">

            <div class="head">

                <h3 style="color:white">Orders</h3>

            </div>

            <%if(orderList.size()==0){%>
            <h1 style="margin: 20px;"> You haven't bought anything yet! &#128546;</h1>
            <%}%>

            <% for (Order order : orderList) { %>

            <div class="order" style="border-bottom:2px whitesmoke outset">

                <p style="margin-top:20px ;color: gold; text-align: center; font-size: 22px; font-weight: 900;">
                    Order <%=iterator%>
                </p>

                <div id="body_Info" style=" overflow: hidden;">


                    <div id="orderStatus">

                        <h3>Order status : </h3>
                        <%if (order.getOrderStatus().equals("In progress")) {%><p style="color:red"> In
                        progress</p><%}%>
                        <%if (order.getOrderStatus().equals("shipped")) {%><p style="color:gold"> shipped</p><%}%>
                        <%if (order.getOrderStatus().equals("delivered")) {%><p style="color:greenyellow">
                        delivered</p><%}%>

                    </div>

                    <div id="orderDate">
                        <h3>Order Date : </h3>
                        <p><%=order.getOrderDate()%>
                        </p>
                    </div>

                    <%if (order.getShippingDate() != null) {%>
                    <div id="shippingDate">
                        <h3>Shipping Date : </h3>
                        <p><%=order.getShippingDate()%>
                        </p>
                    </div>
                    <%}%>

                    <%if (order.getDeliveryDate() != null) {%>
                    <div id="deliveryDate">
                        <h3>Delivery Date : </h3>
                        <p><%=order.getDeliveryDate()%>
                        </p>
                    </div>
                    <%}%>

                    <%if (order.getCoupon().getCouponId() != 0) {%>
                    <div id="orderCoupon">
                        <h3> Coupon Discount used: </h3>
                        <p><%=order.getCoupon().getDiscountPercent() + "%"%>
                        </p>
                    </div>
                    <%}%>

                    <div id="orderShippingPrice">

                        <h3>Shipping Price :</h3>
                        <p><%=order.getShippingPrice()%>€</p>

                    </div>

                    <div id="orderTotalPrice">

                        <h3>Total Price : </h3>
                        <p><%=order.getTotalSpending()%>€</p>

                    </div>


                    <div id="orderTotalAndShppingPrice">

                        <h3>Total+ shipping: </h3>
                        <p><%=Math.round((order.getTotalSpending() + order.getShippingPrice())*100d)/100d%>€</p>

                    </div>

                    <div id="NumberOfProducts" style="border-bottom: 1px white outset; padding-bottom: 10px">

                        <h3>Number Of Products : </h3>
                        <p><%=order.getItemNumber()%>
                        </p>

                    </div>

                    <% for (ProductInCart productInCart : order.getCart().getProductInCart()) {%>

                    <%--_________ Movie ____________--%>
                    <div class="classgen clearfix " style="border-bottom: 0.5px white dotted;border-radius: 10px;">

                        <a href="Dispatcher?controllerAction=CatalogManagement.productView&productId=<%=productInCart.getHomevideoProducts().getProductId()%>">
                            <img style="clear: left;float:left; margin :10px ; width: 100px"
                                 src="images/<%=productInCart.getHomevideoProducts().getPicture_url()%> ">
                        </a>

                        <div class="class1">
                            <div class="class2" style=" margin: 20px;">
                                <p class="" style="text-align: left; margin-bottom: 5px"><span
                                        style="color:#0066cc; font-weight: 900;">Name: </span><%=productInCart.getHomevideoProducts().getName()%>
                                </p>
                                <label style="text-align: left; margin: 10px;font-size: 14px;"><h3
                                        style="color:#0066cc; float: left ">
                                    Quantity:</h3> <%=productInCart.getItemQuantity()%>
                                </label>
                            </div>
                        </div>

                        <div class="price" style="float: right; margin: 60px;">
                            <h3 style="color: greenyellow">Current price:</h3>
                            <p>€ <%=(productInCart.getHomevideoProducts().getPrice())%></p>

                        </div>


                    </div>

                    <%}%>

                </div>
            </div>
            <%
                    iterator--;
                }%>
        </div>
    </div>
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>
