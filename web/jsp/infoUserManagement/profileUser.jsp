<%@page import="model.mo.Customer" %>
<%@page import="model.mo.DeliveryAddress" %>
<%@ page import="model.session.mo.LoggedUser" %>
<%@ page import="model.mo.BillingInfo" %>

<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");

    DeliveryAddress deliveryAddress = (DeliveryAddress) request.getAttribute("deliveryAddress");
    Customer customer = (Customer) request.getAttribute("customer");
    BillingInfo billingInfo = (BillingInfo) request.getAttribute("billingInfo");
    String object = (String) request.getAttribute("object");

    String action = "modify";

    if (object.equals("Address") && deliveryAddress == null)
        action = "insert";
    else if (object.equals("Billing") && billingInfo == null)
        action = "insert";
%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .User_info_box {

            height: max-content;

        }

        #body_Info {

            margin: 20px;

        }

        .multi-field div {

            margin-bottom: 10px;
            display: inline-grid;
            margin-right: 25px;
        }

        .multi-field div label {

            color: #0066cc;
            font-size: 15px;
        }

        .aside_body_a li :hover {
            color: red;
        }

        .multi-field div input {

            font-size: 16px;
            border-radius: 10px;
            padding: 2px;
            margin-top: 6px;

        }

        #location-country {
            border: 0px white solid;
            border-radius: 7px;
            font-size: 14px;
            margin-top: 6px;
        }

    </style>


</head>
<body>
<div class="portal">
    <%@include file="/include/header.inc" %>

    <%-- asid bar with all the modifications--%>

    <%@include file="/include/aside.inc" %>

    <%-- _________User Info Setup______ --%>

    <div id="principle" style="margin-top: 20px;border: 1px white solid;width: 73%;float: right;margin-right: 20px;">

        <div class="User_info_box portal">

            <div class="head">
                <h3 style="">
                    <%if (object.equals("Customer")) {%>
                    User Info
                    <%}%>
                    <%if (object.equals("Address")) {%>
                    Delivery Address Info
                    <%}%>
                    <%if (object.equals("Billing")) {%>
                    Billing Info
                    <%}%>
                </h3>
                <p class="text-right" style=" padding-right: 20px;"></p>
            </div>

            <div id="body_Info" style=" overflow: hidden;">

                <div id="userpic">
                    <button style="border: 0px white;border-radius: 100px;margin-bottom: 15px;margin-top: 10px;">
                        <img style="" src="images/userpic.png" class="profile-photo" height="152" width="152">
                    </button>
                </div>


                <div class="multi-field">

                    <form id="InfoUser" style=" display: none;" method="post" action="Dispatcher">
                        <input type="hidden" name="controllerAction"
                               value="InfoUserManagement.<%=action%><%=object%>">
                    </form>

                    <%if (object.equals("Customer")) {%>


                    <input type="hidden" name="CustomerId" value="<%=customer.getCustomerId()%>" form="InfoUser">

                    <div class="field">
                        <label for="firstname">First Name:</label>
                        <input name="firstName" maxlength="50" id="firstname" class="" type="text" form="InfoUser"
                              required
                               value="<%=customer.getFirstname()%>">
                    </div>

                    <div class="field">
                        <label for="lastname">Last Name:</label>
                        <input name="surname" maxlength="50" id="lastname" type="text" form="InfoUser" required
                               value="<%=customer.getSurname()%>">
                    </div>

                    <div class="field">
                        <label for="username">Username:</label>
                        <input name="username" readonly maxlength="50" id="username" class="" type="text"
                               form="InfoUser"
                               required value="<%=customer.getUsername()%>">
                    </div>

                    <div class="field">
                        <label for="email"> Email:</label>
                        <input name="email" maxlength="50" id="email" class="" type="email" form="InfoUser"
                               value="<%=customer.getEmail()%>">
                    </div>

                    <%} else if (object.equals("Address")) {%>

                    <% if (action.equals("insert")) { %>

                    <p style="color: red;font-size: 18px">Insert a new Delivery Address before purchase!</p>
                    <%} else {%>
                    <input type="hidden" name="AddressId" value="<%=deliveryAddress.getAddressId()%>" form="InfoUser">
                    <%}%>

                    <%-- _____country_____ --%>

                    <div class="field">
                        <label for="country">Country/Region:</label>
                        <input name="country" maxlength="50" id="country" type="text" form="InfoUser" required
                               value="<%=(action.equals("modify"))? deliveryAddress.getCountry() : ""%>">
                    </div>

                    <div class="field">
                        <label for="city"> City:</label>
                        <input name="city" id="city" class="" type="text" form="InfoUser" required
                               value="<%=(action.equals("modify"))?deliveryAddress.getCity():""%>">
                    </div>

                    <div class="field">
                        <label for="location-zipcode" class="">ZIP code:</label>
                        <input name="postalcode" maxlength="8" id="location-zipcode" class="" type="text"
                               form="InfoUser" required
                               value="<%=(action.equals("modify"))?deliveryAddress.getPostalcode():""%>">
                    </div>

                    <div class="Delivery_Address">
                        <label for="deliveryAddress">Delivery Address:</label>
                        <input name="address" maxlength="60" id="deliveryAddress" type="text" form="InfoUser" required
                               value="<%=(action.equals("modify"))?deliveryAddress.getAddress():""%>">
                    </div>
                    <%} else if (object.equals("Billing")) {%>

                    <% if (action.equals("insert")) { %>
                    <p style="color: red;font-size: 18px">Insert Billing Information before purchase!</p>
                    <%} else {%>
                    <input type="hidden" name="BillingId" value="<%=billingInfo.getBillingId()%>" form="InfoUser">
                    <%}%>

                    <div class="field">
                        <label for="card_num">Card number:</label>
                        <input id="card_num" name="card_num" maxlength="19" minlength="8" type="text"
                               form="InfoUser" required
                               placeholder="card ends with:<%=(action.equals("modify"))? " " + billingInfo.getCardNum():""%>">
                    </div>

                    <div class="field">
                        <label for="cardexpdate">Card expiration date:</label>
                        <input id="cardexpdate" name="cardexpdate" id="cardexpdate" type="date" form="InfoUser"
                               required
                               style="" value="<%=(action.equals("modify"))? billingInfo.getCardexpdate():""%>">
                    </div>

                    <div class="field">
                        <label for="cardNetwork">Card network :</label>
                        <select id="cardNetwork" name="cardNetwork" form="InfoUser"
                                style="font-size: 16px;border-radius: 10px;padding: 2px; margin-top: 6px;">
                            <option value="Mastercard"
                                    <%=(action.equals("modify") && billingInfo.getNetworkCard().equals("Mastercard")) ? "selected" : ""%>>
                                Master
                            </option>
                            <option value="Visa" <%=(action.equals("modify") && billingInfo.getNetworkCard().equals("Visa")) ? "selected" : ""%>>
                                Visa
                            </option>
                        </select>
                    </div>
                    <%}%>


                </div>

                <div style="float: right">
                    <input class="login_Botton"
                           style="margin-top: 20px; margin-left: 0px;"
                           type="submit" value="Submit" form="InfoUser">
                    <input class="login_Botton"
                           style="margin-top: 20px; margin-left: 0px;"
                           type="reset" value="Cancel" form="InfoUser">
                </div>

            </div>

        </div>

    </div>

    <!--footer -->
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>
