
<%@page import="java.util.List"%>
<%@ page import="model.mo.HomevideoProduct" %>
<%@ page import="model.session.mo.LoggedAdmin" %>
<%@ page import="model.mo.AudiovisionWork" %>
<%--
  Created by IntelliJ IDEA.
  User: gigil
  Date: 04/07/2019
  Time: 21:24
  To change this template use File | Settings | File Templates.
--%>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");


    /*Filters*/
    String orderFilter = (String) request.getAttribute("orderFilter");
    boolean descendant  = (boolean) request.getAttribute("descendant");

    /*searchString*/
    String searchString = (String)request.getAttribute("searchString");

    /*Result*/
    String entity;
    List<HomevideoProduct> products = (List<HomevideoProduct>) request.getAttribute("products");
    List<AudiovisionWork> works = (List<AudiovisionWork>) request.getAttribute("works");
    entity = products!=null ? "Homevideo" : "AW";

    /*Page*/
    Integer thisPage = (Integer) request.getAttribute("thisPage");
    Boolean isThisLastPage = (Boolean)request.getAttribute("isThisLastPage");
    final int multipleView = (int)request.getAttribute("multipleView");


%>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <style>
        .box-movie{
            margin-top:80px;
            margin-left:30px;
        }

        .box-check > p{
            text-transform: uppercase;
        }
        .box-check > label{
            display: block;
            margin: 20px;
            font-size: 20px;
        }
        .name-check{
            margin-left: 18px;
        }

        .custom-select{
            width: 315px;
            float: right;
            margin-top: 20px
        }

        .custom-select select{
            width: 100px;
            margin-top:6px;
            display: inline;
        }

        .pageButton{
            float: left;
            margin-top: 30px;
            margin-left: 25px;
        }
        .pagnum{
            font-size:14px;
            margin-left:10px;
        }

        #search .search-field {
            height:21px;
        }


        #search{
            float: left;
            margin-top: 33px;
            border: 1px white outset;
            margin-left: 35px;
            padding: 5px;
            border-radius: 13px;
            background-color: #690101;
            width:auto;
        }

        #newButtonDiv{
            margin-left: -15px;
            padding-left: 51px;
            float: left;
            margin-top: 26px;
        }

        .cancelIcon{
            margin-left:155px;
        }

    </style>

    <script>
        function changeFilter() {
            var inp = document.catalogform;
            var sel = document.getElementById('orderSelect');
            var descendant = document.getElementById("descendant");

            inp.orderFilter.value=sel.value;
            inp.descendant.value=descendant.checked;
            inp.thisPage.value="1";
            inp.submit();
        }
        function precpagef() {
            var inp = document.catalogform;
            inp.thisPage.value="<%=thisPage-1%>";
            inp.submit();
        }
        function nextpagef() {
            var inp = document.catalogform;
            inp.thisPage.value="<%=thisPage+1%>";
            inp.submit();
        }
        function  cancelsearchString() {
            var inp = document.catalogform;
            var searchString = document.getElementById("searchString");
            inp.removeChild(searchString);
            inp.thisPage.value="1";
            inp.submit();
        }

        function insert() {
            var inp = document.insertForm;
            inp.submit();
        }
        function remove(Id) {
            var inp = document.modifyAndDeleteForm;
            if(confirm("Are you sure you want to delete it? <%=entity.equals("AW") ?
            "This action will also remove the Homevideo Products related to this Work" : ""%>")) {
                inp.Id.value = Id;
                inp.controllerAction.value = 'AdminCatalogManagement.delete<%=entity%>';
                inp.submit();
            }
        }

        function modify(Id) {
            var inp = document.modifyAndDeleteForm;
            inp.Id.value=Id;
            inp.controllerAction.value='AdminCatalogManagement.modify<%=entity%>View';
            inp.submit();
        }

        function mainOnLoadHandler() {
            var i;
            var sel = document.getElementById("orderSelect");
            var descendant = document.getElementById("descendant");

            for(i=0;i<sel.options.length;i++){
                if(sel.options[i].value==='<%=orderFilter%>')
                    sel.selectedIndex=i;
            }
            descendant.checked=<%=descendant%>;

            sel.addEventListener("change",changeFilter);
            descendant.addEventListener("click",changeFilter);

            var precpage = document.getElementsByClassName("precpage");
            var nextpage = document.getElementsByClassName("nextpage");

            <%if((((entity.equals("Homevideo") && products.size()==multipleView)
                            || (entity.equals("AW") && works.size()==multipleView)) &&!isThisLastPage)
                            || thisPage>1) {
                if(thisPage==1) { %>
            for(i=0;i<2;i++){
                precpage[i].disabled=true;
                precpage[i].style.color="grey";
            }
            <% } else { %>
            precpage[0].addEventListener("click", precpagef);
            precpage[1].addEventListener("click", precpagef);
            <% } %>

            <% if(isThisLastPage){ %>
            for(i=0;i<2;i++){
                nextpage[i].disabled=true;
                nextpage[i].style.color="grey";
            }
            <% } else { %>
            nextpage[0].addEventListener("click", nextpagef);
            nextpage[1].addEventListener("click", nextpagef);
            <% }
        }%>
            document.newButtonForm.newButton.addEventListener("click",insert);
        }
    </script>


</head>
<body>
<div class="portal">

    <%@include file="/include/adminHeader.inc" %>

    <main id="content" class="clearfix">

        <form  name="catalogform" action="Dispatcher" method="post">
                <input type="hidden"  name="controllerAction" value="AdminCatalogManagement.<%=entity%>ManagementView">
                <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
                <input type="hidden" name="descendant" value="<%=descendant%>">
                <input type="hidden" name="thisPage" value="<%=thisPage%>">
                <% if(searchString!=null) { %>
                <input type="hidden"  name="searchString" id="searchString" value=<%=searchString%>>
                <% } %>
        </form>

        <div class="box">

            <div class="head" >
                <div >
                    <h3 > <%=entity.equals("AW") ? "Audiovision Work" : entity%> Management<%=searchString!=null? ", Result : " + searchString : ""%> </h3>
                </div>
                <% if (searchString!=null) {%>
                <p class="text-right" style=" padding-right: 20px;">
                    <a href="javascript:cancelsearchString()" style="text-decoration:underline">Cancel search key</a>
                </p>
                <%}%>
            </div>

            <%if((((entity.equals("Homevideo") && products.size()==multipleView)
                    || (entity.equals("AW") && works.size()==multipleView)) &&!isThisLastPage)
                    || thisPage>1)  { %>
            <div class="pageButton">
                <input type="button" name="precpage" class="precpage userbutton click" value="< Prec. Page">
                <input type="button" name="nextpage" class="nextpage userbutton click" value="Next Page >">
                <span class="pagnum">Page <%=thisPage%></span>
            </div>
            <% } %>

            <div id="newButtonDiv">
                <form name="newButtonForm">
                <input type="button" id="newButton" name="newButton"
                       class="newButton userbutton click" value="+ New <%=entity.equals("AW") ? "Work" : entity%>">
                </form>
            </div>


            <div id="search">
                <form action="Dispatcher" method="get" accept-charset="utf-8">
                    <label for="search-field" style="color: #f8e9bd">SEARCH</label>
                    <input type="text" name="searchString" placeholder="Insert title,actor or director"
                           id="search-field" title="Enter search here" class="blink search-field">
                    <button type="submit" class="icon"><i class="fa fa-search click"></i></button>
                    <input type="hidden" name="controllerAction" value="AdminCatalogManagement.<%=entity%>ManagementView">
                </form>
            </div>


            <div class="custom-select">
                        <span style="margin-right: 10px;">
                            <span class="name-check">Descendant</span>
                            <input type="checkbox" id=descendant name="descendant" value="true">
                        </span>
                <label>Order By: </label>
                <select id=orderSelect>
                    <%if(entity.equals("AW")) {%>
                    <option value="original_title">Title</option>
                    <option value="rating">Rating</option>
                    <option value="year_production">Year</option>
                    <% } else { %>
                    <option value="name">Name</option>
                    <option value="price">Price</option>
                    <option value="release_date">Release date</option>
                    <% } %>
                </select>
            </div>

            <div class="box-movie clearfix">
                <%if(entity.equals("Homevideo")) {%>
                <%for (HomevideoProduct product : products) {%>
                <!-- Movie -->
                <div class="movie">

                    <a href="javascript:remove(<%=product.getProductId()%>)">
                        <img class="cancelIcon" src="images/icon/cancel.png" width="20" height="20">
                    </a>
                    <div class="movie-image">
                        <a href="javascript:modify(<%=product.getProductId()%>)">
                            <span class="play"><span class="name"><%=product.getName()%></span></span>
                            <img src="images/<%=product.getPicture_url()%>" alt="movie">
                        </a>
                    </div>
                    <div class="rating">
                        <p>PRICE</p>
                        <div class="stars">
                            <div class="stars-in">

                            </div>
                        </div>
                        <span class="comments">€<%=product.getPrice()%></span>
                    </div>
                </div>
                <!-- end Movie -->

                <% }%>
                <% } else { %>
                <%for (AudiovisionWork work : works) {%>
                <!-- Movie -->
                <div class="movie">

                    <a href="javascript:remove(<%=work.getWorkId()%>)">
                        <img class="cancelIcon" src="images/icon/cancel.png" width="20" height="20">
                    </a>
                    <div class="movie-image">
                        <a href="javascript:modify(<%=work.getWorkId()%>)">
                            <span class="play"><span class="name"><%=work.getOriginalTitle()%></span></span>
                            <img src="images/<%=work.getImageWork()%>" alt="movie">
                        </a>
                    </div>
                    <div class="rating">
                        <p>RATING</p>
                        <div class="stars">
                            <div class="stars-in">

                            </div>
                        </div>
                        <span class="comments">&#9734;<%=work.getRating()%></span>
                    </div>
                </div>
                <!-- end Movie -->

                <% } }%>
            </div>

            <%if((((entity.equals("Homevideo") && products.size()==multipleView)
                    || (entity.equals("AW") && works.size()==multipleView)) &&!isThisLastPage)
                    || thisPage>1)  { %>
            <div class="pageButton">
                <input type="button" name="precpage" class="precpage userbutton click" value="< Prec. Page">
                <input type="button" name="nextpage" class="nextpage userbutton click" value="Next Page >">
                <span class="pagnum">Page <%=thisPage%></span>
            </div>
            <% } %>

        </div>
        <%@include file="/include/footer.inc" %>
    </main>

</div>

    <form name="insertForm" method="post" action="Dispatcher">
        <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
        <input type="hidden" name="descendant" value="<%=descendant%>">
        <input type="hidden" name="thisPage" value="<%=thisPage%>">
        <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
        <input type="hidden" name="multipleView" value="<%=multipleView%>">
        <% if(searchString!=null) { %>
        <input type="hidden"  name="searchString" id="searchString" value=<%=searchString%>>
        <% } %>
        <input type="hidden" name="controllerAction" value='AdminCatalogManagement.insert<%=entity%>View'>
    </form>

    <form name="modifyAndDeleteForm" method="post" action="Dispatcher">
        <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
        <input type="hidden" name="descendant" value="<%=descendant%>">
        <input type="hidden" name="thisPage" value="<%=thisPage%>">
        <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
        <input type="hidden" name="multipleView" value="<%=multipleView%>">
        <% if(searchString!=null) { %>
        <input type="hidden"  name="searchString" id="searchString" value=<%=searchString%>>
        <% } %>
        <input type="hidden" name="controllerAction">
        <input type="hidden" name="Id">
    </form>



</body>
</html>
