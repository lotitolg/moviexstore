<%@ page import="model.session.mo.LoggedAdmin" %>
<%@ page import="model.mo.Genre" %>
<%@page session="false" %>

<%

    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");
    String applicationMessage = (String) request.getAttribute("applicationMessage");

    Genre genre = (Genre) request.getAttribute("genre");
    String action = (genre != null) ? "modify" : "insert";

    String searchString = (String) request.getAttribute("searchString");
    String thisPage = (String) request.getAttribute("thisPage");
    String isThisLastPage = (String) request.getAttribute("isThisLastPage");
    String multipleView = (String) request.getAttribute("multipleView");

%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <style>

        .field {
            margin: 25px 0;
        }

        label {
            float: left;
            width: 90px;
            margin-right: 10px;
            padding-top: 3px;
            text-align: left;
        }

        input {
            border: none;
            border-radius: 4px;
            padding: 3px;
            box-shadow: 0 1px 0 rgba(0, 0, 0, 0.03) inset;
        }

        input:focus {
            background: #d2d9dd;
            outline-color: #a3271f;
        }

        #insModFormSection {
            margin: 30px;
        }

    </style>
    <script>
        var status = "<%=action%>";

        function submitGenre() {
            var f;
            f = document.insModForm;
            f.controllerAction.value = "AdminCatalogManagement." + status + "Genre";
        }
        function goback() {
            document.backForm.submit();
        }
        function mainOnLoadHandler() {
            document.insModForm.addEventListener("submit", submitGenre);
            document.insModForm.backButton.addEventListener("click", goback);
        }
    </script>
</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>


        <div class="box">

            <div class="head">
                <h3>
                    Genre : <%=(action.equals("modify")) ? "Modify " +genre.getGenreName() : "New"%>
                </h3>
            </div>

            <section id="insModFormSection">
                <form name="insModForm" action="Dispatcher" method="post">

                    <div class="field clearfix">
                        <label for="genreName">Genre Name</label>
                        <input type="text" id="genreName" name="genreName"
                               value="<%=(action.equals("modify")) ? genre.getGenreName() : ""%>"
                               required size="20" maxlength="50"/>

                    <div class="field clearfix">
                        <label>&#160;</label>
                        <input type="submit" name="submitButton" class="userbutton click" value="Submit"/>
                        <input type="button" name="backButton" class="userbutton click" value="Cancel"/>
                    </div>


                    <%if (action.equals("modify")) {%>
                    <input type="hidden" name="genreId" value="<%=genre.getGenreId()%>"/>
                    <%}%>
                    <input type="hidden" name="controllerAction">
                    <input type="hidden" name="thisPage" value="<%=thisPage%>">
                    <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
                    <input type="hidden" name="multipleView" value="<%=multipleView%>">
                    <% if (searchString != null) { %>
                    <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
                    <% } %>

                </form>
            </section>

            <form name="backForm" method="post" action="Dispatcher">
                <input type="hidden" name="controllerAction" value="AdminCatalogManagement.genreManagementView"/>
                <input type="hidden" name="thisPage" value="<%=thisPage%>">
                <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
                <input type="hidden" name="multipleView" value="<%=multipleView%>">
                <% if (searchString != null) { %>
                <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
                <% } %>
            </form>
        </div>

    </main>
    <%@include file="/include/footer.inc" %>
</div>

</body>

</html>
