<%@page import="java.util.List" %>
<%@ page import="model.mo.HomevideoProduct" %>
<%@ page import="model.session.mo.LoggedAdmin" %>
<%@ page import="model.mo.Genre" %>

<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");

    String searchString = (String) request.getAttribute("searchString");

    /*Result*/
    List<Genre> genres = (List<Genre>) request.getAttribute("genres");

    /*Page*/
    Integer thisPage = (Integer) request.getAttribute("thisPage");
    Boolean isThisLastPage = (Boolean) request.getAttribute("isThisLastPage");
    final int multipleView = (int) request.getAttribute("multipleView");


%>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <style>

        .pageButton {
            float: left;
            margin-top: 30px;
            margin-left: 25px;
        }

        .pagnum {
            font-size: 14px;
            margin-left: 10px;
        }

        #search .search-field {
            height: 21px;
        }

        #search {
            float: right;
            margin-top: 33px;
            border: 1px white outset;
            margin-right: 52px;
            padding: 5px;
            border-radius: 13px;
            background-color: #690101;
            width: auto;
        }

        #newButtonDiv {
            margin-left: -15px;
            padding-left: 51px;
            float: left;
            margin-top: 26px;
        }

        .cancelIcon {
            margin-left: 10px;
        }

        #genre_list li {
            text-align: center;
            border-radius: 10px;
            line-height: 35px;
            margin: 16px;
            font-weight: bold;
            display: inline-block;
            width: 200px;
            margin-right: -7px;

        }




    </style>

    <script>

        function precpagef() {
            var inp = document.catalogform;
            inp.thisPage.value = "<%=thisPage-1%>";
            inp.submit();
        }

        function nextpagef() {
            var inp = document.catalogform;
            inp.thisPage.value = "<%=thisPage+1%>";
            inp.submit();
        }

        function cancelsearchString() {
            var inp = document.catalogform;
            var searchString = document.getElementById("searchString");
            inp.removeChild(searchString);
            inp.thisPage.value = "1";
            inp.submit();
        }

        function insert() {
            document.insertForm.submit();
        }

        function remove(id) {
            var inp = document.modifyAndDeleteForm;
            if (confirm("Are you sure you want to delete it?")) {
                inp.id.value = id;
                inp.controllerAction.value = 'AdminCatalogManagement.deleteGenre';
                inp.submit();
            }
        }

        function modify(id,name) {
            var inp = document.modifyAndDeleteForm;
            inp.id.value = id;
            inp.genreName.value=name;
            inp.controllerAction.value = 'AdminCatalogManagement.modifyGenreView';
            inp.submit();
        }

        function mainOnLoadHandler() {

            var precpage = document.getElementsByClassName("precpage");
            var nextpage = document.getElementsByClassName("nextpage");

            <%if((genres.size()==multipleView && !isThisLastPage) || thisPage>1) {
                if(thisPage==1) { %>
            precpage[0].disabled = true;
            precpage[0].style.color = "grey";
            <% } else { %>
            precpage[0].addEventListener("click", precpagef);
            <% } %>
            <% if(isThisLastPage){ %>
            nextpage[0].disabled = true;
            nextpage[0].style.color = "grey";
            <% } else { %>
            nextpage[0].addEventListener("click", nextpagef);
            <% }
        }%>
            document.newButtonForm.newButton.addEventListener("click", insert);

        }
    </script>


</head>
<body>
<div class="portal">

    <%@include file="/include/adminHeader.inc" %>

    <main id="content" class="clearfix">

        <form name="catalogform" action="Dispatcher" method="post">
            <input type="hidden" name="controllerAction" value="AdminCatalogManagement.genreManagementView">
            <input type="hidden" name="thisPage" value="<%=thisPage%>">
            <% if (searchString != null) { %>
            <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
            <% } %>
        </form>

        <div class="box clearfix">

            <div class="head">
                <div>
                    <h3> Genre Management<%=searchString != null ? ", Result : " + searchString : ""%>
                    </h3>
                </div>
                <% if (searchString != null) {%>
                <p class="text-right" style=" padding-right: 20px;">
                    <a href="javascript:cancelsearchString()" style="text-decoration:underline">Cancel search key</a>
                </p>
                <%}%>
            </div>

            <%if ((genres.size() == multipleView && !isThisLastPage) || thisPage > 1) { %>
            <div class="pageButton">
                <input type="button" name="precpage" class="precpage userbutton click" value="< Prec. Page">
                <input type="button" name="nextpage" class="nextpage userbutton click" value="Next Page >">
                <span class="pagnum">Page <%=thisPage%></span>
            </div>
            <% } %>

            <div id="newButtonDiv">
                <form name="newButtonForm">
                    <input type="button" id="newButton" name="newButton"
                           class="newButton userbutton click" value="+ New Genre">
                </form>
            </div>

            <div id="search">
                <form action="Dispatcher" method="get" accept-charset="utf-8">
                    <label for="search-field" style="color: #f8e9bd">SEARCH</label>
                    <input type="text" name="searchString" placeholder="Insert Genre Name.."
                           id="search-field" title="Enter search here" class="blink search-field">
                    <button type="submit" class="icon"><i class="fa fa-search click"></i></button>
                    <input type="hidden" name="controllerAction" value="AdminCatalogManagement.genreManagementView">
                </form>
            </div>

            <div class="clearfix" style="float:left;">
                <ul id="genre_list" class="clearfix" style="list-style-position: inside; list-style-type: none; margin: 3px">
                    <%for (Genre genre : genres) {%>
                    <a href="javascript:modify('<%=genre.getGenreId()%>','<%=genre.getGenreName()%>')" title="Modify <%=genre.getGenreName()%>"><li><%=genre.getGenreName()%></li></a>
                    <a class="clickIcon" href="javascript:remove(<%=genre.getGenreId()%>)">
                        <img class="cancelIcon" src="images/icon/cancel.png" width="20" height="20" alt="20">
                    </a>
                    <%}%>
                </ul>
            </div>


        </div>
    </main>
    <%@include file="/include/footer.inc" %>
</div>

<form name="insertForm" method="post" action="Dispatcher">
    <input type="hidden" name="thisPage" value="<%=thisPage%>">
    <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
    <input type="hidden" name="multipleView" value="<%=multipleView%>">
    <% if (searchString != null) { %>
    <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
    <% } %>
    <input type="hidden" name="controllerAction" value='AdminCatalogManagement.insertGenreView'>
</form>

<form name="modifyAndDeleteForm" method="post" action="Dispatcher">
    <input type="hidden" name="thisPage" value="<%=thisPage%>">
    <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
    <input type="hidden" name="multipleView" value="<%=multipleView%>">
    <% if (searchString != null) { %>
    <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
    <% } %>
    <input type="hidden" name="controllerAction">
    <input type="hidden" name="id">
    <input type="hidden" name="genreName">
</form>


</body>
</html>
