<%@ page import="model.session.mo.LoggedAdmin" %>
<%@ page import="model.mo.HomevideoProduct" %>
<%@page session="false" %>

<%
    int i = 0;
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");
    String applicationMessage = (String) request.getAttribute("applicationMessage");

    HomevideoProduct homevideoProduct = (HomevideoProduct) request.getAttribute("homevideoProduct");
    String action = (homevideoProduct != null) ? "modify" : "insert";


    String orderFilter = (String) request.getAttribute("orderFilter");
    String descendant = (String) request.getAttribute("descendant");
    String searchString = (String) request.getAttribute("searchString");
    String thisPage = (String) request.getAttribute("thisPage");
    String isThisLastPage = (String) request.getAttribute("isThisLastPage");
    String multipleView = (String) request.getAttribute("multipleView");

%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <style>

        .field {
            margin: 25px 0;
        }

        label {
            float: left;
            width: 90px;
            margin-right: 10px;
            padding-top: 3px;
            text-align: left;
        }

        input {
            border: none;
            border-radius: 4px;
            padding: 3px;
            box-shadow: 0 1px 0 rgba(0, 0, 0, 0.03) inset;
        }

        input:focus {
            background: #d2d9dd;
            outline-color: #a3271f;
        }

        #insModFormSection {
            margin: 30px;
        }

    </style>
    <script>
        var status = "<%=action%>";

        function submitHomevideo() {
            var f;
            f = document.insModForm;
            var workInp = f.workId;
            var firstChar = workInp.value.substring(0,1);
            for(var i=0; i < f.elements.length; i++){
                if(f.elements[i].value === '' && f.elements[i].hasAttribute('required')){
                    alert(f.elements[i].name+' is a required field!');
                }
            }
            if(Number.isInteger(parseInt(firstChar))) {
                f.controllerAction.value = "AdminCatalogManagement." + status + "Homevideo";
                f.submit();
            }
            else{
                alert("Incorrect value of Work field");
            }
        }

        function goback() {
            document.backForm.submit();
        }

        function addLanguage() {
            var x = document.getElementById("languagesInput");
            var y = document.getElementById("languages0");
            var z = y.cloneNode();
            z.value = "";
            z.style.display = "block";
            z.style.margin = "10px 0 0 100px";
            x.appendChild(z);
        }

        function loadList(searchString) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var optionsList = JSON.parse(this.responseText);
                    var datalist = document.getElementById("workNameDL");
                    fillDatalist(datalist, optionsList);
                }
            };
            xhttp.open("GET", "jsp/adminCatalogManagement/homevideoManagement/ajax/workListAJAX.jsp?searchString=" + searchString, true);
            xhttp.send();
        }



        function renewDatalist(event) {
            if (event.target.value.trim() !== "")
                loadList(event.target.value);
            var datalist = document.getElementById("workNameDL");
            while (datalist.children.length > 0) {
                datalist.children[0].remove();
            }
        }

        function fillDatalist(datalist, optionsList) {
            console.log(new Date().getTime() + "---------------------------");
            while (datalist.children.length > 0) {
                datalist.children[0].remove();
            }
            for (var i = 0; i < optionsList.length; i++) {
                console.log(optionsList[i]);
                var option = document.createElement('option');
                option.value = optionsList[i];
                datalist.appendChild(option);
            }

        }

        function mainOnLoadHandler() {
            document.insModForm.submitButton.addEventListener("click", submitHomevideo);
            document.insModForm.backButton.addEventListener("click", goback);
            document.insModForm.addLanguage.addEventListener("click", addLanguage);
            document.insModForm.workId.addEventListener("keyup",renewDatalist);
        }



    </script>
</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>


        <div class="box">

            <div class="head">
                <h3>
                    Homevideo : <%=(action.equals("modify")) ? "Modify " +homevideoProduct.getName() : "New"%>
                </h3>
            </div>

            <section id="insModFormSection">
                <form name="insModForm" action="Dispatcher" method="post" enctype="multipart/form-data">

                    <div class="field clearfix">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name"
                               value="<%=(action.equals("modify")) ? homevideoProduct.getName() : ""%>"
                               required size="20" maxlength="50"/>
                    </div>
                    <div class="field clearfix">
                        <label for="workId">Work</label>
                        <input type="text" id="workId" name="workId"
                               value="<%=(action.equals("modify")) ? homevideoProduct.getAudiovisionWork().getWorkId() : ""%>"
                               required size="20" maxlength="50" list="workNameDL"/>
                        <datalist id="workNameDL"></datalist>
                    </div>
                    <div class="field clearfix">
                        <label>Format</label>
                        <input type="radio" name="format" value="DVD" required
                                <%=(action.equals("modify") && homevideoProduct.getFormat().equals("DVD")) ? "checked" : ""%>
                        /> DVD
                        <input type="radio" name="format" value="Blu-ray"
                                <%=(action.equals("modify") && homevideoProduct.getFormat().equals("Blu-ray")) ? "checked" : ""%>
                        /> Blu-ray
                        <input type="radio" name="format" value="UHD"
                                <%=(action.equals("modify") && homevideoProduct.getFormat().equals("UHD")) ? "checked" : ""%>
                        /> UHD (4K)
                    </div>
                    <div class="field clearfix">
                        <label for="price">Price &#8364;</label>
                        <input type="number" id="price" name="price"
                               value="<%=(action.equals("modify")) ? homevideoProduct.getPrice() : ""%>"
                               min="0" step="0.01"/>
                    </div>
                    <div class="field clearfix">
                        <label for="numberSupport">Number Support</label>
                        <input type="number" id="numberSupport" name="numberSupport"
                               value="<%=(action.equals("modify")) ? homevideoProduct.getNumberSupport() : ""%>"
                               min="1" step="1" size="4"/>
                    </div>


                    <div class="field clearfix" id="languagesInput">
                        <label for="languages<%=0%>">Languages</label>
                        <% if (action.equals("modify")) {
                            for (i = 0; i < homevideoProduct.getLanguages().length; i++) { %>
                        <input type="text" id="languages<%=i%>" name="languages"
                               value="<%=homevideoProduct.getLanguage(i)%>"
                               <%=i>0 ? "style=\"display: block;margin:10px 0 0 100px\"" : ""%>/>
                        <%if (i == 0) {%>
                        <input type="button" class="click" name="addLanguage"
                               value="+" style="width: 20px">
                        <% }
                        }
                        } else {%>
                        <input type="text" id="languages<%=0%>" name="languages"/>
                        <input type="button" class="click" name="addLanguage"
                               value="+" style="width: 20px">
                        <% } %>

                    </div>


                    <div class="field clearfix">
                        <label for="releaseDate">Release Date</label>
                        <input type="date" id="releaseDate" name="releaseDate"
                               value="<%=(action.equals("modify")) ? homevideoProduct.getReleaseDate() : ""%>"/>
                    </div>
                    <div class="field clearfix">
                        <label for="available">Availability</label>
                        <input type="number" id="available" name="available"
                               value="<%=(action.equals("modify")) ? homevideoProduct.getAvailable() : ""%>"
                               required min="0" step="1"/>
                    </div>

                        <input type="hidden" id="pictureUrl" name="pictureUrl"
                               value="<%=(action.equals("modify")) ? homevideoProduct.getPicture_url() : ""%>">


                    <div class="field clearfix">
                        <label for="pictureFile">Picture Url</label>
                        <input type="file" id="pictureFile" name="pictureFile">
                    </div>

                    <div class="field clearfix">
                        <label>&#160;</label>
                        <input type="button" name="submitButton" class="userbutton click" value="Submit"/>
                        <input type="button" name="backButton" class="userbutton click" value="Cancel"/>
                    </div>


                    <%if (action.equals("modify")) {%>
                    <input type="hidden" name="productId" value="<%=homevideoProduct.getProductId()%>"/>
                    <%}%>
                    <input type="hidden" name="controllerAction"/>
                    <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
                    <input type="hidden" name="descendant" value="<%=descendant%>">
                    <input type="hidden" name="thisPage" value="<%=thisPage%>">
                    <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
                    <input type="hidden" name="multipleView" value="<%=multipleView%>">
                    <% if (searchString != null) { %>
                    <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
                    <% } %>

                </form>
            </section>

            <form name="backForm" method="post" action="Dispatcher">
                <input type="hidden" name="controllerAction" value="AdminCatalogManagement.HomevideoManagementView"/>
                <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
                <input type="hidden" name="descendant" value="<%=descendant%>">
                <input type="hidden" name="thisPage" value="<%=thisPage%>">
                <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
                <input type="hidden" name="multipleView" value="<%=multipleView%>">
                <% if (searchString != null) { %>
                <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
                <% } %>
            </form>
        </div>

    </main>
    <%@include file="/include/footer.inc" %>
</div>

</body>

</html>
