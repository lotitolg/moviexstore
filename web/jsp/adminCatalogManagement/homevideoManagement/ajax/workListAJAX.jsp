<%@ page import="model.dao.DAOFactory" %>
<%@ page import="services.config.Configuration" %>
<%@ page import="model.mo.AudiovisionWork" %>
<%@ page import="model.dao.AudiovisionWorkDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="services.logservice.LogService" %>
<%@ page import="java.util.logging.Level" %>
<%@page contentType="text/plain" pageEncoding="UTF-8"%>
<%@page session="false" %>
<%
    Logger logger = LogService.getApplicationLogger();
    DAOFactory daoFactory = null;
    List<AudiovisionWork> audiovisionWorkList;
    try {
        daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
        daoFactory.beginTransaction();

        AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();

        String searchString = request.getParameter("searchString");

        audiovisionWorkList = audiovisionWorkDAO.findByFilters(null, null, null, null, false, 1, 10, searchString);

        daoFactory.commitTransaction();

    } catch (Exception e) {
        logger.log(Level.SEVERE, "AJAX Error", e);
        try {
            if (daoFactory != null) {
                daoFactory.rollbackTransaction();
            }
        } catch (Throwable t) {
        }
        throw new RuntimeException(e);

    } finally {
        try {
            if (daoFactory != null) {
                daoFactory.closeTransaction();
            }
        } catch (Throwable t) {
        }
    }
%>
[
<% for (int i = 0; i < audiovisionWorkList.size(); i++) { %>
"<%=audiovisionWorkList.get(i).getWorkId()%>-<%=audiovisionWorkList.get(i).getOriginalTitle()%>"<%=i != audiovisionWorkList.size() - 1 ? "," : ""%>
<%}%>
]