<%--
  Created by IntelliJ IDEA.
  User: gigil
  Date: 16/07/2019
  Time: 17:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.session.mo.LoggedAdmin" %>
<%@ page import="model.mo.AudiovisionWork" %>
<%@page session="false" %>

<%
    int i = 0;
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");
    String applicationMessage = (String) request.getAttribute("applicationMessage");

    AudiovisionWork audiovisionWork = (AudiovisionWork) request.getAttribute("audiovisionWork");
    String action = (audiovisionWork != null) ? "modify" : "insert";


    String orderFilter = (String) request.getAttribute("orderFilter");
    String descendant = (String) request.getAttribute("descendant");
    String searchString = (String) request.getAttribute("searchString");
    String thisPage = (String) request.getAttribute("thisPage");
    String isThisLastPage = (String) request.getAttribute("isThisLastPage");
    String multipleView = (String) request.getAttribute("multipleView");

%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <style>

        .field {
            margin: 25px 0;
        }

        label {
            float: left;
            width: 90px;
            margin-right: 10px;
            padding-top: 3px;
            text-align: left;
        }

        input {
            border: none;
            border-radius: 4px;
            padding: 3px;
            box-shadow: 0 1px 0 rgba(0, 0, 0, 0.03) inset;
        }

        input:focus {
            background: #d2d9dd;
            outline-color: #a3271f;
        }

        #insModFormSection {
            margin: 30px;
        }

    </style>
    <script>
        var status = "<%=action%>";

        function submitAudiovisionWork() {
            var f;
            f = document.insModForm;
            f.controllerAction.value = "AdminCatalogManagement." + status + "AW";
        }

        function goback() {
            document.backForm.submit();
        }

        function add(event) {
            var x;
            var y;
            var z;
            switch (event.target.name) {
                case "addGenre":
                    x = document.getElementById("genresInput");
                    y = document.getElementById("genres0");
                    break;
                case "addActor":
                    x = document.getElementById("actorsInput");
                    y = document.getElementById("actors0");
                    break;
                case "addDirector":
                    x = document.getElementById("directorsInput");
                    y = document.getElementById("directors0");
                    break;
            }
            z = y.cloneNode();
            z.value = "";
            z.style.display = "block";
            z.style.margin = "10px 0 0 100px";
            z.addEventListener("keyup", renewDatalist);
            x.appendChild(z);
        }

        function loadList(searchString, nameInput) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var optionsList = JSON.parse(this.responseText);
                    var datalist = document.getElementById(nameInput + "NameDL");
                    fillDatalist(datalist, optionsList);
                }
            };
            xhttp.open("GET", "jsp/adminCatalogManagement/workManagement/ajax/" + nameInput + "ListAJAX.jsp?searchString=" + searchString, true);
            xhttp.send();
        }


        function renewDatalist(event) {
            if (event.target.value.trim() !== "")
                loadList(event.target.value, event.target.name);
            var datalist = document.getElementById(event.target.name + "NameDL");
            while (datalist.children.length > 0) {
                datalist.children[0].remove();
            }
        }

        function fillDatalist(datalist, optionsList) {
            console.log(new Date().getTime() + "---------------------------");
            while (datalist.children.length > 0) {
                datalist.children[0].remove();
            }
            for (var i = 0; i < optionsList.length; i++) {
                console.log(optionsList[i]);
                var option = document.createElement('option');
                option.value = optionsList[i];
                datalist.appendChild(option);
            }
        }

        function mainOnLoadHandler() {
            document.insModForm.addEventListener("submit", submitAudiovisionWork);
            document.insModForm.backButton.addEventListener("click", goback);
            document.insModForm.addGenre.addEventListener("click", add);
            document.insModForm.addActor.addEventListener("click", add);
            document.insModForm.addDirector.addEventListener("click", add);
            var i;
            var actors = document.insModForm.actors;
            if (actors.length === undefined) actors.addEventListener("keyup", renewDatalist);
            else {
                for (i = 0; i < actors.length; i++)
                    actors[i].addEventListener("keyup", renewDatalist);
            }
            var genres = document.insModForm.genres;
            if (genres.length === undefined) genres.addEventListener("keyup", renewDatalist);
            else {
                for (i = 0; i < genres.length; i++)
                    genres[i].addEventListener("keyup", renewDatalist);
            }
            var directors = document.insModForm.directors;
            if (directors.length === undefined) directors.addEventListener("keyup", renewDatalist);
            else {
                for (i = 0; i < directors.length; i++)
                    directors[i].addEventListener("keyup", renewDatalist);
            }
        }

    </script>
</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>


        <div class="box">

            <div class="head">
                <h3>
                    Audiovision Work
                    : <%=(action.equals("modify")) ? "Modify " + audiovisionWork.getOriginalTitle() : "New"%>
                </h3>
            </div>

            <section id="insModFormSection">
                <form name="insModForm" action="Dispatcher" method="post" enctype="multipart/form-data">

                    <div class="field clearfix">
                        <label for="title">Title</label>
                        <input type="text" id="title" name="title"
                               value="<%=(action.equals("modify")) ? audiovisionWork.getOriginalTitle() : ""%>"
                               required size="20" maxlength="50"/>
                    </div>

                    <div class="field clearfix">
                        <label>Type</label>
                        <input type="radio" name="type" value="Movie" required
                                <%=(action.equals("modify") && audiovisionWork.getType().equals("Movie")) ? "checked" : ""%>
                        /> Movie
                        <input type="radio" name="type" value="TV series"
                                <%=(action.equals("modify") && audiovisionWork.getType().equals("TV series")) ? "checked" : ""%>
                        /> TV series

                    </div>
                    <div class="field clearfix">
                        <label for="duration">Duration(min)</label>
                        <input type="number" id="duration" name="duration"
                               value="<%=(action.equals("modify")) ? audiovisionWork.getDuration() : ""%>"
                               min="1" step="1"/>
                    </div>
                    <div class="field clearfix">
                        <label for="rating">Rating</label>
                        <input type="number" id="rating" name="rating"
                               value="<%=(action.equals("modify")) ? audiovisionWork.getRating() : ""%>"
                               min="0" max="10" step="0.1"/> /10
                    </div>

                    <div class="field clearfix">
                        <label for="yearProduction">Year Production</label>
                        <input type="text" id="yearProduction" name="yearProduction"
                               value="<%=(action.equals("modify")) ? audiovisionWork.getYearProdction() : ""%>"
                               size="20" maxlength="50"/>
                    </div>

                    <div class="field clearfix">
                        <label for="studioProduction">Studio Production</label>
                        <input type="text" id="studioProduction" name="studioProduction"
                               value="<%=(action.equals("modify")) ? audiovisionWork.getStudioProduction() : ""%>"
                               size="20" maxlength="50"/>
                    </div>

                    <div class="field clearfix">
                        <label for="trailerUrl">YouTube Trailer URL Code</label>
                        <input type="text" id="trailerUrl" name="trailerUrl"
                               value="<%=(action.equals("modify")) ? audiovisionWork.getTrailerUrl() : ""%>"
                               size="20" maxlength="50"/>
                    </div>

                    <div class="field clearfix">
                        <label for="plot">Plot</label>
                        <textarea id="plot" name="plot"
                                  cols="50" rows="5"
                                  wrap="soft"><%=(action.equals("modify")) ? audiovisionWork.getPlot() : ""%>
                        </textarea>
                    </div>


                    <div class="field clearfix" id="genresInput">
                        <label for="genres0">Genres</label>
                        <% if (action.equals("modify")) { %>
                        <input type="text" id="genres<%=i%>" name="genres" list="genresNameDL"
                               value="<%=audiovisionWork.getGenres().length==0 ?
                               "" : audiovisionWork.getGenre(0).getGenreName()%>">
                        <input type="button" class="click" name="addGenre"
                               value="+" style="width: 20px">
                        <% for (i = 1; i < audiovisionWork.getGenres().length; i++) { %>
                        <input type="text" id="genres<%=i%>" name="genres" list="genresNameDL"
                               value="<%=audiovisionWork.getGenre(i).getGenreName()%>"
                               style="display: block;margin:10px 0 0 100px"/>
                        <datalist id="genresNameDL"></datalist>
                        <% }
                        } else {%>
                        <input type="text" id="genres0" name="genres" list="genresNameDL"/>
                        <datalist id="genresNameDL"></datalist>
                        <input type="button" class="click" name="addGenre"
                               value="+" style="width: 20px">
                        <% } %>
                    </div>

                    <div class="field clearfix" id="actorsInput">
                        <label for="actors0">Actors</label>
                        <% i=0; if (action.equals("modify")) { %>
                        <input type="text" id="actors<%=i%>" name="actors" list="actorsNameDL"
                               value="<%=audiovisionWork.getActors().length==0 ?
                               "" : audiovisionWork.getActor(0).getActorName() %>">
                        <input type="button" class="click" name="addActor"
                               value="+" style="width: 20px">
                        <% for (i = 1; i < audiovisionWork.getActors().length; i++) { %>
                        <input type="text" id="actors<%=i%>" name="actors"
                               value="<%=audiovisionWork.getActor(i).getActorName()%>" list="actorsNameDL"
                                 style="display: block;margin:10px 0 0 100px"/>
                        <datalist id="actorsNameDL"></datalist>
                        <%}
                        } else {%>
                        <input type="text" id="actors0" name="actors" list="actorsNameDL"/>
                        <datalist id="actorsNameDL"></datalist>
                        <input type="button" class="click" name="addActor"
                               value="+" style="width: 20px">
                        <% } %>
                    </div>

                    <div class="field clearfix" id="directorsInput">
                        <label for="directors0">Directors</label>
                        <% i=0; if (action.equals("modify")) { %>
                        <input type="text" id="directors<%=i%>" name="directors" list="directorsNameDL"
                               value="<%=audiovisionWork.getDirectors().length==0 ?
                               "" : audiovisionWork.getDirector(0).getDirectorName()%>">
                        <input type="button" class="click" name="addDirector"
                               value="+" style="width: 20px">
                          <%  for (i = 1; i < audiovisionWork.getDirectors().length; i++) { %>
                        <input type="text" id="directors<%=i%>" name="directors" list="directorsNameDL"
                               value="<%=audiovisionWork.getDirector(i).getDirectorName()%>"
                                style="display: block;margin:10px 0 0 100px"/>
                        <datalist id="directorsNameDL"></datalist>
                        <% }
                        } else {%>
                        <input type="text" id="directors0" name="directors" list="directorsNameDL"/>
                        <datalist id="directorsNameDL"></datalist>
                        <input type="button" class="click" name="addDirector"
                               value="+" style="width: 20px">
                        <% } %>
                    </div>


                    <input type="hidden" id="imageWorkUrl" name="imageWorkUrl"
                               value="<%=(action.equals("modify")) ? audiovisionWork.getImageWork() : ""%>"/>


                    <div class="field clearfix">
                        <label for="imageWork">Picture Url</label>
                        <input type="file" id="imageWork" name="imageWork">
                    </div>

                    <div class="field clearfix">
                        <label>&#160;</label>
                        <input type="submit" class="userbutton click" value="Submit"/>
                        <input type="button" name="backButton" class="userbutton click" value="Cancel"/>
                    </div>

                    <%if (action.equals("modify")) {%>
                    <input type="hidden" name="workId" value="<%=audiovisionWork.getWorkId()%>"/>
                    <%}%>
                    <input type="hidden" name="controllerAction"/>
                    <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
                    <input type="hidden" name="descendant" value="<%=descendant%>">
                    <input type="hidden" name="thisPage" value="<%=thisPage%>">
                    <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
                    <input type="hidden" name="multipleView" value="<%=multipleView%>">
                    <% if (searchString != null) { %>
                    <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
                    <% } %>

                </form>
            </section>

            <form name="backForm" method="post" action="Dispatcher">
                <input type="hidden" name="controllerAction" value="AdminCatalogManagement.AWManagementView"/>
                <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
                <input type="hidden" name="descendant" value="<%=descendant%>">
                <input type="hidden" name="thisPage" value="<%=thisPage%>">
                <input type="hidden" name="isThisLastPage" value="<%=isThisLastPage%>">
                <input type="hidden" name="multipleView" value="<%=multipleView%>">
                <% if (searchString != null) { %>
                <input type="hidden" name="searchString" id="searchString" value=<%=searchString%>>
                <% } %>
            </form>
        </div>

    </main>
    <%@include file="/include/footer.inc" %>
</div>

</body>

</html>

