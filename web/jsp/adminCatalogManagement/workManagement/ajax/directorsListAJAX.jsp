<%@ page import="model.dao.DAOFactory" %>
<%@ page import="services.config.Configuration" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="services.logservice.LogService" %>
<%@ page import="java.util.logging.Level" %><%@ page import="model.mo.Director"%><%@ page import="model.dao.DirectorDAO"%>
<%@page contentType="text/plain" pageEncoding="UTF-8"%>
<%@page session="false" %>
<%
    Logger logger = LogService.getApplicationLogger();
    DAOFactory daoFactory = null;
    List<Director> directorList;
    try {
        daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
        daoFactory.beginTransaction();

        DirectorDAO directorDAO = daoFactory.getDirectorDAO();

        String searchString = request.getParameter("searchString");

        directorList = directorDAO.findBySearchString(searchString);

        daoFactory.commitTransaction();

    } catch (Exception e) {
        logger.log(Level.SEVERE, "AJAX Error", e);
        try {
            if (daoFactory != null) {
                daoFactory.rollbackTransaction();
            }
        } catch (Throwable t) {
        }
        throw new RuntimeException(e);

    } finally {
        try {
            if (daoFactory != null) {
                daoFactory.closeTransaction();
            }
        } catch (Throwable t) {
        }
    }
%>
[
<% for (int i = 0; i < directorList.size(); i++) { %>
"<%=directorList.get(i).getDirectorName()%>"<%=i != directorList.size() - 1 ? "," : ""%>
<%}%>
]