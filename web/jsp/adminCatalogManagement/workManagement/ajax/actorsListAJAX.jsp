<%@ page import="model.dao.DAOFactory" %>
<%@ page import="services.config.Configuration" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="services.logservice.LogService" %>
<%@ page import="java.util.logging.Level" %><%@ page import="model.mo.Actor"%><%@ page import="model.dao.ActorDAO"%>
<%@page session="false" %>
<%@page contentType="text/plain" pageEncoding="UTF-8"%>

<%
    Logger logger = LogService.getApplicationLogger();
    DAOFactory daoFactory = null;
    List<Actor> actorList;
    try {
        daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
        daoFactory.beginTransaction();

        ActorDAO actorDAO = daoFactory.getActorDAO();

        String searchString = request.getParameter("searchString");

        actorList = actorDAO.findBySearchString(searchString);

        daoFactory.commitTransaction();

    } catch (Exception e) {
        logger.log(Level.SEVERE, "AJAX Error", e);
        try {
            if (daoFactory != null) {
                daoFactory.rollbackTransaction();
            }
        } catch (Throwable t) {
        }
        throw new RuntimeException(e);

    } finally {
        try {
            if (daoFactory != null) {
                daoFactory.closeTransaction();
            }
        } catch (Throwable t) {
        }
    }
%>
[
<% for (int i = 0; i < actorList.size(); i++) { %>
"<%=actorList.get(i).getActorName()%>"<%=i != actorList.size() - 1 ? "," : ""%>
<%}%>
]