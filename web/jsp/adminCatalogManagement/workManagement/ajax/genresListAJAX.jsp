<%@ page import="model.dao.DAOFactory" %>
<%@ page import="services.config.Configuration" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="services.logservice.LogService" %>
<%@ page import="java.util.logging.Level" %><%@ page import="model.mo.Genre"%><%@ page import="model.dao.GenreDAO"%>
<%@page contentType="text/plain" pageEncoding="UTF-8"%>
<%@page session="false" %>
<%
    Logger logger = LogService.getApplicationLogger();
    DAOFactory daoFactory = null;
    List<Genre> genreList;
    try {
        daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
        daoFactory.beginTransaction();

        GenreDAO genreDAO = daoFactory.getGenreDAO();

        String searchString = request.getParameter("searchString");

        genreList = genreDAO.findBySearchString(searchString,1,30);

        daoFactory.commitTransaction();

    } catch (Exception e) {
        logger.log(Level.SEVERE, "AJAX Error", e);
        try {
            if (daoFactory != null) {
                daoFactory.rollbackTransaction();
            }
        } catch (Throwable t) {
        }
        throw new RuntimeException(e);

    } finally {
        try {
            if (daoFactory != null) {
                daoFactory.closeTransaction();
            }
        } catch (Throwable t) {
        }
    }
%>
[
<% for (int i = 0; i < genreList.size(); i++) { %>
"<%=genreList.get(i).getGenreName()%>"<%=i != genreList.size() - 1 ? "," : ""%>
<%}%>
]