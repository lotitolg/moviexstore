<%@ page import="model.session.mo.LoggedAdmin" %>
<%@ page import="model.mo.Admin" %><%--
  Created by IntelliJ IDEA.
  User: gigil
  Date: 14/07/2019
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@page session="false" %>

<%
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
%>
<html>
<head>

    <%@include file="/include/htmlHead.inc" %>

</head>
    <body>
        <div class="portal">
            <%@include file="/include/adminHeader.inc"%>
            <main>
                <div class="box">
                    <div class="head">
                        <h3>ADMINISTATOR AREA</h3>
                    </div>
                    <%if(!loggedOn) {%>
                        <p class="mytext">Please logon.</p>
                    <% } else { %>
                    <p class="mytext">Welcome <%=loggedAdmin.getFirstname()%>, choose a management on the navigation bar.</p>
                    <% } %>
                </div>
            </main>
            <%@include file="/include/footer.inc"%>
        </div>

    </body>
</html>
