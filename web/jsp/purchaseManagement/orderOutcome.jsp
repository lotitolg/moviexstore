<%@ page import="model.session.mo.LoggedUser" %>

<%-- 
    Document   : orderConfirmed
    Created on : 14-lug-2019, 18.00.57
    Author     : simon70
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page session="false"%>
<%
    boolean completeOrder= (boolean)request.getAttribute("completedOrder");
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");
%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/include/htmlHead.inc"%> 

        <style>
            .aside_body_a ul li{

                margin: 5px;
            }

            .User_info_box{

                height: max-content;

            }

            #body_Info{

                margin: 20px;

            }

            .aside_body_a li :hover{
                color: red;
            }

            .aside_body_a > button {
                font-size: 20px;
                margin: 7px;
                padding: 10%;
                border-color: whitesmoke;
                background: darkred;
                border-width: 2px;
                box-shadow: 1px 1px grey;

            }

            .aside_body_a > button:hover {
                font-size: 20px;
                margin: 6px;
                margin-bottom: 10px;
                padding: 10%;
                border-color: whitesmoke;
                background: darkred;
                border-width: 1px;
                box-shadow: none;

            }
            #aside1{
                width: 20%;
                border: 1px whitesmoke solid;
                margin-top: 20px;
                margin-left: 21px;
                clear: left;
            }

        </style>
        <title>Oreder confirmed</title>
    </head>
    <body>
        <%@include file="/include/header.inc"%>

        <%@include file="/include/aside.inc"%>

        <div id="principle" style="margin-top: 20px;
             margin-bottom: 10px;
             border: 1px white solid;
             width: 73%;
             float: right;
             margin-right: 20px;">

            <div class="User_info_box portal">
                <div class="head">
                    <h3 style="">Cart</h3>
                    <p class="text-right" style=" padding-right: 20px;"></p>
                </div>

                <div class="clearfix" style=" border-bottom: 0.5px white dotted; border-radius: 10px; text-align: center; margin-left: 320px">
                    <h3 style="margin: 12px;float:left ;color: greenyellow"> Order <%=completeOrder?"Confirmed Succesfully":"Failed"%><p style="font-size:100px">
                        <%=completeOrder?"&#128522;":"&#128543;"%></p></h3>

                </div>
            </div>
        </div>
    </body>
</html>
