<%@page import="java.text.DecimalFormat"%>
<%@page import="model.mo.Coupon"%>
<%@page import="java.util.List"%>
<%@page import="model.mo.ProductInCart"%>
<%@ page import="model.session.mo.LoggedUser" %>
<%@page import="model.mo.Customer" %>
<%@page import="model.mo.BillingInfo" %>
<%@page import="model.mo.DeliveryAddress" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page session="false"%>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");

    List<ProductInCart> productInCart = (List<ProductInCart>) request.getAttribute("productsInCart");
    Coupon coupon = (Coupon) request.getAttribute("coupon_discount");
    /*Customer customer = (Customer) request.getAttribute("customer");*/
    DeliveryAddress deliveryAddress = (DeliveryAddress) request.getAttribute("deliveryAddress");
    BillingInfo billingInfo = (BillingInfo) request.getAttribute("billingInfo");

%>

<%    double totalPrice = 0;
    int numberOfProducts = 0;
    double shippingPrice = (double)request.getAttribute("shippingPrice");
    DecimalFormat df = new DecimalFormat("#.##");

    for (ProductInCart product : productInCart) {
        totalPrice = (product.getHomevideoProducts().getPrice()) * (product.getItemQuantity()) + totalPrice;
        numberOfProducts = numberOfProducts + product.getItemQuantity();
    }

    int discount =  coupon!=null? coupon.getDiscountPercent(): 0;
    totalPrice = totalPrice - (totalPrice * (discount / 100.0));
    double totalPriceWithSpedition = totalPrice + shippingPrice;
    totalPrice = (double) Math.round(totalPrice * 100d) / 100d;
    totalPriceWithSpedition = (double) Math.round(totalPriceWithSpedition * 100d) / 100d;
%>

<!DOCTYPE html>
<html>

    <head>
        <%@include file="/include/htmlHead.inc"%> 

        <style>

            .displayAddressDiv{
                overflow: hidden ;
                margin-top: 5px;
                margin-left: 10px;
                float:left;

            }

            .displayAddressDiv h3{
                overflow: hidden ;
                margin-top: 5px;
                color: deepskyblue;
                font-size: 17px;
            }

            .displayPaymentDiv{
                margin-right: 10px;
                float:left;
            }


            .displayPaymentDiv h3{
                overflow: hidden ;
                margin-top: 5px;
                color: deepskyblue;
                font-size: 17px;
            }

            #costsPurchase h3{
                margin-top: 10px;
                font-size: 17px;
                margin-bottom: 10px;
                text-align: right;
                display: flex;
                float: right; 
            }

            #costsPurchase > h3 > p{
                color: red;margin-left: 40px;margin-right: 40px; font-size: 22px ; color: #40ff00 ;
            }

            #Coupon_inserition{

                margin-left: 10px;
                margin-right: 7px;
                margin-top: 5px;
                float: right;
                width: 294px;
            }


        </style>

        <script>

            var confirmButton = document.getElementById("confirmB");

            <% if (billingInfo.getCardNum().equals("0000") || deliveryAddress.getAddress() == null || deliveryAddress.getCity()==null || deliveryAddress.getCountry()==null) {%>
            confirmButton.disabled = true;
            confirmButton.textContent = "disabled control your date";
            <%}%>
        </script>

    </head>

    <body>
    <div class="portal clearfix">
        <%@include file="/include/header.inc"%>

        <div style="margin-top: 20px">

            <aside id="purchase" class="left portal" style="margin-left: 21px; width: 70%; border: 1px white solid ;">


                <div class="head" style="">
                    <h3 style="text-align: center "> 
                        Check and complete the order
                    </h3>
                </div> 

                <div style=" ">



                    <div class="displayAddressDiv clearfix">

                        <h3 >Shipping address <a href="Dispatcher?controllerAction=InfoUserManagement.deliveryAddressView" style="font-size:10px;"> modify</a></h3>

                        <div>

                            <ul class="displayAddressUL">
                                <li style="text-transform: uppercase;" class="displayAddressFullName"><%=loggedUser.getFirstname()%> <%=loggedUser.getSurname()%></li>
                                <li class="displayAddressAddressLine1"><%=deliveryAddress.getAddress()%></li>
                                <li class="displayAddressCityStateOrRegionPostalCode"> <%=deliveryAddress.getCity()%> <%=deliveryAddress.getPostalcode()%></li>
                                <li class="displayAddressCountryName"><%=deliveryAddress.getCountry()%></li>
                            </ul>

                        </div>
                    </div>

                    <div class="displayPaymentDiv">

                        <div style="float:left; margin-right:40px; margin-top: 5px;">
                            <h3 style="margin-right:7px">Terms of payment<a href="Dispatcher?controllerAction=InfoUserManagement.changeBillingInfoView" style="font-size:10px;"> modify</a></h3>

                            <div style="float:left; margin-right: 10px">

                                <img class= "clearfix" src="images/paylogos/<%=billingInfo.getNetworkCard()%>.png" style="float:left; width: 30px; height: 25px; margin-right: 7px">
                                <p>Card ends with<%=" "%> <%=": " + billingInfo.getCardNum()%></p>

                            </div>
                        </div>

                        <% if (coupon!= null) {%>

                        <div id="Coupon_inserition" style="">

                            <h3>Coupons, promotional codes</h3>

                            <h2>you inserted this Coupon <%= coupon.getCode()%> with a discount of <%=discount%>% </h2>

                        </div>

                        <%} else {%>

                        <div id="Coupon_inserition" style=" ">

                            <h3>Coupons, promotional codes</h3>

                            <form name="couponCodeForm" method="post" action="Dispatcher?controllerAction=PurchaseManagement.orderSummary">
                                <input type="text" autocomplete="off" class="clearfix"
                                       name="couponCode" maxlength="25" id="Coupons" placeholder="Insert the code"
                                       style=" height: 20px; border-radius: 7px; padding: 4px ">

                                <button class="submit click userbutton" type ="submit">   
                                    Control Coupon
                                </button>
                            </form>

                        </div>

                        <% }%>


                    </div>


                </div>

            </aside>
            <div class=" right portal" style="width: 25%; margin-right: 21px; border: 1px white solid" >

                <div class="head" style="">
                    <h3 style="text-align: center"> Pagamento </h3>
                </div> 
                <div id="costsPurchase" class="clearfix"> 
                    <h3 style=" "> 
                        Subtotal <%=numberOfProducts%> items
                        <p style="">€<%=df.format(totalPrice)%></p>
                    </h3>
                    <h3 style=" "> 
                        Spedition cost for 
                        <p style="">€<%=df.format(shippingPrice)%></p>
                    </h3>

                    <h3 style="  border-top: 0.5px white dotted; padding-top: 10px;     ">
                        New Total cost :
                        <p style="">€<%=df.format(totalPriceWithSpedition)%></p>
                    </h3>
                </div>
                <div class="aside_body_a " style="overflow: hidden; text-align: center; margin-bottom: 10px ">
                    <button id=confirmB class="userbutton click" style=""
                            onclick="window.location.href =
                                    'Dispatcher?controllerAction=PurchaseManagement.Purchase' +
                                    '&couponId=<%=coupon!=null? coupon.getCouponId():""%>'+'&totalSpending=<%=totalPrice%>';">
                        Purchase 
                    </button>
                </div>
            </div>

        </div>
        <%@include file="/include/footer.inc"%>
    </div>

    </body>

</html>


