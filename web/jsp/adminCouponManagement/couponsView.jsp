<%@page import="model.mo.Coupon" %>
<%@page import="model.mo.Customer" %>
<%@page import="model.mo.Order" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.mo.ProductInCart" %>
<%@page import="java.util.List" %>
<%@ page import="model.session.mo.LoggedAdmin" %>

<%-- 
    Document   : couponsView
    Created on : 22-lug-2019, 9.31.27
    Author     : simon70
--%>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");

    ArrayList<Coupon> couponList = (ArrayList<Coupon>) request.getAttribute("couponList");
    //Coupon coupon = new Coupon();
%>

<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .User_info_box {

            height: max-content;

        }


        #body_Info {
            margin-left: 45px;
            height: 220px;
            width: 170px;

        }

        .OrdersDiv div {

        }

        .OrdersDiv div > h3 {
            float: left;
            color: #0066cc;
        }

        .OrdersDiv div p {
            color: whitesmoke;
            font-size: 17px;
        }

        .aside_body_a li :hover {
            color: red;
        }

        #DisableButton {
            margin-left: 40px;
        }
    </style>

</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>
        <div id="principle"
             style="margin-top: 20px;border: 1px white solid;width: 73%;margin-right: 20px; margin-left: 15%;">

            <div class="OrdersDiv portal ">

                <div class="head">

                    <h3 style="color:white">Coupons</h3>

                </div>

                <form name="newButtonForm" style="">
                    <input type="button" id="newButton" name="newButton" class="newButton userbutton click"
                           value="+ New Coupon"
                           onclick="window.location.href = 'Dispatcher?controllerAction=AdminCouponManagement.addCouponView'">
                </form>


                <div class="" style=" display: block; overflow: auto; padding:2% ">


                    <% int j = 0;
                        for (Coupon coupon : couponList) {%>


                    <div id="body_Info"
                         style=" overflow: hidden; border:2px whitesmoke outset; padding: 10px; border-radius: 20px; float: left; margin: 10px">

                        <p style="margin:20px ;color: gold; text-align: center; font-size: 22px; font-weight: 900;">
                            Coupon <%=++j%>
                        </p>

                        <div id="coupon Code">

                            <h3>Code: </h3>
                            <p><%= coupon.getCode()%>
                            </p>

                        </div>

                        <div id="coupon Id">

                            <h3>Coupon ID: </h3>
                            <p><%= coupon.getCouponId()%>
                            </p>

                        </div>


                        <div id="coupon Discount" style="margin-bottom:20px">

                            <h3>Discount: </h3>
                            <p><%= coupon.getDiscountPercent()%>%</p>

                        </div>

                        <%if (!coupon.isDisabled()) {%>
                        <button id="DisableButton" style=" " class="userbutton click"
                                onclick="window.location.href = 'Dispatcher?controllerAction=AdminCouponManagement.couponDisable&couponId=<%=coupon.getCouponId()%>';">
                            Disable
                        </button>
                        <%} else {%>
                        <button id="DisableButton" style=" " class="userbutton click"
                                onclick="window.location.href = 'Dispatcher?controllerAction=AdminCouponManagement.couponUnDisable&couponId=<%=coupon.getCouponId()%>';">
                            UnDisable
                        </button>
                        <%}%>

                    </div>


                    <%}%>

                </div>

            </div>

        </div>

    </main>
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>
