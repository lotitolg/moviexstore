<%@page import="model.mo.Customer" %>
<%@page import="model.mo.Order" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.mo.ProductInCart" %>
<%@page import="java.util.List" %>
<%@ page import="model.session.mo.LoggedAdmin" %>

<%-- 
    Document   : couponInsert
    Created on : 23-lug-2019, 0.15.14
    Author     : simon70
--%>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");

%>

<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .User_info_box {

            height: max-content;

        }


        #body_Info {

            margin-left: 20px;
            margin-top: 20px;
            margin-right: 20px;
        }

        .OrdersDiv div {

            clear: both;

        }

        .OrdersDiv div > h3 {
            float: left;
            color: #0066cc;
        }

        .OrdersDiv div p {
            color: whitesmoke;
            font-size: 17px;
        }

        .OrdersDiv input {
            font-size: 14px;
            border-radius: 10px;
            padding: 2px;
            margin-left: 6px;
        }


        .aside_body_a li :hover {
            color: red;
        }

        #orderCustomerId a {
            font-weight: 900;
            color: skyblue;
        }

        #orderCustomerId a:hover {
            color: red;
        }


    </style>

</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>
        <div id="principle"
             style="margin-top: 20px;border: 1px white solid;width: 73%;margin-right: 20px; margin-left: 15%;">

            <div class="OrdersDiv portal ">

                <div class="head">

                    <h3 style="color:white">Coupon Insertion</h3>

                </div>


                <div class="order" style="">


                    <div id="body_Info" style=" overflow: hidden;">

                        <form id="Coupon_insert_form" method="post"
                              action="Dispatcher?controllerAction=AdminCouponManagement.addCoupon"></form>

                        <div class="OrdersDiv" style=" margin: 20px;">

                            <h3>Coupon Code : </h3>
                            <input name="couponCode" type="text" style="margin-left:35px" maxlength="10"
                                   form="Coupon_insert_form">

                        </div>

                        <div class="OrdersDiv" style=" margin: 20px;">

                            <h3>Coupon Discount : </h3>
                            <input name="couponDiscount" type="text" style="" maxlength="10" form="Coupon_insert_form">

                        </div>

                        <div class="field clearfix" style="margin-left:10px; margin-top:30px">
                            <label>&nbsp;</label>
                            <input name="submitButton" style="padding:6px" class="userbutton click" value="Submit"
                                   type="submit" form="Coupon_insert_form">
                            <input name="backButton" style="padding:6px" class="userbutton click" value="Cancel"
                                   type="reset" form="Coupon_insert_form">
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </main>
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>
