<%@ page import="model.session.mo.LoggedAdmin" %>
<%@ page import="model.mo.Genre" %>
<%@ page import="model.mo.AdminConfiguration" %>
<%@ page import="java.util.List" %>
<%@ page import="services.config.Configuration" %>
<%@page session="false" %>

<%

    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");
    String applicationMessage = (String) request.getAttribute("applicationMessage");

    List<AdminConfiguration> adminConfigurationList = (List<AdminConfiguration>) request.getAttribute("adminConfigurationList");

    String[] labels = new String[adminConfigurationList.size()];
    int i = 0;
    for (AdminConfiguration adminConfiguration : adminConfigurationList) {
        switch (adminConfiguration.getName()) {
            case Configuration.ADMIN_MULTIPLE_VIEW_CONFIG_NAME:
                labels[i] = "Number of admin catalog items per page";
                break;
            case Configuration.CUSTOMER_MULTIPLE_VIEW_CONFIG_NAME:
                labels[i] = "Number of customer catalog items per page";
                break;
            case Configuration.SHIPPING_PRICE_CONFIG_NAME:
                labels[i] = "Set shipping price";
                break;
        }
        i++;
    }


%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <style>

        .field {
            margin: 50px 0;
        }

        label {
            float: left;
            width: 15%;
            margin-right: 10px;
            padding-top: 3px;
            text-align: left;
        }

        input {
            border: none;
            border-radius: 4px;
            padding: 3px;
            box-shadow: 0 1px 0 rgba(0, 0, 0, 0.03) inset;
        }

        input:focus {
            background: #d2d9dd;
            outline-color: #a3271f;
        }

        #insModFormSection {
            margin: 30px;
        }

    </style>
    <script>
        function goback() {
            document.backForm.submit();
        }

        function mainOnLoadHandler() {
            document.insModForm.backButton.addEventListener("click", goback);
        }
    </script>
</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>


        <div class="box">

            <div class="head">
                <h3>
                    Configuration
                </h3>
            </div>

            <section id="insModFormSection">
                <form name="insModForm" action="Dispatcher" method="post">

                    <%
                        i = 0;
                        for (AdminConfiguration adminConfiguration : adminConfigurationList) {
                    %>
                    <div class="field clearfix">
                        <label for="<%=adminConfiguration.getName()%>"><%=labels[i]%>
                        </label>
                        <input type="text" id="<%=adminConfiguration.getName()%>"
                               name="<%=adminConfiguration.getName()%>"
                               value="<%=adminConfiguration.getValue()%>"
                               required size="20" maxlength="50"/>
                    </div>
                            <%i++;}%>


                        <div class="field clearfix">
                            <label>&#160;</label>
                            <input type="submit" name="submitButton" class="userbutton click" value="Submit"/>
                            <input type="button" name="backButton" class="userbutton click" value="Cancel"/>
                        </div>
                        <input type="hidden" name="controllerAction" value="ConfigurationManagement.modify">
                </form>
            </section>

            <form name="backForm" method="post" action="Dispatcher">
                <input type="hidden" name="controllerAction" value="AdminHomeManagement.adminHomeView"/>
            </form>
        </div>

    </main>
        <%@include file="/include/footer.inc" %>
</div>
</body>

</html>
