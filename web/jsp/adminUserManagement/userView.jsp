<%@page import="java.util.ArrayList" %>
<%@page import="model.session.mo.LoggedAdmin" %>
<%@page import="model.mo.Customer" %>
<%@ page import="java.util.List" %>
<%@ page import="model.mo.Admin" %>

<%-- 
    Document   : searchForCustomer
    Created on : 19-lug-2019, 14.52.40
    Author     : simon70
--%>


<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");

    String selectedInitial = (String) request.getAttribute("selectedInitial");
    List<String> initials = (List<String>) request.getAttribute("initials");
    Boolean waitApprove = (Boolean) request.getAttribute("waitApprove");
    String searchStirng = (String) request.getAttribute("searchString");

    List<Admin> adminList = (List<Admin>) request.getAttribute("admins");
    ArrayList<Customer> customersList = (ArrayList<Customer>) request.getAttribute("customersList");
    String entity = customersList != null ? "customer" : "admin";
%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <title>search for customer</title>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .info_box {

            height: max-content;
            border: 1px white solid;

        }

        #body_Info {

            margin: 20px;

        }

        .multi-field {
            clear: left;
            margin: 20px;
        }

        .multi-field > div {

            display: inline-block;
            margin: 10px;
            width: 18%;
            border: dotted 1px;
            padding: 6px;
            border-radius: 10px;
        }

        .multi-field div h3 {

            color: deepskyblue;
        }

        .aside_body_a li :hover {
            color: red;
        }

        .multi-field div input {

            font-size: 16px;
            border-radius: 10px;
            padding: 2px;
            margin-top: 6px;

        }

        #initialSelector {
            margin: 12px;
            font-size: 18px;
            background: #313131;
        }

        .initial {
            color: white;
            letter-spacing: 2px;
        }

        .selectedInitial {
            color: red;
            letter-spacing: 2px;
        }

        #search {
            float: left;
            margin: 15px 23px;
            border: 1px white outset;
            padding: 5px;
            border-radius: 13px;
            background-color: #690101;
            width: auto;
        }

    </style>
    <script>

        function changeInitial(inital) {
            document.changeInitialForm.selectedInitial.value = inital;
            document.changeInitialForm.submit();
        }

        <%if(entity.equals("admin")) {%>

        function approveAndDelete(action, adminId) {
            document.approveAndDeleteForm.adminId.value = adminId;
            document.approveAndDeleteForm.controllerAction.value = "AdminUserManagement." + action + "Admin";
            document.approveAndDeleteForm.submit();
        }

        function filterWaitApprove(event) {
            document.filterWaitApproveForm.waitApprove.value = event.target.checked;
            document.filterWaitApproveForm.submit();
        }

        function mainOnLoadHandler() {
            var check = document.getElementById("waitApprove");
            check.addEventListener("click", filterWaitApprove);
        }

        <%}%>
    </script>
</head>
<body>
<div class="portal">
<%@include file="/include/adminHeader.inc" %>

<main>

    <div id="principle" style="margin: 60px;">

        <div class="info_box portal">
            <div class="head">
                <h3 style="text-transform: capitalize"><%=entity%>s</h3>
                <p class="text-right" style=" padding-right: 20px;"></p>
            </div>

            <nav id="initialSelector">
                <%if (selectedInitial.equals("*")) { %>
                <span class="selectedInitial">*</span>
                <%} else {%>
                <a class="initial" href="javascript:changeInitial('*');">*</a>
                <%}%>
                <%
                    for (int i = 0; i < initials.size(); i++) {
                        if (initials.get(i).equals(selectedInitial)) {
                %>
                <span class="selectedInitial"><%=initials.get(i)%></span>
                <%} else {%>
                <a class="initial" href="javascript:changeInitial('<%=initials.get(i)%>');"><%=initials.get(i)%>
                </a>
                <%}%>
                <%}%>
            </nav>

            <div id="search">
                <form action="Dispatcher" method="get" accept-charset="utf-8">
                    <label for="search-field" style="color: #f8e9bd">SEARCH</label>
                    <input type="text" name="searchString" placeholder="Insert firstname and/or surname"
                           id="search-field" title="Enter search here" class="blink search-field">
                    <button type="submit" class="icon"><i class="fa fa-search click"></i></button>
                    <input type="hidden" name="controllerAction" value="AdminUserManagement.<%=entity%>View">
                    <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>">
                </form>
            </div>

            <%if (entity.equals("admin")) {%>
            <div style="margin-top: 40px;">
                <span class="name-check">Waiting to be approved</span>
                <input type="checkbox" id="waitApprove" name="waitApprove" value="<%=waitApprove%>"
                    <%=waitApprove? "checked" : ""%>>
            </div>
            <%}%>

            <div class="multi-field">

                <% if (entity.equals("customer"))
                    for (Customer customer : customersList) {%>

                <div class="user">

                    <% if (customer.isBanned() || customer.isDeleted()) {%>
                    <span>
                    <p style="color: red;float: right;margin-right:15px"><%=customer.isBanned() ? "Banned" : "Deleted"%></p>
                </span>
                    <%}%>

                    <div class="UserName-field">

                        <h3 for="" class="">
                            Username:
                            <span style="color:white">
                            <%=" " + customer.getUsername()%>
                        </span>
                        </h3>
                    </div>


                    <div class="name-field">

                        <h3 for="" class="">
                            First Name: <span style="color:white"><%=" " + customer.getFirstname()%></span>
                        </h3>

                    </div>

                    <div class="Lastname-field">
                        <h3 class="">Last Name:<span style="color:white"><%=" " + customer.getSurname()%></span></h3>
                    </div>

                    <button style="float:right;margin-right: 10px" class="userbutton click"
                            onclick="window.location.href = 'Dispatcher?controllerAction=AdminUserManagement.customerInfoView&customerId=<%=customer.getCustomerId()%>';">
                        More Info
                    </button>

                </div>

                <%
                    }
                else {
                %>
                <% for (Admin admin : adminList) {%>

                <div class="user" style="">

                    <% if (!admin.isApproved()) {%>
                    <span>
                    <i class="fa fa-asterisk" style="color: red;float: right;margin-right:15px"></i>
                </span>
                    <%}%>
                    <div class="UserName-field">

                        <h3 for="" class="">
                            Username:
                            <span style="color:white">
                            <%=" " + admin.getUsername()%>
                        </span>
                        </h3>
                    </div>


                    <div class="name-field">

                        <h3 for="" class="">
                            First Name: <span style="color:white"><%=" " + admin.getFirstname()%></span>
                        </h3>

                    </div>

                    <div class="Lastname-field">
                        <h3 class="">Last Name:<span style="color:white"><%=" " + admin.getSurname()%></span></h3>
                    </div>

                    <% if (!admin.isApproved()) {%>
                    <button onclick="approveAndDelete('approve','<%=admin.getAdminId()%>');"
                            style="float:left;margin-right: 10px" class="userbutton click">
                        Approve
                    </button>
                    <%}%>
                    <button onclick="approveAndDelete('delete','<%=admin.getAdminId()%>');"
                            style="float:right;margin-right: 10px" class="userbutton click">
                        Delete
                    </button>
                </div>

                <%
                        }
                    }
                %>
            </div>
        </div>
    </div>
    <form name="changeInitialForm" method="post" action="Dispatcher">
        <input type="hidden" name="selectedInitial"/>
        <input type="hidden" name="controllerAction" value="AdminUserManagement.<%=entity%>View"/>
    </form>
    <%if (entity.equals("admin")) {%>
    <form name="filterWaitApproveForm" method="post" action="Dispatcher">
        <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>"/>
        <input type="hidden" name="controllerAction" value="AdminUserManagement.adminView"/>
        <input type="hidden" name="waitApprove">
    </form>

    <form name="approveAndDeleteForm" method="post" action="Dispatcher">
        <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>"/>
        <input type="hidden" name="waitApprove" value="<%=waitApprove%>">
        <input type="hidden" name="controllerAction"/>
        <input type="hidden" name="adminId">
    </form>
    <%}%>
</main>
<%@include file="/include/footer.inc" %>
</div>
</body>
</html>
