<%@page import="model.mo.Customer" %>
<%@page import="model.mo.DeliveryAddress" %>
<%@ page import="model.session.mo.LoggedAdmin" %>

<%-- 
    Document   : adminCustomerManagement
    Created on : 15-lug-2019, 18.19.05
    Author     : simon70
--%>

<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");
    DeliveryAddress deliveryAddress = (DeliveryAddress) request.getAttribute("deliveryAddress");
    Customer customer = (Customer) request.getAttribute("customer");
%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <title>Customer View</title>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .info_box {

            height: max-content;
            border: 1px white solid;

        }

        #body_Info {

            margin: 20px;

        }

        .multi-field > div {

            margin-bottom: 10px;
            display: inline-grid;
            margin: 10px;
        }

        .multi-field div h3 {

            color: deepskyblue;
        }

        .aside_body_a li :hover {
            color: red;
        }

        .multi-field div input {

            font-size: 16px;
            border-radius: 10px;
            padding: 2px;
            margin-top: 6px;

        }


    </style>
</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>
        <div id="principle" style="margin: 60px;">

            <div class="info_box portal">
                <div class="head">
                    <h3 style="">customer Info</h3>
                    <p class="text-right" style=" padding-right: 20px;"></p>
                </div>

                <div class="multi-field" style=" display: inline-grid;">

                    <div class="ID-field">

                        <h3 for="" class="">
                            User ID:
                            <span style="color:white">

                                <%=" " + customer.getCustomerId()%>
                            </span>
                        </h3>
                    </div>

                    <div class="UserName-field">

                        <h3 for="" class="">
                            Username:
                            <span style="color:white">

                                <%=" " + customer.getUsername()%>
                            </span>
                        </h3>
                    </div>


                    <div class="name-field">

                        <h3 for="" class="">
                            First Name: <span style="color:white"><%=" " + customer.getFirstname()%></span>
                        </h3>

                    </div>

                    <div class="Lastname-field">
                        <h3 class="">Last Name:<span style="color:white"><%=" " + customer.getSurname()%></span></h3>
                    </div>

                    <%-- _____country_____ --%>
                    <div class="" style="">
                        <h3 class="" for="location-country">Country/Region:
                            <span style="color:white">
                                <%=" " + deliveryAddress.getCountry()%>
                            </span>
                        </h3>
                    </div>

                    <div class="">
                        <h3 for="city" class=""> City:
                            <span style="color:white">
                                <%=deliveryAddress.getCity()%>
                            </span>
                        </h3>
                    </div>


                    <div class="">
                        <h3 for="zipcode" class="">ZIP code:
                            <span style="color:white">
                                <%=deliveryAddress.getPostalcode()%>
                            </span>
                        </h3>

                    </div>

                    <div class="Delivery_Address">

                        <h3 class="">Delivery Address:
                            <span style="color:white">

                                <%=deliveryAddress.getAddress()%>
                            </span>
                        </h3>

                    </div>


                    <div class="Email-field">
                        <h3 for="" class="">
                            Email:
                            <span style="color:white">
                                <%=" " + customer.getEmail()%>
                            </span>
                        </h3>
                    </div>

                    <div class="deleted-field">

                        <h3 for="" class="">
                            deleted:
                            <span style="color:white">
                                <%if (customer.isDeleted()) {%> <%="Yes"%> <%} else {%>
                                <%="No"%>
                                <%}%>

                            </span>
                        </h3>
                        <%if (!customer.isDeleted()) {%>
                        <div style="float: right ; margin-top: -30px">
                            <form id="Delete" style=" display: none;" method="post"
                                  action="Dispatcher?controllerAction=AdminUserManagement.deleteCustomer&customerId=<%=customer.getCustomerId()%>">
                            </form>

                            <input class="login_Botton click"
                                   style="float: right; width: 80px"
                                   type="submit" value="Delete" form="Delete"
                            >
                        </div>
                        <%} else {%>
                        <div style="float: right ; margin-top: -25px">
                            <form id="unDelete" style=" display: none;" method="post"
                                  action="Dispatcher?controllerAction=AdminUserManagement.unDeleteCustomer&customerId=<%=customer.getCustomerId()%>">
                            </form>

                            <input class="login_Botton click"
                                   style="float: right; width: 80px"
                                   type="submit" value="unDelete" form="unDelete"
                            >
                        </div>
                        <%}%>
                    </div>

                    <div class="banned-field">

                        <h3 for="" class="">
                            Banned:
                            <span style="color:white">
                                <%if (customer.isBanned()) {%> <%="Yes"%> <%} else {%>
                                <%="No"%>
                                <%}%>

                            </span>
                        </h3>

                        <%if (!customer.isBanned()) {%>
                        <div style="float: right ; margin-top: -30px">
                            <form id="Ban" style=" display: none;" method="post"
                                  action="Dispatcher?controllerAction=AdminUserManagement.banCustomer&customerId=<%=customer.getCustomerId()%>">
                            </form>

                            <input class="login_Botton click"
                                   style="float: right; width: 80px"
                                   type="submit" value="Ban" form="Ban"
                            >
                        </div>
                        <%} else {%>
                        <div style="float: right ; margin-top: -25px">
                            <form id="unBan" style=" display: none;" method="post"
                                  action="Dispatcher?controllerAction=AdminUserManagement.unBanCustomer&customerId=<%=customer.getCustomerId()%>">
                            </form>

                            <input class="login_Botton click"
                                   style="float: right; width: 80px"
                                   type="submit" value="unBan" form="unBan"
                            >
                        </div>
                        <%}%>


                    </div>

                    <div>
                        <h3>
                            Orders:
                            <a class="login_Botton click"
                               href="Dispatcher?controllerAction=AdminUserManagement.showCustomerOrders&customerId=<%=customer.getCustomerId()%>">
                                Go to Orders</a>
                        </h3>
                    </div>

                </div>
            </div>
        </div>
    </main>
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>
