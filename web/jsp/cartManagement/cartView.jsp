<%@page import="java.util.List" %>
<%@page import="model.mo.ProductInCart" %>
<%@ page import="model.session.mo.LoggedUser" %>


<%-- 
    Document   : cartInfo
    Created on : 2-lug-2019, 18.15.31
    Author     : simon70
--%>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");
    List<ProductInCart> productInCart = (List<ProductInCart>) request.getAttribute("productsInCart");

    Boolean[] warningsAvailability = (Boolean[]) request.getAttribute("warningsAvailability");

    double totalPrice = 0;
    int numberOfProducts = 0;
    for (ProductInCart product : productInCart) {
        totalPrice = (product.getHomevideoProducts().getPrice()) * (product.getItemQuantity()) + totalPrice;
        numberOfProducts = numberOfProducts + product.getItemQuantity();
    }

    totalPrice = (double) Math.round(totalPrice * 100d) / 100d;


%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/include/htmlHead.inc" %>

        <style>
            .aside_body_a ul li {

                margin: 5px;
            }

            .User_info_box {

                height: max-content;

            }

            #body_Info {

                margin: 20px;

            }

            .aside_body_a li :hover {
                color: red;
            }

            .aside_body_a > button {
                font-size: 20px;
                margin: 7px;
                padding: 10%;
                border-color: whitesmoke;
                background: darkred;
                border-width: 2px;
                box-shadow: 1px 1px grey;

            }

            .aside_body_a > button:hover {
                font-size: 20px;
                margin: 6px;
                margin-bottom: 10px;
                padding: 10%;
                border-color: whitesmoke;
                background: darkred;
                border-width: 1px;
                box-shadow: none;

            }

            #aside1 {
                width: 20%;
                border: 1px whitesmoke solid;
                margin-top: 20px;
                margin-left: 21px;
                clear: left;
            }

            #aside2 {

                width: 20%;
                border: 1px whitesmoke solid;
                margin-left: 21px;
                clear: left;
                margin-top: 7%;
                position: sticky;
                top: 10px;
            }

            #removeButton {
                float: left;
                height: 40px;
                margin-right: 30px;
                margin-top: 45px;
            }

            #rightCart {
                float: right;
            }

            .price {
                float: right;
                margin: 60px 45px 60px 60px;
                padding-left: 45px;
                width: 80px;
            }

            .quantitySelect {
                margin: 10px;
                font-size: 14px;
                border-radius: 9px;
            }
            #principle{
                margin-top: 20px;
                margin-bottom: 10px;
                border: 1px white solid;
                width: 73%;
                float: right;
                margin-right: 20px;
            }


        </style>


        <script>
            function update(select, productid, oldQuantity) {
                var form = document.updateItemQuantityForm;
                form.productId.value = productid;
                form.quantity.value = select.value;
                form.oldQuantity.value = oldQuantity;
                form.submit();
            }

            function remove(productId, oldQuantity) {
                var form = document.removeItemForm;
                form.productId.value = productId;
                form.oldQuantity.value = oldQuantity;
                form.submit();
            }

            function mainOnLoadHandler() {

                var selects = document.getElementsByClassName("quantitySelect");
                var nullOption = document.createElement("option");
                var checkoutButton = document.getElementById("checkoutButton");
                nullOption.textContent = "-";
                nullOption.disalbed = true;
                nullOption.selected = true;
                nullOption.hidden = true;
            <% for (int i = 0; i < warningsAvailability.length; i++) {
                    if (warningsAvailability[i]) {%>
                selects[<%=i%>].options[0].selected = false;
                selects[<%=i%>].appendChild(nullOption);
                checkoutButton.disabled = true;
                checkoutButton.style.color = 'grey';
            <% }
                } %>
            <% if (productInCart.size() == 0) {%>   <%--_____disabilito se carello vuoto____--%>
                checkoutButton.disabled = true;
                checkoutButton.style.color = 'grey';
                <%}%>
            }


        </script>


    </head>
    <div class="portal">
    <body>
        <%@include file="/include/header.inc" %>

        <%-- asid bar with all the modifications--%>

        <%@include file="/include/aside.inc"%>


        <%-- _________User Info Setup______ --%>

        <div id="principle">


            <div class="User_info_box portal">
                <div class="head">
                    <h3 style="">Cart</h3>
                    <p class="text-right" style=" padding-right: 20px;"></p>
                </div>

                <%if(productInCart.size()==0){%>
                <h1 style="margin: 20px;"> Your cart is empty &#128542;</h1>
                <% }else {%>
                <div class="clearfix" style=" border-bottom: 0.5px white dotted; border-radius: 10px;">
                    <h3 style="margin: 12px;float:left"> Product: </h3>
                    <h3 style="float: right; font-size: 18px; margin: 12px; margin-right: 70px;"> Price:</h3>
                </div>
                <%}%>

                <% int i = 0;
                    for (ProductInCart product : productInCart) {%>

                <%--_________ Movie ____________--%>
                <div class="classgen clearfix " style="border-bottom: 0.5px white dotted;border-radius: 10px;">

                    <img style="clear: left;float:left; margin :10px ; width: 100px"
                         src="images/<%=product.getHomevideoProducts().getPicture_url()%> ">


                    <div class="class1" style="float: left;display: flex; margin-top: 20px;">

                        <div class="class2" style=" margin: 20px;">
                            <p class="" style="text-align: left; margin: 5px">
                                <span>Name: </span><%=product.getHomevideoProducts().getName()%>
                            </p>

                            <label for="quantity" style="text-align: left; margin: 10px;font-size: 14px;">Number: </label>

                            <select onchange="update(this,<%=product.getHomevideoProducts().getProductId()%>,
                                    <%=product.getItemQuantity()%>)"
                                    id="quantity" name="quantity" class="quantitySelect">
                                <% for (int j = 1; j <= product.getHomevideoProducts().getAvailable(); j++) {%>
                                <option value="<%=j%>" <%if (product.getItemQuantity() == j) {%>
                                        selected<% }%>><%=j%>
                                </option>
                                <%}%>
                            </select>

                            <p class="type" style="color:greenyellow">
                                <span>Availabilty : </span><%=product.getHomevideoProducts().getAvailable()%>
                            </p>
                            <% if (warningsAvailability[i]) {%>
                            <p class="type" style="color:red;margin-top: 10px">
                                WARNING! The previous quantity is no longer available.</br>
                                Modify quantity o remove the product from cart</p>
                                <% }%>
                        </div>

                    </div>

                    <span id="rightCart" class="clearfix">

                        <div id="removeButton">
                            <input onclick="remove(<%=product.getHomevideoProducts().getProductId()%>,<%=product.getItemQuantity()%>)"
                                   class="userbutton click" type="button" value="Remove">
                        </div>

                        <div class="price">

                            <h3>
                                € <%=Math.round((product.getHomevideoProducts().getPrice()) * (product.getItemQuantity()) * 100d) / 100d%>
                            </h3>

                        </div>
                    </span>


                </div>
                <%-- End Movie --%>

                <% i++;
                    }%>

                <%if(productInCart.size()>0){%>
                <div class="clearfix" style="float: right;display: contents;font-size: 21px;">

                    <h3 style="float: right;display: inline-flex;margin-top: 10px;font-size: inherit;margin-bottom: 10px;">
                        Subtotal (<%=numberOfProducts%> item):
                        <p style="color: red;margin-left: 17px;margin-right: 40px;font-size: 22px; color: #40ff00;">
                            € <%=totalPrice%>
                        </p>
                    </h3>
                </div>
                <%}%>

            </div>

        </div>

        <%if(productInCart.size()>0) { %>

        <aside id="aside2" class="left portal" >

            <div class="head" style="">
                <h3 style="text-align: center"> Pagamento </h3>
            </div>
            <h3 style="    margin-top: 10px;font-size: 17px;margin-bottom: 10px;text-align: center;">
                Subtotal (<%=numberOfProducts%> item):
                <p style="color: red;margin-left: 40px;margin-right: 40px; font-size: 22px ; color: #40ff00 ;">€ <%=totalPrice%>
                </p>
            </h3>
            <div class="aside_body_a " style="overflow: hidden; text-align: center; margin-bottom: 10px ">

                <button id="checkoutButton" class="userbutton click"
                        onclick="window.location.href = 'Dispatcher?controllerAction=PurchaseManagement.orderSummary';">
                    Go to Check Out
                </button>

            </div>


        </aside>
        <%}%>

        <form name="updateItemQuantityForm" method="post" action="Dispatcher">
            <input type="hidden" name="controllerAction" value="CartManagement.updateItemQuantity">
            <input type="hidden" name="quantity" value="">
            <input type="hidden" name="productId" value="">
            <input type="hidden" name="oldQuantity" value="">
        </form>

        <form name="removeItemForm" method="post" action="Dispatcher">
            <input type="hidden" name="controllerAction" value="CartManagement.removeItem">
            <input type="hidden" name="productId" value="">
            <input type="hidden" name="oldQuantity" value="">
        </form>

        <!--footer -->
        <%@include file="/include/footer.inc" %>
    </body>
    </div>
</html>
