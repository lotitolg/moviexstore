<%@page import="model.mo.Customer" %>
<%@page import="model.mo.Order" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.mo.ProductInCart" %>
<%@page import="java.util.List" %>
<%@ page import="model.session.mo.LoggedAdmin" %>

<%-- 
    Document   : ProductsTracking
    Created on : 20-lug-2019, 20.54.13
    Author     : simon70
--%>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedAdmin loggedAdmin = (LoggedAdmin) request.getAttribute("loggedAdmin");

    List<Order> orderList = (ArrayList<Order>) request.getAttribute("orderList");


    int j = 0;
%>

<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .User_info_box {

            height: max-content;

        }


        #body_Info {

            margin: 20px;

        }

        .OrdersDiv div {

            margin-bottom: 10px;
        }

        .OrdersDiv div > h3 {
            float: left;
            color: #0066cc;
        }

        .OrdersDiv div p {
            color: whitesmoke;
            font-size: 17px;
        }

        .aside_body_a li :hover {
            color: red;
        }

        #orderCustomerId a {
            font-weight: 900;
            color: skyblue;
        }

        #orderCustomerId a:hover {
            color: red;
        }


    </style>

</head>
<body>
<div class="portal">
    <%@include file="/include/adminHeader.inc" %>
    <main>
        <div id="principle"
             style="margin-top: 20px;border: 1px white solid;width: 73%;margin-right: 20px; margin-left: 15%;">

            <div class="OrdersDiv portal ">

                <div class="head">

                    <h3 style="color:white">Orders</h3>

                </div>

                <%if (orderList.size() == 0) {%>
                <h1 style="margin: 20px;"> No results &#128542;</h1>
                <%}%>

                <% for (Order order : orderList) {%>

                <div class="order">

                    <div id="body_Info" style=" overflow: hidden;">


                        <div id="orderID">

                            <h3>Order ID : </h3>
                            <p><%=order.getOrderId()%>
                            </p>

                        </div>

                        <div id="orderDate">

                            <h3>Order Date : </h3>
                            <p><%=order.getOrderDate()%>
                            </p>

                        </div>

                        <div id="orderStatus">

                            <h3>Order status : </h3>
                            <%if (order.getOrderStatus().equals("In progress")) {%><p style="color:red"> In
                            progress</p><%}%>
                            <%if (order.getOrderStatus().equals("shipped")) {%><p style="color:gold"> shipped</p><%}%>
                            <%--<%if (order.getOrderStatus().equals("delivered")) {%><p style="color:greenyellow"> delivered</p><%}%>--%>

                            <form id="ChangeStatus<%=j%>" style=" display: none;" method="post"
                                  action="Dispatcher?controllerAction=AdminTrackingManagement.modifyCustomerOrderStatus&customerId=<%=order.getCustomer().getCustomerId()%>&orderId=<%=order.getOrderId()%>">
                            </form>

                            <input class="login_Botton click"
                                   style="float: right; width: 170px"
                                   type="submit" value="Change Status" form="ChangeStatus<%=j%>"
                            >

                        </div>

                        <div id="orderCustomer">

                            <h3>Customer Name : </h3>
                            <p style="text-transform : uppercase"><%= order.getCustomer().getFirstname() + " " + order.getCustomer().getSurname()%>
                            </p>
                        </div>

                        <div id="orderCustomerId">
                            <h3>Customer ID : </h3>
                            <p style="text-transform : uppercase"><%= order.getCustomer().getCustomerId()%>
                                <a style=" font-size: 10px"
                                   href="Dispatcher?controllerAction=AdminCustomerManagement.customerInfo&customerId=<%=order.getCustomer().getCustomerId()%>">
                                    go to profile</a>
                        </div>


                        <div id="orderDeliveryAddress">

                            <h3>Address :</h3>
                            <p><%=order.getAddress().getCountry() + ", " + order.getAddress().getCity() + ", " + order.getAddress().getAddress() + ", code (" + order.getAddress().getPostalcode() + ")"%>
                            </p>

                        </div>


                    </div>
                </div>
                <%
                        j++;
                    }
                %>
            </div>
        </div>
    </main>
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>
