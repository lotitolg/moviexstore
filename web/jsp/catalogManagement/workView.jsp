<%@ page import="model.session.mo.LoggedUser" %>
<%@ page import="model.mo.*" %>
<%@ page import="java.util.List" %>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");

    AudiovisionWork audiovisionWork = (AudiovisionWork) request.getAttribute("audiovisionWork");

    /*Filters*/
    String[] typeFilter = null;
    String[] formatFilter = null;
    String[] genreFilter = null;
    try {
        typeFilter = (String[]) request.getAttribute("typeFilter");
    } catch (Exception ignored) {
    }
    try {
        formatFilter = (String[]) request.getAttribute("formatFilter");
    } catch (Exception ignored) {
    }
    try {
        genreFilter = (String[]) request.getAttribute("genreFilter");
    } catch (Exception ignored) {
    }
    String entityFilter = (String) request.getAttribute("entityFilter");
    String orderFilter = (String) request.getAttribute("orderFilter");
    String descendant = (String) request.getAttribute("descendant");
    String searchString = (String) request.getAttribute("searchString");
    String thisPage = (String) request.getAttribute("thisPage");

%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>

    <style>

        #principle {
            clear: none;
        }

        .movie_info_box {
            width: 900px;
            margin: 20px;
            border: 1px activecaption solid;
            border-radius: 0px;
        }

        .movie_info_box p.type {
            font-size: 14px;
            color: #fff;
            margin-bottom: 4px;
        }

        .movie_info_box p.type span {
            font-size: 16px;
            color: deepskyblue;
            margin-bottom: 4px;

        }

        .movie_info_box iframe {
            margin: 30px
        }

        .movie_info_body {

            margin-top: 40px;
        }

        .movie_info {

            margin: 40px;

        }

        .movie_info form {

            font-size: 15px;


        }

        .movie_info form input {

            font-size: 13px;
            border: 2px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;
        }

        .movie_info form input:hover, :checked {

            font-size: 12px;
            border: 1px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;

        }

        #secondary {
            display: none;
            margin: 60px;
        }

        .cast {
            float: left;
            margin: 25px 20px;
            padding: 1px;

        }

        .cast form input {

            font-size: 13px;
            border: 2px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;
        }

        .cast form input :hover :checked {

            font-size: 12px;
            border: 1px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;

        }

        .cast form label {

            font-size: 16px;
            color: deepskyblue;
            margin-bottom: 4px;


        }

        .cast div {
            margin-top: 7px;

        }

        .gotoCatalogButton {
            clear: none;
        }


    </style>
</head>
<script>


    function hello() {
        var elem = document.getElementById("principle");
        elem.style.display = "none";
        //  elem.style.visibility = "hidden";
        //  elem.parentNode.removeChild(elem);

        var elem3 = document.getElementById("secondary");
        elem3.style.display = "block";
    }

    function unhello() {
        var elem = document.getElementById("principle");
        elem.style.display = "block";
        //  elem.style.visibility = "hidden";
        //  elem.parentNode.removeChild(elem);

        var elem3 = document.getElementById("secondary");
        elem3.style.display = "none";
    }

    function mainOnLoadHandler() {

        var CartADD = document.querySelector("#buyHV");
        CartADD.addEventListener("click", hello);

        var CartCancel = document.querySelector("#annula");
        CartCancel.addEventListener("click", unhello);

    }

</script>
<body>

<div class="portal">
    <%@include file="/include/header.inc" %>
    <main id="main">
        <!--header -->


        <%-- _______Aside bar___________--%>

        <aside class="right portal clearfix"
               style="width: 20%; border: 1px whitesmoke solid; margin-top: 60px; margin-right: 3%;">

            <div class="head">
                <h3> CAST</h3>
                <div class="cast">

                    <h3 style="margin-top: 10px">Actors: </h3>

                    <div class="clearfix" style="float:left;margin-bottom:22px">
                        <% for (Actor actor : audiovisionWork.getActors()) {%>
                        <a href="Dispatcher?controllerAction=CatalogManagement.catalogView&searchString=<%=actor.getActorName()%>">
                            <p class="type" style="text-align: left;">-<%=actor.getActorName()%></p>
                        </a>

                        <%}%>
                    </div>

                    <h3 style="margin-top: 10px">Directors: </h3>

                    <div class="clearfix" style="float:left">
                        <% for (Director director : audiovisionWork.getDirectors()) {%>
                        <a href="Dispatcher?controllerAction=CatalogManagement.catalogView&searchString=<%=director.getDirectorName()%>">
                        <p class="type" style="text-align: left;">-<%=director.getDirectorName()%></p>
                        </a>
                        <%}%>
                    </div>
                </div>

            </div>

        </aside>

        <%-- ___________principle____________ --%>

        <div id="principle" style="margin: 60px;">

            <div class="movie_info_box portal">
                <div class="head">
                    <h3 style="">MOVIE INFO</h3>
                    <p class="text-right" style=" padding-right: 20px;"></p>
                </div>

                <div class="clearfix">
                    <h1 style="padding:10px"><%=audiovisionWork.getOriginalTitle()%>
                    </h1>
                    <img style="float:left; margin :30px ; width: 320px"
                         src="images/<%=audiovisionWork.getImageWork()%>">

                    <div class="movie_info_body">
                        <p class="type"><span>Type: </span><%=audiovisionWork.getType()%>
                        </p>
                        <p class="type"><span>Genre: </span>
                                <%for(Genre genre : audiovisionWork.getGenres()) { %>
                            <a href="#" title="<%=genre.getGenreName()%>"><%=genre.getGenreName()%>
                            </a>
                                <% } %>
                        <p class="type"><span>Studio Production: </span><%=audiovisionWork.getStudioProduction()%>
                        </p>
                        <p class="type"><span>Year Production: </span><%=audiovisionWork.getYearProdction()%>
                        </p>
                        <p class="type"><span>Rating★: </span><%=audiovisionWork.getRating()%>/10</p>
                        <%--<p class="type" style="color:greenyellow"><span>Availabilty : </span >Available </p> --%>
                        <%--<p class="type"><span>Price : </span>40€ </p> --%>
                        <p class="type"><span>Duration: </span><%=audiovisionWork.getDuration()%> min.</p>
                        <p class="type"><span>Plot Summary: </span><%=audiovisionWork.getPlot()%>
                        </p>

                        <div class="movie_info">
                            <form name="registerForm">

                                <input id="buyHV" class="userbutton click" type="button" value="Buy Homevideos">
                            </form>
                        </div>

                        <form name="goCatalogForm">
                            <button class="userbutton click gotoCatalogButton"
                                    style="margin-left:7px; margin-top:30px;">
                                &#8592; Go to Catalog
                            </button>
                            <input type="hidden" name="controllerAction" value="CatalogManagement.catalogView">
                            <%=entityFilter != null ? "<input type=\"hidden\" name=\"entityFilter\" value=\"" + entityFilter + "\">" : ""%>
                            <%=orderFilter != null ? "<input type=\"hidden\" name=\"orderFilter\" value=\"" + orderFilter + "\">" : ""%>
                            <%=orderFilter != null ? "<input type=\"hidden\" name=\"descendant\" value=\"" + descendant + "\">" : ""%>
                            <%=thisPage != null ? "<input type=\"hidden\" name=\"thisPage\" value=\"" + thisPage + "\">" : ""%>
                            <% if (searchString != null) { %>
                            <input type="hidden" id="searchString" name="searchString" value=<%=searchString%>>
                            <% } %>
                            <%
                                if (formatFilter != null) {
                                    for (String s : formatFilter) {
                            %>
                            <input type="hidden" name="formatFilter" value="<%=s%>">
                            <% }
                            }%>
                            <%
                                if (genreFilter != null) {
                                    for (String s : genreFilter) {
                            %>
                            <input type="hidden" name="genreFilter" value="<%=s%>">
                            <% }
                            }%>
                            <%
                                if (typeFilter != null) {
                                    for (String s : typeFilter) {
                            %>
                            <input type="hidden" name="typeFilter" value="<%=s%>">
                            <% }
                            }%>
                        </form>
                    </div>

                </div>


                <iframe width="853" height="480"
                        src=https://www.youtube.com/embed/<%=audiovisionWork.getTrailerUrl() + "?rel=0"%>
 frameborder="0"
                        allow="accelerometer; picture-in-picture"
                        allowfullscreen>

                </iframe>

            </div>
        </div>

        <div id="secondary">

            <div class="box clearfix portal" style="float:none">

                <div class="head">
                    <h3><%=audiovisionWork.getOriginalTitle()%>
                    </h3>
                    <p class="text-right" style=" padding-right: 20px;"></p>
                </div>

                <div class="box-movie clearfix">

                    <% for (HomevideoProduct product : audiovisionWork.getHomevideoProducts()) {%>
                    <!-- Movie -->
                    <div class="movie">

                        <div class="movie-image">
                            <a href="Dispatcher?controllerAction=CatalogManagement.productView&productId=<%=product.getProductId()%>">
                                <span class="play"><span class="name"><%=product.getName()%></span></span>
                                <img src="images/<%=product.getPicture_url()%>" alt="movie">
                            </a>
                        </div>
                        <div class="rating">
                            <p>PRICE</p>
                            <div class="stars">
                                <div class="stars-in">

                                </div>
                            </div>
                            <span class="comments">$<%=product.getPrice()%></span>
                        </div>
                    </div>
                    <!-- end Movie -->
                    <% } %>
                </div>
            </div>
        </div>

    </main>
    <!--footer -->
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>




