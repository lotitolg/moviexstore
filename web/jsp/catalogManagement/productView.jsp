<%@ page import="model.session.mo.LoggedUser" %>
<%@ page import="model.mo.HomevideoProduct" %>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");

    HomevideoProduct homevideoProduct = (HomevideoProduct) request.getAttribute("homevideoProduct");

    /*Filters*/
    String[] typeFilter = null;
    String[] formatFilter = null;
    String[] genreFilter = null;
    try {
        typeFilter = (String[]) request.getAttribute("typeFilter");
    } catch (Exception ignored) {
    }
    try {
        formatFilter = (String[]) request.getAttribute("formatFilter");
    } catch (Exception ignored) {
    }
    try {
        genreFilter = (String[]) request.getAttribute("genreFilter");
    } catch (Exception ignored) {
    }
    String entityFilter = (String) request.getAttribute("entityFilter");
    String orderFilter = (String) request.getAttribute("orderFilter");
    String descendant = (String) request.getAttribute("descendant");
    String searchString = (String) request.getAttribute("searchString");
    String thisPage = (String) request.getAttribute("thisPage");

%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>

    <style>

        #principle {
            clear: none;
        }

        .movie_info_box {
            width: 900px;
            margin: 20px;
            border: 1px activecaption solid;
            border-radius: 0px;
        }

        .movie_info_box p.type {
            font-size: 14px;
            color: #fff;
            margin-bottom: 4px;
        }

        .movie_info_box p.type span {
            font-size: 16px;
            color: deepskyblue;
            margin-bottom: 4px;

        }

        .movie_info_box iframe {
            margin: 30px
        }

        .movie_info_body {

            margin-top: 40px;
        }

        .movie_info {

            margin: 40px;

        }

        .movie_info form {

            font-size: 15px;


        }

        .movie_info form input {

            font-size: 13px;
            border: 2px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;
        }

        .movie_info form input:hover, :checked {

            font-size: 12px;
            border: 1px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;

        }


        .movie_payment {
            float: left;
            margin: 25px 20px;
            padding: 1px;

        }

        .movie_payment form input {

            font-size: 13px;
            border: 2px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;
        }

        .movie_payment form input :hover :checked {

            font-size: 12px;
            border: 1px whitesmoke outset;
            padding: 8px;
            border-radius: 50px;

        }

        .movie_payment form {

            font-size: 16px;
            color: deepskyblue;
            margin-bottom: 4px;


        }

        .movie_payment p {
            margin-top: 20px;

        }

        .movie-image-aside img {

            border-radius: 40px;
            margin: 10px;
            width: 90px;
            height: 80px;
        }

    </style>
    <script>

        function mainOnLoadHandler() {
            document.wishlist.wishlistButton.addEventListener("click", function (ev) {
                document.wishlist.submit();
            });
        }
    </script>
</head>
<body>
<div class="portal">
<%@include file="/include/header.inc" %>
<main id="main">
    <!--header -->


    <%-- _______Aside bar___________--%>

    <aside class="right portal clearfix"
           style="width: 20%; border: 1px whitesmoke solid; margin-top: 60px; margin-right: 3%;">

        <div class="head">
            <h3> ADD TO CART</h3>
            <div class="movie_payment">

                <h3 style="margin-top: 10px">Product: </h3>

                <form style="text-align: left;" id="formAddCart " name="formAddCart"
                      action="Dispatcher" method="post">

                    <div class="clearfix" style="float:left;margin-bottom:22px">
                        <p class="type" style="text-align: left;color:white"><%=homevideoProduct.getName()%>
                        </p>

                        <%if (homevideoProduct.getAvailable() > 0) { %>

                        <div>

                            <p class="type" style="color:greenyellow">
                                <span>Availabilty : </span><%=homevideoProduct.getAvailable()%>
                            </p>

                            <p class="type" style="color:orangered">
                                <span>Price : </span>€<%=homevideoProduct.getPrice()%>
                            </p>

                        </div>

                        <div>
                            <label for="quantity">Quantity: </label>

                            <select id="quantity" name="quantity" style="margin-top: 20px">

                                <% for (int i = 1; i <= homevideoProduct.getAvailable(); i++) {%>
                                <option value="<%= i%>"><%= i%>
                                </option>
                                "); <%
                                }
                            %>
                            </select>
                        </div>

                        <div>

                            <button type="submit" name="cartButton" class="userbutton click"
                                    style="margin-left:7px; margin-top:30px;">
                                Add to cart <i class="fa fa-shopping-cart"></i>
                            </button>
                        </div>
                        <% } else { %>
                        <p class="type" style="color:greenyellow">Not Available</p>
                        <% } %>
                    </div>

                    <input type="hidden" name="controllerAction" value="CatalogManagement.addToCart">
                    <input type="hidden" name="productId" value="<%=homevideoProduct.getProductId()%>">

                </form>
            </div>
        </div>

    </aside>

    <%-- ___________principle____________ --%>

    <div id="principle" style="margin: 60px;">

        <div class="movie_info_box portal">
            <div class="head">
                <h3 style="">HOMEVIDEO INFO</h3>
                <p class="text-right" style=" padding-right: 20px;"></p>
            </div>


            <div class="clearfix">
                <h1 style="padding:10px"><%=homevideoProduct.getName()%>
                </h1>
                <img style="float:left; margin :30px ; width: 320px"
                     src="images/<%=homevideoProduct.getPicture_url()%>">

                <div class="movie_info_body">

                    <p class="type"><span>Format: </span><%=homevideoProduct.getFormat()%>
                    </p>
                    <p class="type"><span>Number support: </span><%=homevideoProduct.getNumberSupport()%>
                    </p>
                    <p class="type"><span>Release Date: </span><%=homevideoProduct.getReleaseDate()%>
                    </p>
                    <p class="type"><span>Language:
                    <% for (String language : homevideoProduct.getLanguages()) {%>
                    </span><%=language != null ? language : ""%>
                        <% } %>
                    </p>
                    <%--<p class="type" style="color:greenyellow"><span>Availabilty : </span >Available </p> --%>
                    <%--<p class="type"><span>Price : </span>40€ </p> --%>


                    <div class="movie_info">

                        <form name="movieInfoform">
                            <input id="ViewInfo" class="userbutton click" type="submit" value="View info about work">
                            <input type="hidden" name="controllerAction" value="CatalogManagement.workView">
                            <input type="hidden" name="workId"
                                   value="<%=homevideoProduct.getAudiovisionWork().getWorkId()%>">
                        </form>

                        <form name="wishlist">
                            <button name="wishlistButton" class="userbutton click"
                                    style="margin-left:7px; margin-top:30px;">
                                Add to wishlist <i class="fa fa-heart"></i>
                            </button>
                            <input type="hidden" name="controllerAction" value="CatalogManagement.addtoWishlist">
                            <input type="hidden" name="productId" value="<%=homevideoProduct.getProductId()%>">
                        </form>

                        <form name="goCatalogForm">
                            <button class="userbutton click gotoCatalogButton"
                                    style="margin-left:7px; margin-top:30px;">
                                &#8592; Go to Catalog
                            </button>
                            <input type="hidden" name="controllerAction" value="CatalogManagement.catalogView">
                            <%=entityFilter != null ? "<input type=\"hidden\" name=\"entityFilter\" value=\"" + entityFilter + "\">" : ""%>
                            <%=orderFilter != null ? "<input type=\"hidden\" name=\"orderFilter\" value=\"" + orderFilter + "\">" : ""%>
                            <%=orderFilter != null ? "<input type=\"hidden\" name=\"descendant\" value=\"" + descendant + "\">" : ""%>
                            <%=thisPage != null ? "<input type=\"hidden\" name=\"thisPage\" value=\"" + thisPage + "\">" : ""%>
                            <% if (searchString != null) { %>
                            <input type="hidden" id="searchString" name="searchString" value=<%=searchString%>>
                            <% } %>
                            <%
                                if (formatFilter != null) {
                                    for (String s : formatFilter) {
                            %>
                            <input type="hidden" name="formatFilter" value="<%=s%>">
                            <% }
                            }%>
                            <%
                                if (genreFilter != null) {
                                    for (String s : genreFilter) {
                            %>
                            <input type="hidden" name="genreFilter" value="<%=s%>">
                            <% }
                            }%>
                            <%
                                if (typeFilter != null) {
                                    for (String s : typeFilter) {
                            %>
                            <input type="hidden" name="typeFilter" value="<%=s%>">
                            <% }
                            }%>
                        </form>

                    </div>


                </div>

            </div>

        </div>

    </div>


</main>
<!--footer -->
<%@include file="/include/footer.inc" %>
</div>
</body>
</html>


