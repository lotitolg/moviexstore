<%@page import="model.mo.Genre"%>
<%@page import="java.util.List"%>
<%@ page import="model.session.mo.LoggedUser" %>
<%@ page import="model.mo.HomevideoProduct" %>
<%@ page import="model.mo.AudiovisionWork" %>
<%--
  Created by IntelliJ IDEA.
  User: gigil
  Date: 04/07/2019
  Time: 21:24
  To change this template use File | Settings | File Templates.
--%>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");


    List<Genre> genres = (List<Genre>) request.getAttribute("genres");

    /*Filters*/
    List<String> checkedList = (List<String>)request.getAttribute("checkedList");
    String entityFilter = (String) request.getAttribute("entityFilter");
    String orderFilter = (String) request.getAttribute("orderFilter");
    boolean descendant  = (boolean) request.getAttribute("descendant");

    /*searchString*/
    String searchString = (String)request.getAttribute("searchString");

    /*Result*/
    List<HomevideoProduct> products = (List<HomevideoProduct>) request.getAttribute("products");
    List<AudiovisionWork> works = (List<AudiovisionWork>) request.getAttribute("works");

    /*Page*/
    Integer thisPage = (Integer) request.getAttribute("thisPage");
    Boolean isThisLastPage = (Boolean)request.getAttribute("isThisLastPage");
    final int multipleView = (int)request.getAttribute("multipleView");


%>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>
    <style>
        .box-movie{
            margin-top:80px;
            margin-left:30px;
        }
        .box-check{
            margin: 24px;
            font-size: 18px;
            border-bottom: 1px solid white
        }
        .box-check > p{
            text-transform: uppercase;
        }
        .box-check > label{
            display: block;
            margin: 20px;
            font-size: 20px;
        }
        .name-check{
            margin-left: 18px;
        }

        .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark {
            /*background-color: #2196F3;*/
            background-color:#690101;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        #catalogformDiv{
            font-size: 17px;
            margin-left: 38px;
        }

        .button {
            padding: 4px 23px;
            font-size: 24px;
            text-align: center;
            cursor: pointer;
            outline: none;
            color: #fff;
            background-color: #690101;
            border: none;
            border-radius: 15px;
            box-shadow: 0 5px #999;
        }

        .button:hover {color: red;}

        .button:active {
            box-shadow: 0 5px #666;
            transform: translateY(4px);
        }

        .custom-select{
            width: 315px;
            float: right;
            margin-top: 20px
        }

        .custom-select select{
            width: 100px;
            margin-top:6px;
            display: inline;
        }
        .pageButton{
            float: left;
            margin-top: 30px;
            margin-left: 25px;
        }

        .pagnum{
            font-size:14px;
            margin-left:10px;
        }

        #search .search-field {
            height:21px;
        }

        #cart{
            margin-top: 2px;
        }



    </style>

    <script>
        function changeFilter(event) {
            var inp = document.catalogform;
            var cause = event.target.name;
            var sel = document.getElementById('orderSelect');
            var descendant = document.getElementById("descendant");
            if(cause==='Homevideo' || cause==='Work') {
                inp.entityFilter.value = cause;
                if(cause==='Homevideo')
                    inp.orderFilter.value='name';
                else inp.orderFilter.value='original_title';
            }
            else {
                inp.orderFilter.value=sel.value;
            }
            inp.descendant.value=descendant.checked;
            inp.thisPage.value="1";
            inp.submit();
        }
        function precpagef() {
            var inp = document.catalogform;
            inp.thisPage.value="<%=thisPage-1%>";
            inp.submit();
        }
        function nextpagef() {
            var inp = document.catalogform;
            inp.thisPage.value="<%=thisPage+1%>";
            inp.submit();
        }
        function cancelFilters() {
            var inp = document.catalogform;
            var i;
            for(i=0;i<inp.length;i++){
                if(inp[i].type==='checkbox')
                    inp[i].checked=false;
            }
            inp.thisPage.value="1";
            inp.submit();

        }
        function  cancelsearchString() {
            var inp = document.catalogform;
            var searchString = document.getElementById("searchString");
            document.getElementById("catalogformDiv").removeChild(searchString);
            inp.thisPage.value="1";
            inp.submit();
        }

        function viewInfobyId(entity,id) {
            var div = document.getElementById("catalogformDiv");
            var input = document.createElement('input');
            input.type="hidden";
            input.name=entity+"Id";
            input.value=id;
            div.appendChild(input);
            document.catalogform.controllerAction.value='CatalogManagement.'+entity+'View';
            document.catalogform.submit();
        }
        function mainOnLoadHandler() {
            document.catalogform.<%=entityFilter%>.disabled=true;
            document.catalogform.<%=entityFilter%>.style.color='red';
            var i;
            var inp = document.catalogform;
            var sel = document.getElementById("orderSelect");
            var descendant = document.getElementById("descendant");

            <%if(checkedList!=null) {
               for(String s : checkedList) {%>
            for(i=0;i<inp.length;i++){
                if(inp[i].type==='checkbox' && inp[i].value==='<%=s%>')
                    inp[i].checked=true;
            }
            <% } }%>

            for(i=0;i<sel.options.length;i++){
                if(sel.options[i].value==='<%=orderFilter%>')
                    sel.selectedIndex=i;
            }
            descendant.checked=<%=descendant%>;

            for(i=0;i<inp.length;i++){
                inp[i].addEventListener("click",changeFilter);
            }

            sel.addEventListener("change",changeFilter);
            descendant.addEventListener("click",changeFilter);

            var precpage = document.getElementsByClassName("precpage");
            var nextpage = document.getElementsByClassName("nextpage");

            <%if((((entityFilter.equals("Homevideo") && products.size()==multipleView)
                || (entityFilter.equals("Work") && works.size()==multipleView)) &&!isThisLastPage)
                || thisPage>1) {
                if(thisPage==1) { %>
                    for(i=0;i<2;i++){
                        precpage[i].disabled=true;
                        precpage[i].style.color="grey";
                    }
                <% } else { %>
                    precpage[0].addEventListener("click", precpagef);
                    precpage[1].addEventListener("click", precpagef);
                <% } %>

                <% if(isThisLastPage){ %>
                    for(i=0;i<2;i++){
                        nextpage[i].disabled=true;
                        nextpage[i].style.color="grey";
                    }
                <% } else { %>
                    nextpage[0].addEventListener("click", nextpagef);
                    nextpage[1].addEventListener("click", nextpagef);
                <% }
            }%>
        }
    </script>


</head>
<body>
    <div class="portal">
        <header>
            <%@include file="/include/header.inc" %>
        </header>
        <main id="content" class="clearfix">

            <section class="pageTitle" style="margin: 31px 560px">
                <div class="head" style="width: 180px;padding-top: 9px;background: none;">
                    <h3 style="margin-right: 12px; color:#ff1e1e;font-size:24px">CATALOG</h3>
                </div>
            </section>

            <form  name="catalogform" action="Dispatcher" method="post">
                <div id="catalogformDiv">
                    <label>Search by:</label>
                    <input class="button" type="button" id="Homevideo" name="Homevideo" value="Homevideo">
                    <input class="button" type="button" id="Work" name="Work" value="Work">
                    <input type="hidden"  name="controllerAction" value="CatalogManagement.catalogView">
                    <input type="hidden" name="entityFilter" value="<%=entityFilter%>">
                    <input type="hidden" name="orderFilter" value="<%=orderFilter%>">
                    <input type="hidden" name="descendant" value="<%=descendant%>">
                    <input type="hidden" name="thisPage" value="<%=thisPage%>">
                    <% if(searchString!=null) { %>
                    <input type="hidden" id="searchString" name="searchString" value="<%=searchString%>">
                    <% } %>
                </div>


                <aside>
                <div id="boxLeft" class="box-genre">
                    <div class="head" >
                        <div>
                            <h3> Filters </h3>
                        </div>
                        <p class="text-right" style=" padding-right: 20px;">
                                <a href="javascript:cancelFilters()" style="text-decoration:underline">Cancel filters</a>
                        </p>
                    </div>
                    <div class="box-check">
                            <p>Type:</p>
                            <label class="container">
                                <input type="checkbox" name="typeFilter" value="Movie">
                                <span class="name-check">Film</span>
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">
                                <input type="checkbox" name="typeFilter" value="TV series">
                                <span class="name-check">Tv Series</span>
                                <span class="checkmark"></span>
                            </label>
                    </div>
                    
                    <div class="box-check">
                            <p>Format:</p>
                            <label class="container">
                                <input type="checkbox" name="formatFilter" value="DVD"><span class="name-check">DVD</span>
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">
                                <input type="checkbox" name="formatFilter" value="Blu-ray"><span class="name-check">Blu-ray</span>
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">
                                <input type="checkbox" name="formatFilter" value="UHD"><span class="name-check">4K</span>
                                <span class="checkmark"></span>
                            </label>
                            
                    </div>
                    
                    <div class="box-check">
                        <p>Genre:</p>
                        <% for(Genre genre: genres) { %>
                            <label class="container">
                                <input type="checkbox" name="genreFilter" value="<%=genre.getGenreName()%>"><span class="name-check"><%=genre.getGenreName()%></span>
                                <span class="checkmark"></span>
                            </label>
                        <%}%>
                    </div>
                        
                </div>
            </aside>

            </form>
            
                <div class="box">

                   <div class="head" >
                        <div >
                            <h3 > <%=searchString!=null? "Result : " + searchString : "Titles"%> </h3>
                        </div>
                       <% if (searchString!=null) {%>
                       <p class="text-right" style=" padding-right: 20px;">
                           <a href="javascript:cancelsearchString()" style="text-decoration:underline">Cancel search key</a>
                       </p>
                       <%}%>
                   </div>

                    <%if((((entityFilter.equals("Homevideo") && products.size()==multipleView)
                            || (entityFilter.equals("Work") && works.size()==multipleView)) &&!isThisLastPage)
                            || thisPage>1) { %>
                    <div class="pageButton">
                        <input type="button" name="precpage" class="precpage userbutton click" value="< Prec. Page">
                        <input type="button" name="nextpage" class="nextpage userbutton click" value="Next Page >">
                        <span class="pagnum">Page <%=thisPage%></span>
                    </div>
                    <% } %>

                    <div class="custom-select">
                        <span style="margin-right: 10px;">
                            <span class="name-check">Descendant</span>
                            <input type="checkbox" id=descendant name="descendant" value="true">
                        </span>
                        <label>Order By: </label>
                        <select id=orderSelect>
                            <%if (entityFilter.equals("Homevideo")) {%>
                            <option value="name">Name</option>
                            <option value="price">Price</option>
                            <option value="release_date">Release date</option>
                            <% } else { %>
                            <option value="original_title">Title</option>
                            <option value="rating">Rating</option>
                            <option value="year_production">Year</option>
                            <% } %>

                        </select>
                    </div>

                    <div class="box-movie clearfix">
                        <%
                            if (entityFilter.equals("Homevideo"))
                                for (HomevideoProduct product : products) {
                        %>
                        <!-- Movie -->
                        <div class="movie">

                            <div class="movie-image">
                                <a href="javascript:viewInfobyId('product','<%=product.getProductId()%>')">
                                    <span class="play"><span class="name"><%=product.getName()%></span></span>
                                    <img src="images/<%=product.getPicture_url()%>" alt="movie">
                                </a>
                            </div>
                            <div class="rating">
                                <p>PRICE</p>
                                <div class="stars">
                                    <div class="stars-in">

                                    </div>
                                </div>
                                <span class="comments">€<%=product.getPrice()%></span>
                            </div>
                        </div>
                        <!-- end Movie -->
                        <% }
                        else {
                            for (AudiovisionWork work : works) {%>
                        <div class="movie">

                            <div class="movie-image">
                                <a href="javascript:viewInfobyId('work','<%=work.getWorkId()%>')">
                                    <span class="play"><span class="name"><%=work.getOriginalTitle()%></span></span>
                                    <img src="images/<%=work.getImageWork()%>" alt="movie">
                                </a>
                            </div>
                            <div class="rating">
                                <p>Rating</p>
                                <div class="stars">
                                    <div class="stars-in">

                                    </div>
                                </div>
                                <span class="comments"><i class="fa fa-star"></i><%=work.getRating()%>/10</span>
                            </div>
                        </div>
                        <% }
                        } %>
                    </div>

                    <%if((((entityFilter.equals("Homevideo") && products.size()==multipleView)
                            || (entityFilter.equals("Work") && works.size()==multipleView)) &&!isThisLastPage)
                            || thisPage>1) { %>
                    <div class="pageButton">
                        <input type="button" name="precpage" class="precpage userbutton click" value="< Prec. Page">
                        <input type="button" name="nextpage" class="nextpage userbutton click" value="Next Page >">
                        <span class="pagnum">Page <%=thisPage%></span>
                    </div>
                    <% } %>

                </div>
        </main>

    </div>
</body>
</html>
