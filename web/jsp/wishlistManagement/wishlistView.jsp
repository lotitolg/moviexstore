<%@page import="java.util.List" %>
<%@ page import="model.session.mo.LoggedUser" %>
<%@ page import="model.mo.HomevideoProduct" %>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>


<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");
    List<HomevideoProduct> homevideoProducts = (List<HomevideoProduct>) request.getAttribute("homevideoProducts");

%>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/include/htmlHead.inc" %>

    <style>
        .aside_body_a ul li {

            margin: 5px;
        }

        .User_info_box {

            height: max-content;

        }

        #body_Info {

            margin: 20px;

        }

        .aside_body_a li :hover {
            color: red;
        }

        .aside_body_a > button {
            font-size: 20px;
            margin: 7px;
            padding: 10%;
            border-color: whitesmoke;
            background: darkred;
            border-width: 2px;
            box-shadow: 1px 1px grey;

        }

        .aside_body_a > button:hover {
            font-size: 20px;
            margin: 6px;
            margin-bottom: 10px;
            padding: 10%;
            border-color: whitesmoke;
            background: darkred;
            border-width: 1px;
            box-shadow: none;

        }

        #aside1 {
            width: 20%;
            border: 1px whitesmoke solid;
            margin-top: 20px;
            margin-left: 21px;
            clear: left;
        }

        #aside2 {

            width: 20%;
            border: 1px whitesmoke solid;
            margin-left: 21px;
            clear: left;
            margin-top: 7%;
            position: sticky;
            top: 10px;
        }

        .wishlistInput {
            float: left;
            height: 40px;
            margin-right: 30px;
            margin-top: 45px;
        }

        #rightCart {
            float: right;
        }

        .price {
            float: right;
            margin: 60px 45px 60px 60px;
            padding-left: 45px;
            width: 80px;
        }

        .quantitySelect {
            margin: 10px;
            font-size: 14px;
            border-radius: 9px;
        }

        .box {
            float: none;
            width: auto;
        }

        #quantitySpan{
            margin-right: 0px;
        }

        #notAvailable{
            color:greenyellow;
            font-size: 14px;
            margin-top: 60px;
        }


    </style>


    <script>
        function addToCart(productid,i) {
            var form = document.addtoCartForm;
            form.productId.value = productid;
            var select = document.getElementById("quantity"+i);
            form.quantity.value = select.value;
            form.submit();
        }

        function remove(productId) {
            var form = document.removeItemForm;
            form.productId.value = productId;
            form.submit();
        }
    </script>


</head>
<body>
<div class="portal">
    <%@include file="/include/header.inc" %>

    <main id="content" class="clearfix">

        <div class="box">
            <div class="head">
                <h3 style="">Wishlist</h3>
                <p class="text-right" style=" padding-right: 20px;"></p>
            </div>

            <%if(homevideoProducts.size()==0){%>
            <h1 style="margin: 20px;"> Your wishlist is empty &#128542;</h1>
            <% }else {%>
            <div class="clearfix" style=" border-bottom: 0.5px white dotted; border-radius: 10px;">
                <h3 style="margin: 12px;float:left"> Product: </h3>
                <h3 style="float: right; font-size: 18px; margin: 12px; margin-right: 70px;"> Price:</h3>
            </div>
            <%}%>

            <% int i=0;
                for(HomevideoProduct product : homevideoProducts) {%>

            <%--_________ Movie ____________--%>
            <div class="classgen clearfix " style="border-bottom: 0.5px white dotted;border-radius: 10px;">

                <a href="Dispatcher?controllerAction=CatalogManagement.productView&productId=<%=product.getProductId()%>"><img style="clear: left;float:left; margin :10px ; width: 100px"
                     src="images/<%=product.getPicture_url()%> "></a>

                <div class="class1" style="float: left;display: flex; margin-top: 20px;">

                    <div class="class2" style=" margin: 20px;">
                        <p class="" style="text-align: left; margin: 5px">
                            <span>Name: </span><%=product.getName()%>
                        </p>

                        <p class="type" style="color:greenyellow">
                            <span>Availabilty : </span><%=product.getAvailable()%>
                        </p>
                    </div>

                </div>

                <span id="rightCart" class="clearfix">

                    <%if(product.getAvailable()>0){%>
                    <span id="quantitySpan" class="wishlistInput">
                    <label for="quantity<%=i%>" style="text-align: right;font-size: 14px;margin-right: -12px">Quantity: </label>

                        <select id="quantity<%=i%>"  name="quantity" class="quantitySelect">
                            <% for (int j = 1; j <= product.getAvailable(); j++) { %>
                            <option value="<%=j%>"><%=j%>
                            </option>
                            <%}%>
                        </select>
                        </span>
                        <span id="addToCartButton" class="wishlistInput">
                            <input onclick="addToCart(<%=product.getProductId()%>,<%=i%>)" class="userbutton click" type="button" value="Add to Cart">
                        </span>
                    <%} else {%>
                    <span class="wishlistInput" id="notAvailable" >Not Available</span>
                    <%}%>
                        <span id="removeButton" class="wishlistInput">
                            <input onclick="remove(<%=product.getProductId()%>)" class="userbutton click" type="button" value="Remove">
                        </span>

                        <div class="price">
                            <h3>€ <%=product.getPrice()%></h3>
                        </div>
                    </span>


            </div>
            <%-- End Movie --%>

            <%i++;}%>


        </div>

    </main>

    <form name="addtoCartForm" method="post" action="Dispatcher">
        <input type="hidden" name="controllerAction" value="CatalogManagement.addToCart">
        <input type="hidden" name="quantity" value="">
        <input type="hidden" name="productId" value="">
    </form>

    <form name="removeItemForm" method="post" action="Dispatcher">
        <input type="hidden" name="controllerAction" value="WishlistManagement.removeItem">
        <input type="hidden" name="productId" value="">
    </form>

    <!--footer -->
    <%@include file="/include/footer.inc" %>
</div>
</body>
</html>
