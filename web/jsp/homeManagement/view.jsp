<%@ page import="model.session.mo.LoggedUser" %>
<%@ page import="model.mo.HomevideoProduct" %>
<%@ page import="java.util.List" %>
<%@ page import="model.mo.AudiovisionWork" %>
<%@ page import="model.mo.Genre" %>
<%@page session="false"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<% 
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = (Boolean) request.getAttribute("loggedOn");
    LoggedUser loggedUser = (LoggedUser) request.getAttribute("loggedUser");
    List<HomevideoProduct> latestReleases = (List<HomevideoProduct>) request.getAttribute("latestReleases");
    List<HomevideoProduct> bestSellers = (List<HomevideoProduct>) request.getAttribute("bestSellers");
    List<AudiovisionWork> topRated = (List<AudiovisionWork>) request.getAttribute("topRated");
    List<Genre> genres = (List<Genre>) request.getAttribute("genres");
%>

<!DOCTYPE html>

<html>
    
    
    <head>
    <%@include file="/include/htmlHead.inc"%>  
    </head>

    <body>
        <div class="portal">
            <!--header -->
            <%@include file="/include/header.inc"%>
            <!--main-->
            <main id="content" class="clearfix">

                    <!-- side bar -->
                    <aside>
                        <div class="box-genre">
                            <div class="head">
                                <div >
                                    <h3> Genres </h3>
                                </div>

                            </div>
                            <ul id="genre_list" style="list-style-position: inside; list-style-type: none; margin: 3px">

                                <%for(Genre genre : genres) {%>
                                <a href="Dispatcher?controllerAction=CatalogManagement.catalogView&genreFilter=<%=genre.getGenreName()%>"
                                   title="<%=genre.getGenreName()%>"><li><%=genre.getGenreName()%></li></a>
                                <%}%>
                                
                           </ul>
                        </div>
                    </aside>
                    <!-- end side bar-->

                    <div class="box clearfix">
                       
                        <div class="head">
                            <h3>LATEST RELEASES</h3>
                        </div>

                        <% for(HomevideoProduct product : latestReleases) { %>

                            <!-- Movie -->
                            <div class="movie ">

                                <div class="movie-image">
                                    <a href="Dispatcher?controllerAction=CatalogManagement.productView&productId=<%=product.getProductId()%>">
                                        <span class="play "><span class="name"><%=product.getName()%></span></span>
                                        <img src="images/<%=product.getPicture_url()%>" alt="movie">
                                    </a>
                                </div>
                                <div class="rating">
                                    <p>PRICE</p>
                                    <div class="stars">
                                        <div class="stars-in">

                                        </div>
                                    </div>
                                    <span class="comments">€<%=product.getPrice()%></span>
                                </div>
                            </div>
                            <!-- end Movie -->
                        <% } %>
                    </div>



                    <div class="box">
                        <div class="head">
                            <h3>BEST SELLERS</h3>

                            </p>
                        </div>

                        <% for(HomevideoProduct product : bestSellers) { %>

                            <!-- Movie -->
                            <div class="movie">

                                <div class="movie-image">


                                    <a href="Dispatcher?controllerAction=CatalogManagement.productView&productId=<%=product.getProductId()%>">
                                        <span class="play">
                                        <span class="name"><%=product.getName()%></span></span>
                                        <img src="images/<%=product.getPicture_url()%>" alt="movie">
                                    </a>
                                </div>
                                <div class="rating">
                                    <p>PRICE</p>
                                    <div class="stars">
                                        <div class="stars-in">

                                        </div>
                                    </div>
                                    <span class="comments">€<%=product.getPrice()%></span>
                                </div>
                            </div>
                            <!-- end Movie -->
                        <% } %>
                    </div>

                    <div class="box">
                        <div class="head">
                            <h3>TOP RATED</h3>
                            </p>
                        </div>

                            <% for(AudiovisionWork work : topRated){ %>

                            <!-- Movie -->
                            <div class="movie">

                                <div class="movie-image">


                                    <a href="Dispatcher?controllerAction=CatalogManagement.workView&workId=<%=work.getWorkId()%>"><span class="play">
                                        <span class="name"><%=work.getOriginalTitle()%></span></span>
                                        <img src="images/<%=work.getImageWork()%>" alt="movie">
                                    </a>
                                </div>
                                <div class="rating">
                                    <p>Rating</p>
                                    <div class="stars">
                                        <div class="stars-in">

                                        </div>
                                    </div>
                                    <span class="comments"><i class="fa fa-star"></i><%=work.getRating()%>/10</span>
                                </div>
                            </div>
                            <!-- end Movie -->
                            <%}%>


                    </div>




            </main>
            
            <!--footer -->
            <%@include file="/include/footer.inc"%>
        </div>
    </body>

</html>
