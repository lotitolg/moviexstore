<%-- 
    Document   : viewSubscribe
    Created on : 29-giu-2019, 22.10.45
    Author     : Luigi
--%>
<%@ page import="model.session.mo.LoggedUser" %>
<%@page session="false"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    String applicationMessage = (String) request.getAttribute("applicationMessage");
    boolean loggedOn = false;
    LoggedUser loggedUser = null;
%>

<html>
    <head>

        <%@include file="/include/htmlHead.inc"%>  
        <style>

            .field {
                margin: 20px;
                text-align: center;
            }     

            #insModFormSection label {
                float: left;
                width: 56px;
                margin-right: 10px;
                padding-top: 3px;
                text-align: left;
            }

            #insModFormSection{
                margin: 10px 543px;
                width: 336px;
            }

            #insModFormSection input[type="text"],  #insModFormSection input[type="password"], input[type="email"] {
                border: none;
                border-radius: 4px;
                padding: 3px; 
                background-color: #e8eeef;
                color:#8a97a0;
                box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
            }

            #insModFormSection input[type="text"]:focus, #insModFormSection input[type="password"]:focus{
                background: #d2d9dd;
                outline-color: #a3271f;
            }
        </style>
    </head>

    <body>
        <div class="portal">

            <!--header -->
            <%@include file="/include/header.inc"%>

            <main style=" text-align: center;">
                <div style=" text-align: center; border: 1px white solid">
                    <section class="pageTitle" style="width: 10%;">
                        <div class="head" style="width :180px; border-radius: 30px">
                            <h3 style="">SIGN IN</h3>
                        </div>   
                    </section>

                    <section id="insModFormSection" class="label-login">

                        <form name="insModForm" action="Dispatcher" method="post">

                            <div class="field clearfix">
                                <label for="username">Username*</label>
                                <input type="text" id="username" name="username" 
                                       value="" required size="20" maxlength="50"/>
                            </div>

                            <div class="field clearfix">
                                <label for="password">Password*</label>
                                <input type="password" id="password" name="password" 
                                       value="" required size="20" maxlength="50"/>
                            </div>

                            <div class="field clearfix">
                                <label for="firstname">Name*</label>
                                <input type="text" id="firstname" name="firstname" 
                                       value="" required size="20" maxlength="50"/>
                            </div>

                            <div class="field clearfix">
                                <label for="surname">Surname*</label>
                                <input type="text" id="surname" name="surname" 
                                       value="" required size="20" maxlength="50"/>
                            </div>

                            <div class="field clearfix">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email"
                                       value="" size="20" maxlength="50"/>
                            </div>

                            <p class="field">*campi obbligatori</p>

                            <div class="field clearfix">
                                <label>&#160;</label>
                                <input type="submit" class="userbutton click" value="Send"/>
                                <input type="reset" class="userbutton click" value="Cancel"/>
                                <input type="hidden" name="controllerAction" value="HomeManagement.subscribe"/>
                            </div>
                        </form>
                    </section>
                </div>
            </main>
        </div>
    </body>


</html>
