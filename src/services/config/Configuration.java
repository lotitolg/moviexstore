package services.config;

import java.util.logging.Level;

import model.dao.DAOFactory;
import model.session.dao.SessionDAOFactory;

public class Configuration {
  
  /* Database Configruation */

  //MYSQL
  /*
  public static final String DAO_IMPL= DAOFactory.MYSQLJDBCIMPL;
  public static final String DATABASE_DRIVER="com.mysql.jdbc.Driver";
  public static final String DATABASE_URL="jdbc:mysql://localhost/moviexstore?user=root&password=";
  */

  //DB2
  public static final String DAO_IMPL= DAOFactory.DB2JDBCIMPL;
  public static final String DATABASE_DRIVER="com.ibm.db2.jcc.DB2Driver";
  public static final String DATABASE_URL="jdbc:db2://localhost:50000/moviex";
  public static final String USER_ID = "db2inst1";
  public static final String PASSWORD = "GD1OJfLGG64HV2dtwK";

  /* Session Configuration */
  public static final String SESSION_IMPL=SessionDAOFactory.COOKIEIMPL;
  
  /* Logger Configuration */
  public static final String GLOBAL_LOGGER_NAME="MovieXStore";
  private static final String USERNAME = System.getProperty("user.name");
  public static final Level GLOBAL_LOGGER_LEVEL=Level.ALL; 
  public static final String GLOBAL_LOGGER_FILE="C:\\Users\\"+USERNAME+"\\Documents\\logs\\MovieXStore_log.%g.%u.txt";

  /*Upload Configuration*/
  public static final String GLOBAL_UPLOAD_IMAGES_PATH="C:\\Users\\gigil\\OneDrive\\Università\\Progetti intelliJ\\portatileMovieXStore\\web\\images\\";

  /*Name Configuration Site On Database*/
  public static final String ADMIN_MULTIPLE_VIEW_CONFIG_NAME = "admin_multipleView";
  public static final String CUSTOMER_MULTIPLE_VIEW_CONFIG_NAME = "customer_multipleView";
  public static final String SHIPPING_PRICE_CONFIG_NAME = "current_shipping_price";
}
