package model.dao;

import model.dao.db2JDBCImpl.db2JDBCDAOFactory;
import model.dao.mySQLJDBCImpl.MySQLJDBCDAOFactory;

public abstract class DAOFactory {

  // List of DAO types supported by the factory
  public static final String MYSQLJDBCIMPL = "mySQLJDBCImpl";
  public static final String DB2JDBCIMPL = "db2JDBCImpl";

  public abstract void beginTransaction();
  public abstract void commitTransaction();
  public abstract void rollbackTransaction();
  public abstract void closeTransaction();
  
  public abstract CustomerDAO getCustomerDAO();
  public abstract HomevideoProductDAO getHomevideoProductDAO();
  public abstract AudiovisionWorkDAO getAudiovisionWorkDAO();
  public abstract GenreDAO getGenreDAO();
  public abstract ActorDAO getActorDAO();
  public abstract AdminDAO getAdminDAO();
  public abstract BillingInfoDAO getBillingInfoDAO();
  public abstract CartDAO getCartDAO();
  public abstract CouponDAO getCouponDAO();
  public abstract DeliveryAddressDAO getDeliveryAddressDAO();
  public abstract DirectorDAO getDirectorDAO();
  public abstract OrderDAO getOrderDAO();
  public abstract AdminConfigurationDAO getAdminConfigurationDAO();

  public static DAOFactory getDAOFactory(String whichFactory) {

    if (whichFactory.equals(MYSQLJDBCIMPL)) {
      return new MySQLJDBCDAOFactory();
    } else if (whichFactory.equals(DB2JDBCIMPL)) {
        return new db2JDBCDAOFactory();
      }
      else {
      return null;
    }
  }
}

