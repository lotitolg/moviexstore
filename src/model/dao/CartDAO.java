package model.dao;

import java.util.List;

import model.mo.Cart;
import model.mo.Customer;
import model.mo.ProductInCart;

public interface CartDAO {

    public List<ProductInCart> findProductsInCartByCartId(long cartId);

    public List<ProductInCart> findCartWithProductbyUserId(long user_id);

    public Cart assignNewCart(Customer customer);
    
    public void checkOutCart(Customer customer);
    
    public Cart findCartInfobyUserId(long userId);

    public Integer countItemsInCart(Cart cart);

    public void addProduct(Long productId, int quantity,Long cartId);

    public void updateItemQuantity(Long productId,int quantity,Long cartId);

    public void removeItem(Long productId,Long cartId);

}
