package model.dao;

import model.dao.exception.DuplicatedObjectException;
import model.mo.*;

import java.util.List;

public interface AudiovisionWorkDAO {

    public AudiovisionWork insert(
            String title,
            String type,
            int duration,
            float rating,
            String yearProduction,
            String plot,
            String trailerUrl,
            String studioProduction,
            String imageWork,
            Genre[] genres,
            Actor[] actors,
            Director[] directors
        ) throws DuplicatedObjectException;

    public void delete(AudiovisionWork audiovisionWork);

    public void update(AudiovisionWork audiovisionWork) throws DuplicatedObjectException;
    public List<AudiovisionWork> findNTopRated(int N);

    public List<AudiovisionWork> findByFilters(
            String[] typeFilter,
            String[] formatFilter,
            String[] genreFilter,
            String orderFilter,
            Boolean descendant,
            int thispage,final int multipleView, String searchString);

    public AudiovisionWork findWithGenresbyId(Long workid);
}
