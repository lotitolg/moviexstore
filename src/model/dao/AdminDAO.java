package model.dao;

import model.dao.exception.DuplicatedUsernameException;
import model.mo.Admin;

import java.util.List;

public interface AdminDAO {

    public Admin insert(
            String firstname,
            String surname,
            String username,
            String password
    )throws DuplicatedUsernameException;

    public void update(Admin admin)throws DuplicatedUsernameException;

    public void delete(Admin admin);

    public void approve(Admin admin);

    public List<String> findInitial();

    public List<Admin> findByInitialAndSearchString(String initial, String searchString,Boolean waitApprove);

    public Admin findbyAdminId(Long adminId);

    public Admin findbyUsername(String firstName);

    public void changePasword(Admin customer, String newPassword);

    
    
}
