package model.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import model.dao.exception.DuplicatedObjectException;
import model.mo.Coupon;
import model.mo.Order;

public interface CouponDAO {

    public void insert(
            String code,
            int discountPercent) throws DuplicatedObjectException;

    public void update(Coupon coupon);

    public Coupon findCouponbyCode(String couponCode);

    public ArrayList<Coupon> findAllCoupons();

    public void disableCoupon(long couponId);

    public void unDisableCoupon(long couponId);

}
