package model.dao;

import java.sql.Date;
import model.dao.exception.DuplicatedUsernameException;
import model.mo.BillingInfo;
import model.mo.Customer;

public interface BillingInfoDAO {

    public BillingInfo insert(
                Customer customer,
                String cardNumber,
                Date cardexpdate,
                String networkCard) throws DuplicatedUsernameException;
    
    public BillingInfo findActiveBillingInfobyCustomerId(long customer_id);
    
    public void update(long cutomer_id, BillingInfo newBillingInfo);

    public void setInactive(BillingInfo billingInfo);

    public void setUsed(BillingInfo billingInfo);
}
