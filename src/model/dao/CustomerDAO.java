package model.dao;

import java.util.List;

import model.dao.exception.DuplicatedUsernameException;
import model.mo.Customer;

public interface CustomerDAO {

    public void insert(
            String firstname,
            String surname,
            String username,
            String password,
            String email) throws DuplicatedUsernameException;

    public void update(Customer customer) throws DuplicatedUsernameException;

    public void changePasword(Customer customer, String newPassword);

    public void delete(long customerId);

    public void undelete(long customerId);

    public void ban(long customer);

    public void unBan(long customerId);

    public Customer findCustomerbyId(long customer_id);

    public Customer findByUsername(String username);

    public Customer updateCustomerInfo(Customer customer) throws DuplicatedUsernameException;

    public Customer findCustomerbyIdDeletedEven(long customer_id);


    public List<String> findInitial();

    public List<Customer> findByInitialAndSearchString(String initial, String searchString);

}
