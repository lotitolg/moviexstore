package model.dao;

import model.mo.AdminConfiguration;

import java.util.List;

public interface AdminConfigurationDAO {

    public String read(String configName);

    public void update(String configName,String configValue);

    public List<AdminConfiguration> readAll();

}
