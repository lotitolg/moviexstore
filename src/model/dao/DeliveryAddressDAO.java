package model.dao;

import model.mo.Customer;
import model.mo.DeliveryAddress;

import java.util.List;

public interface DeliveryAddressDAO {

    public DeliveryAddress insert(
            Customer customer,
            String country,
            String city,
            String address,
            String postalcode);
    
    public DeliveryAddress getActiveAddressbyCustomer(long customerId);
    
    public DeliveryAddress update(DeliveryAddress deliveryAddress);

    public List<DeliveryAddress>getAddressesByCustomer(Customer customer);

    public void setInactive(DeliveryAddress deliveryAddress);

    public void setUsed(DeliveryAddress deliveryAddress);
}
