package model.dao.exception;

public class DuplicatedUsernameException extends Exception {

    public DuplicatedUsernameException() {
    }

    public DuplicatedUsernameException(String message) {
        super(message);
    }
}
