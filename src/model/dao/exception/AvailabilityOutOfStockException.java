package model.dao.exception;

public class AvailabilityOutOfStockException extends Exception {

    public AvailabilityOutOfStockException() {
    }

    public AvailabilityOutOfStockException(String message) {
        super(message);
    }
}
