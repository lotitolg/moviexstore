package model.dao.exception;

public class DuplicatedObjectException extends Exception {


    public DuplicatedObjectException() {
    }

    public DuplicatedObjectException(String message) {
        super(message);
    }
}
