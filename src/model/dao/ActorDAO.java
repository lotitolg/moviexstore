package model.dao;

import model.dao.exception.DuplicatedObjectException;
import model.mo.Actor;
import model.mo.AudiovisionWork;


import java.util.List;

public interface ActorDAO {

    public Actor insert(String actorName) throws DuplicatedObjectException;

    public void delete(Actor actor);

    public void update(Actor actor);

    public List<Actor> findByAudiovisionWork(AudiovisionWork audiovisionWork);

    public List<Actor> extractActors();

    public List<Actor> findBySearchString(String searchString);
}
