package model.dao;

import java.util.ArrayList;
import java.util.List;

import model.mo.BillingInfo;
import model.mo.Cart;
import model.mo.Coupon;
import model.mo.Customer;
import model.mo.DeliveryAddress;
import model.mo.Order;

public interface OrderDAO {

    public void ConnectOrdertoCart(Cart cart,
            Coupon coupon,
            DeliveryAddress address,
            Customer customer,
            BillingInfo billing,Double totalSpending,double shippingPrice);

    public int numberOfOrdersForCustomer(long customerId);

    public int [] ArrayOfNumberOfOrdersForCustomer(long customerId, int numberofOrders) ;
    
    public List<Order> ListOfOrdersForCustomer(long customerId);
    
    public void changeOrderStatus(long orderId) ;
    
    public ArrayList<Order> listOfAllOrdersNotDelivered();
}
