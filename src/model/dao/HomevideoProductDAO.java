package model.dao;

import java.util.Date;
import java.util.List;

import model.dao.exception.AvailabilityOutOfStockException;
import model.dao.exception.DuplicatedObjectException;
import model.mo.AudiovisionWork;
import model.mo.Customer;
import model.mo.HomevideoProduct;
import model.mo.ProductInCart;

public interface HomevideoProductDAO {

    public void insert(
            AudiovisionWork audiovisionWork,
            String name,
            Double price,
            String picture_url,
            String format,
            int numberSupport,
            int available,
            Date releaseDate,
            String[] languages
            )throws DuplicatedObjectException;

    public void update(HomevideoProduct product) throws DuplicatedObjectException;

    public void delete(HomevideoProduct product);

    public List<HomevideoProduct> findNMostRecent(int N);

    public List<HomevideoProduct> findNBestSeller(int N);

    public List<HomevideoProduct> findByFilters(
            String[] typeFilter,
            String[] formatFilter,
            String[] genreFilter,
            String orderFilter,
            Boolean descendant,
            int thisPage, final int multipleView, String searchString);

    public List<HomevideoProduct> findByAudiovisionWork(AudiovisionWork audiovisionWork);//tito

    public HomevideoProduct findWithLanguagesbyId(Long productId);//tito

    public void updateProductsPurchase(List<ProductInCart> productInCarts)throws AvailabilityOutOfStockException;

    public void insertInWishlistByCustomer(Customer customer,HomevideoProduct homevideoProduct);

    public List<HomevideoProduct> findWishlistbyCustomerId(Customer customer);

    public void removeFromWishlistByCustomer(Customer customer, HomevideoProduct homevideoProduct);

}
