package model.dao.db2JDBCImpl;

import model.dao.*;
import services.config.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class db2JDBCDAOFactory extends DAOFactory {
    private Connection connection;

    @Override
    public void beginTransaction() {

        try {
            Class.forName(Configuration.DATABASE_DRIVER);
            this.connection = DriverManager.getConnection(Configuration.DATABASE_URL,Configuration.USER_ID,Configuration.PASSWORD);
            this.connection.setAutoCommit(false);
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void commitTransaction() {
        try {
            this.connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void rollbackTransaction() {

        try {
            this.connection.rollback();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void closeTransaction() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CustomerDAO getCustomerDAO() {
        return new CustomerDAOdb2JDBCImpl(connection);
    }

    @Override
    public AudiovisionWorkDAO getAudiovisionWorkDAO() {
        return new AudiovisionWorkDAOdb2JDBCImpl(connection);
    }

    @Override
    public GenreDAO getGenreDAO() {
        return new GenreDAOdb2JDBCImpl(connection);
    }

    @Override
    public ActorDAO getActorDAO() {
        return new ActorDAOdb2JDBCImpl(connection);
    }

    @Override
    public AdminDAO getAdminDAO() {
        return new AdminDAOdb2JDBCImpl(connection);
    }

    @Override
    public BillingInfoDAO getBillingInfoDAO() {
        return new BillingInfoDAOdb2JDBCImpl(connection);
    }

    @Override
    public CartDAO getCartDAO() {
        return new CartDAOdb2JDBCImpl(connection);
    }

    @Override
    public CouponDAO getCouponDAO() {
        return new CouponDAOdb2JDBCImpl(connection);
    }

    @Override
    public DeliveryAddressDAO getDeliveryAddressDAO() {
        return new DeliveryAddressDAOdb2JDBCImpl(connection);
    }

    @Override
    public DirectorDAO getDirectorDAO() {
        return new DirectorDAOdb2JDBCImpl(connection);
    }

    @Override
    public OrderDAO getOrderDAO() {
        return new OrderDAOdb2JDBCImpl(connection);
    }

    @Override
    public AdminConfigurationDAO getAdminConfigurationDAO() {
        return new AdminConfigurationDAOdb2JDBCImpl(connection);
    }

    @Override
    public HomevideoProductDAO getHomevideoProductDAO() {
        return new HomevideoProductDAOdb2JDBCImpl(connection);
    }


}
