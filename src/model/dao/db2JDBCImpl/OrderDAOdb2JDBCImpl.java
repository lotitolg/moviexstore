package model.dao.db2JDBCImpl;

import model.dao.OrderDAO;
import model.mo.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderDAOdb2JDBCImpl implements OrderDAO {

    private final String COUNTER_ID = "order_id";
    Connection conn;

    public OrderDAOdb2JDBCImpl(Connection conn) {
        this.conn = conn;
    }

    /*Aggiunge una riga del ordine nel DB */
    @Override
    public void ConnectOrdertoCart(Cart cart, Coupon coupon, DeliveryAddress address, Customer customer,
                                   BillingInfo billing, Double totalSpending,double shippingPrice) {

        PreparedStatement ps;
        Order order = new Order();

        order.setCart(cart);
        order.setAddress(address);
        order.setBilling(billing);
        order.setCustomer(customer);
        order.setTotalSpending(totalSpending);
        order.setShippingPrice(shippingPrice);

        try {

            String sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();
            order.setOrderId(resultSet.getLong("counter_value"));
            resultSet.close();



            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            order.setOrderDate(date);

            sql
                    = " INSERT INTO my_order "
                    + "   ( order_id," //1
                    + "     billing_id, " //2
                    + "     customer_id, " //3
                    + "     address_id, " //4
                    + "     cart_id, " //5
                    + "     order_status, " //6
                    + "     order_date, " //7
                    + "     shipping_price," +
                    "       total_spending " //8
                    + "   ) "
                    + " VALUES (?,?,?,?,?,'In progress',?,?,?)";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++, order.getOrderId());
            ps.setLong(i++, order.getBilling().getBillingId());
            ps.setLong(i++, order.getCustomer().getCustomerId());
            ps.setLong(i++, order.getAddress().getAddressId());
            ps.setLong(i++, order.getCart().getCartId());
            ps.setDate(i++, order.getOrderDate());
            ps.setDouble(i++, order.getShippingPrice());
            ps.setDouble(i++, order.getTotalSpending());

            ps.executeUpdate();

            if (coupon != null) {

                String sql1
                        = " INSERT INTO coupon_order"
                        + " ( order_id,"
                        + " coupon_id) "
                        + " VALUES( ? , ? )";

                ps = conn.prepareStatement(sql1);

                ps.setLong(1, order.getOrderId());
                ps.setLong(2, coupon.getCouponId());

                ps.executeUpdate();
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public int numberOfOrdersForCustomer(long customerId) {

        PreparedStatement ps;
        int number = 0;

        try {
            String sql
                    = " SELECT COUNT(*) AS number"
                    + " FROM my_order"
                    + " WHERE customer_id = ? ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customerId);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                number = resultSet.getInt("number");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return number;
    }

    @Override
    public int[] ArrayOfNumberOfOrdersForCustomer(long customerId, int numberofOrders) {

        PreparedStatement ps;
        int[] ArrayOfNumbers = new int[numberofOrders];

        try {
            String sql
                    = " SELECT cart_id "
                    + " FROM my_order"
                    + " WHERE customer_id = ? ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customerId);

            ResultSet resultSet = ps.executeQuery();

            int i = 0;
            while (resultSet.next()) {
                ArrayOfNumbers[i++] = resultSet.getInt("cart_id");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return ArrayOfNumbers;
    }

    @Override
    public List<Order> ListOfOrdersForCustomer(long customerId) {

        PreparedStatement ps;
        List<Order> orderList = new ArrayList<>();

        try {
            String sql
                    = " SELECT * "
                    + " FROM my_order as o " +
                    " INNER JOIN CART on o.CART_ID = CART.CART_ID" +
                    " INNER JOIN product_in_cart on CART.CART_ID = PRODUCT_IN_CART.CART_ID" +
                    " INNER JOIN homevideo_product on PRODUCT_IN_CART.PRODUCT_ID = HOMEVIDEO_PRODUCT.PRODUCT_ID" +
                    " LEFT OUTER JOIN coupon_order as co on o.order_id = co.order_id" +
                    " LEFT OUTER JOIN coupon as c on co.coupon_id = c.coupon_id" +
                    " WHERE o.customer_id = ? " +
                    " ORDER BY order_date,o.order_id desc";

            ps = conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setLong(1, customerId);

            ResultSet resultSet = ps.executeQuery();

            long orderId = 0;
            int i = 0;

            List<ProductInCart> productInCartList;

            while (resultSet.next()) {
                productInCartList = new ArrayList<>();
                orderList.add(readwithCoupon(resultSet));
                do {
                    try {
                        orderId = resultSet.getLong("order_id");
                    } catch (SQLException ignored) {
                    }
                    if (orderId == orderList.get(i).getOrderId())
                        productInCartList.add(readProductInCartOrdered(resultSet));
                }
                while (orderId == orderList.get(i).getOrderId() && resultSet.next());
                resultSet.previous();
                ProductInCart[] productsInCart = productInCartList.toArray(new ProductInCart[0]);
                orderList.get(i).getCart().setProductInCart(productsInCart);
                i++;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return orderList;
    }

    @Override
    public void changeOrderStatus(long orderId) {

        PreparedStatement ps;

        String sql = null;
        String orderStatus = null;

        try {

            sql = "SELECT order_status FROM my_order WHERE order_id = ? ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, orderId);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                orderStatus = resultSet.getString("order_status");
            }

            if (orderStatus.equals("In progress")) {

                sql
                        = " UPDATE my_order SET"
                        + " order_status = 'shipped'," +
                        " shipping_date = current_date"
                        + " WHERE order_id = ?";

            } else {

                if (orderStatus.equals("shipped")) {
                    sql
                            = " UPDATE my_order SET"
                            + " order_status = 'delivered'," +
                            " delivery_date = current_date"
                            + " WHERE order_id = ? ";

                } else {
                    return;
                }
            }

            ps = conn.prepareStatement(sql);
            ps.setLong(1, orderId);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ArrayList<Order> listOfAllOrdersNotDelivered() {
        PreparedStatement ps;
        ArrayList<Order> listofOrders = new ArrayList<Order>();

        try {
            String sql
                    = " SELECT * "
                    + " FROM my_order INNER JOIN customer on MY_ORDER.CUSTOMER_ID = CUSTOMER.CUSTOMER_ID" +
                    " INNER JOIN delivery_address on CUSTOMER.CUSTOMER_ID = DELIVERY_ADDRESS.CUSTOMER_ID"
                    + " WHERE order_status <> 'delivered' ";

            ps = conn.prepareStatement(sql);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                listofOrders.add(readSpec(resultSet));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
        }
        return listofOrders;
    }


    Order readwithCoupon(ResultSet rs) {

        Order order = read(rs);
        Coupon coupon = new Coupon();

        /*_______Coupon______ */
        try {
            coupon.setCouponId(rs.getLong("coupon_id"));
        } catch (SQLException e) {
        }

        try {
            coupon.setDiscountPercent(rs.getInt("discount_percent"));
        } catch (SQLException e) {
        }

        try {
            coupon.setCode(rs.getString("code"));
        } catch (SQLException e) {
        }

        order.setCoupon(coupon);
        return order;
    }

    Order read(ResultSet rs) {

        Order order = new Order();
        Cart cart = new Cart();
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        BillingInfo billingInfo = new BillingInfo();
        Customer customer = new Customer();

        try {
            order.setOrderId(rs.getLong("order_id"));
        } catch (SQLException e) {
        }
        try {
            order.setOrderStatus(rs.getString("order_status"));
        } catch (SQLException e) {
        }

        try {
            order.setOrderDate(rs.getDate("order_date"));
        } catch (SQLException e) {
        }

        try {
            order.setShippingPrice(rs.getDouble("shipping_price"));
        } catch (SQLException e) {
        }

        try {
            order.setShippingDate(rs.getDate("shipping_date"));
        } catch (SQLException e) {
        }

        try {
            order.setDeliveryDate(rs.getDate("delivery_date"));
        } catch (SQLException e) {
        }

        try {
            order.setTotalSpending(rs.getDouble("total_spending"));
        } catch (SQLException e) {
        }

        try {
            cart.setCartId(rs.getLong("cart_id"));
        } catch (SQLException e) {
        }

        try {
            customer.setCustomerId(rs.getLong("customer_id"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setAddressId(rs.getLong("address_id"));
        } catch (SQLException e) {
        }

        try {
            billingInfo.setBillingId(rs.getLong("billing_id"));
        } catch (SQLException e) {
        }

        order.setCart(cart);
        order.setCustomer(customer);
        order.setBilling(billingInfo);
        order.setAddress(deliveryAddress);

        return order;
    }

    ProductInCart readProductInCartOrdered(ResultSet rs) {

        ProductInCart productInCart = new ProductInCart();
        HomevideoProduct homevideoProduct = new HomevideoProduct();

        try {
            productInCart.setItemQuantity(rs.getInt("item_quantity"));
        } catch (SQLException e) {
        }

        //homevideo_Product
        try {
            homevideoProduct.setProductId(rs.getLong("product_id"));
        } catch (SQLException e) {
        }

        try {
            homevideoProduct.setName(rs.getString("name"));
        } catch (SQLException e) {
        }

        try {
            homevideoProduct.setPrice(rs.getDouble("price"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setPicture_url(rs.getString("picture_url"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setFormat(rs.getString("format"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setNumberSupport(rs.getInt("number_support"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setAvailable(rs.getInt("available"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setDeleted(rs.getBoolean("deleted"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setReleaseDate(rs.getDate("release_date"));
        } catch (SQLException e) {
        }

        productInCart.setHomevideoProducts(homevideoProduct);
        return productInCart;
    }


    public static Order readSpec(ResultSet rs) {

        Order order = new Order();
        Customer customer = new Customer();
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        BillingInfo billingInfo = new BillingInfo();

        //______________Customer________________
        try {
            customer.setCustomerId(rs.getLong("customer_id"));
        } catch (SQLException e) {
        }

        try {
            customer.setFirstname(rs.getString("firstname"));
        } catch (SQLException e) {
        }

        try {
            customer.setSurname(rs.getString("surname"));
        } catch (SQLException e) {
        }

        try {
            customer.setEmail(rs.getString("e-mail"));
        } catch (SQLException e) {
        }
        try {
            customer.setUsername(rs.getString("username"));
        } catch (SQLException e) {
        }

        order.setCustomer(customer);

        //____________delivery Address______________
        try {
            deliveryAddress.setAddressId(rs.getLong("address_id"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setCountry(rs.getString("country"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setCity(rs.getString("city"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setAddress(rs.getString("address"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setPostalcode(rs.getString("postalcode"));
        } catch (SQLException e) {
        }
        order.setAddress(deliveryAddress);

        //____________billing Info_______________
        try {
            billingInfo.setBillingId(rs.getLong("billing_id"));
        } catch (SQLException e) {
        }
        order.setBilling(billingInfo);

        //_______________order Id___________________
        try {
            order.setOrderId(rs.getLong("order_id"));
        } catch (SQLException e) {
        }
        try {
            order.setOrderStatus(rs.getString("order_status"));
        } catch (SQLException e) {
        }

        try {
            order.setOrderDate(rs.getDate("order_date"));
        } catch (SQLException e) {
        }

        try {
            order.setShippingPrice(rs.getDouble("shipping_price"));
        } catch (SQLException e) {
        }

        try {
            order.setShippingDate(rs.getDate("shipping_date"));
        } catch (SQLException e) {
        }

        try {
            order.setDeliveryDate(rs.getDate("delivery_date"));
        } catch (SQLException e) {
        }

        return order;

    }
}
