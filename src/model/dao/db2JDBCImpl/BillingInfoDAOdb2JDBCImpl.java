package model.dao.db2JDBCImpl;

import model.dao.BillingInfoDAO;
import model.dao.exception.DuplicatedUsernameException;
import model.mo.BillingInfo;
import model.mo.Customer;

import java.sql.*;

public class BillingInfoDAOdb2JDBCImpl implements BillingInfoDAO {

    private final String COUNTER_ID = "billing_id";
    Connection conn;

    public BillingInfoDAOdb2JDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public BillingInfo insert(Customer customer, String cardNumber, Date cardexpdate, String networkCard) throws DuplicatedUsernameException {

        PreparedStatement ps;
        BillingInfo billingInfo = new BillingInfo();
        //////////////Da completare //////////////

        billingInfo.setCustomer(customer);
        billingInfo.setCardNum(cardNumber);
        billingInfo.setCardexpdate(cardexpdate);
        billingInfo.setNetworkCard(networkCard);
        billingInfo.setDeleted(false);

        try {

            String sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();
            
            billingInfo.setBillingId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql
                    = " INSERT INTO billing_info"
                    + " (billing_id, "
                    + " customer_id, "
                    + " card_num, "
                    + " cardexpdate,"
                    + " network_card,"
                    + " active ," +
                    "   used "
                    + " ) "
                    + " VALUES (?,?,?,?,?,true,false)";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++, billingInfo.getBillingId());
            ps.setLong(i++, customer.getCustomerId());
            ps.setString(i++, billingInfo.getCardNum());
            ps.setDate(i++, new Date(billingInfo.getCardexpdate().getTime()));
            ps.setString(i++, billingInfo.getNetworkCard());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return billingInfo;
    }

    @Override
    public BillingInfo findActiveBillingInfobyCustomerId(long customer_id) {

        PreparedStatement ps;
        BillingInfo billingInfo = null;

        try {
            String sql
                    = " SELECT * "
                    + " FROM billing_info "
                    + " WHERE customer_id = ?" +
                    "  AND active = true";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customer_id);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                billingInfo = readBillingInfo(resultSet);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
        }

        return billingInfo;
    }

    @Override
    public void update(long cutomer_id, BillingInfo billingInfo) {
        PreparedStatement ps;
        BillingInfo newbillingInfo = new BillingInfo();

        try {

            String sql
                    = " UPDATE billing_info "
                    + " SET card_num = ? ,"
                    + " cardexpdate = ? ,"
                    + " network_card = ?"
                    + " WHERE customer_id= ? ";

            ps = conn.prepareStatement(sql);

            int i = 1;

            ps.setString(i++, billingInfo.getCardNum());
            ps.setDate(i++, billingInfo.getCardexpdate());
            ps.setString(i++, billingInfo.getNetworkCard());
            ps.setLong(i++, cutomer_id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void setInactive(BillingInfo billingInfo) {

        PreparedStatement ps;

        try {
            String sql =
                    " UPDATE billing_info" +
                            " SET active=false" +
                            " WHERE billing_id = ?;";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, billingInfo.getBillingId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setUsed(BillingInfo billingInfo) {

        PreparedStatement ps;

        try {
            String sql =
                    " UPDATE billing_info" +
                            " SET used=true" +
                            " WHERE billing_id = ?;";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, billingInfo.getBillingId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static BillingInfo readBillingInfo(ResultSet rs) {

        BillingInfo billingInfo = new BillingInfo();

        try {
            billingInfo.setBillingId(rs.getLong("billing_id"));
        } catch (SQLException e) {
        }

        try {
            billingInfo.setCardNum(rs.getString("card_num"));
        } catch (SQLException e) {
        }

        try {
            billingInfo.setCardexpdate(rs.getDate("cardexpdate"));
        } catch (SQLException e) {
        }

        try {
            billingInfo.setNetworkCard(rs.getString("network_card"));
        } catch (SQLException e) {
        }
        try {
            billingInfo.setActive(rs.getBoolean("active"));
        } catch (SQLException e) {
        }
        try {
            billingInfo.setUsed(rs.getBoolean("used"));
        } catch (SQLException e) {
        }

        return billingInfo;
    }


}
