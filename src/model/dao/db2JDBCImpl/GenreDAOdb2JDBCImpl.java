package model.dao.db2JDBCImpl;

import model.dao.GenreDAO;
import model.dao.exception.DuplicatedObjectException;
import model.mo.Genre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GenreDAOdb2JDBCImpl implements GenreDAO {

    private final String COUNTER_ID = "genre_id";
    Connection conn;

    public GenreDAOdb2JDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Genre insert(String genreName) throws DuplicatedObjectException {

        PreparedStatement ps;
        Genre genre = new Genre();
        try {

            String sql =
                    " SELECT *" +
                            " FROM genre" +
                            " WHERE genre_name = ?" +
                            " AND deleted=false";

            ps = conn.prepareStatement(sql);
            ps.setString(1, genreName);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next())
                throw new DuplicatedObjectException("GenreDAOMySQLJDBCImpl: Genre already exists");

            sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            resultSet.next();

            genre.setGenreName(genreName);
            genre.setGenreId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql =
                    " INSERT INTO genre" +
                            "(genre_id, genre_name,deleted) " +
                            "VALUES (?,?,false)";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++, genre.getGenreId());
            ps.setString(i++, genre.getGenreName());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return genre;
    }

    @Override
    public void delete(Genre genre) {

        PreparedStatement ps;

        try {
            String sql=
                    "UPDATE genre" +
                            " SET deleted = true" +
                            " WHERE genre_id = ?";

            ps=conn.prepareStatement(sql);
            ps.setLong(1,genre.getGenreId());
            ps.executeUpdate();
            ps.close();
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Genre genre) throws DuplicatedObjectException {

        PreparedStatement ps;

        try{
            String sql =
                    "SELECT genre_id" +
                            " FROM genre" +
                            " WHERE genre_name = ?" +
                            " AND deleted = false" +
                            " AND genre_id <> ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1,genre.getGenreName());
            ps.setLong(2,genre.getGenreId());
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()){
                throw new DuplicatedObjectException("GenreDAOMySQLJDBCImpl : Genre already exists");
            }

            resultSet.close();

            sql =
                    "UPDATE genre" +
                            " SET genre_name = ?" +
                            " WHERE  genre_id = ?";

            ps=conn.prepareStatement(sql);
            ps.setString(1,genre.getGenreName());
            ps.setLong(2,genre.getGenreId());

            ps.executeUpdate();

            ps.close();

        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Genre> extractGenres() {

        PreparedStatement ps;
        List<Genre> genres = new ArrayList<>();

        try {

            String sql
                    = "SELECT * " +
                    " FROM genre " +
                    " WHERE deleted=false" +
                    " ORDER BY genre_name";

            ps = conn.prepareStatement(sql);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                genres.add(read(resultSet));
            }
            resultSet.close();
            ps.close();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return genres;
    }

    @Override
    public List<Genre> findBySearchString(String searchString,int thisPage,int multipleView) {

        PreparedStatement ps;
        List<Genre> genres = new ArrayList<>();

        try {
            String sql = "SELECT *" +
                    " FROM genre" +
                    " WHERE deleted = false";
            if (searchString != null)
                sql += " AND INSTR(genre_name,?)>0";

            sql += " ORDER BY genre_name";

            ps = conn.prepareStatement(sql);
            if (searchString != null)
                ps.setString(1, searchString);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next())
                if (resultSet.getRow() > (thisPage - 1) * multipleView && resultSet.getRow() <= (thisPage * multipleView) + 1)
                genres.add(read(resultSet));

            ps.close();
            resultSet.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return genres;
    }

    public static Genre read(ResultSet rs) {

        Genre genre = new Genre();

        try {
            genre.setGenreId(rs.getLong("genre_id"));
        } catch (SQLException e) {
        }

        try {
            genre.setGenreName(rs.getString("genre_name"));
        } catch (SQLException e) {
        }

        return genre;
    }
}
