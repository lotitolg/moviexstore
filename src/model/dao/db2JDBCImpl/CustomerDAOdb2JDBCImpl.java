package model.dao.db2JDBCImpl;

import model.dao.CustomerDAO;
import model.dao.exception.DuplicatedUsernameException;
import model.mo.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOdb2JDBCImpl implements CustomerDAO {

    private final String COUNTER_ID = "customer_id";
    Connection conn;

    public CustomerDAOdb2JDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(
            String firstname,
            String surname,
            String username,
            String password,
            String email) throws DuplicatedUsernameException {

        PreparedStatement ps;
        Customer customer = new Customer();

        customer.setUsername(username);
        customer.setPassword(password);
        customer.setFirstname(firstname);
        customer.setSurname(surname);
        customer.setEmail(email);
        customer.setDeleted(false);

        try {

            String sql
                    = "SELECT customer_id "
                    + "FROM customer "
                    + "WHERE"
                    + " username = ? AND deleted = false ";

            ps=conn.prepareStatement(sql);

            ps.setString(1, username);

            ResultSet resultSet = ps.executeQuery();

            boolean exist;
            exist = resultSet.next();

            if (exist) {
                throw new DuplicatedUsernameException("Username already exists");
            }

            sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            resultSet.next();
            customer.setCustomerId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql
                    = " INSERT INTO customer "
                    + "   ( customer_id,"
                    + "     firstname, "
                    + "     surname, "
                    + "     username, "
                    + "     password, "
                    + "     email, "
                    + "     banned, "
                    + "     deleted "
                    + "   ) "
                    + " VALUES (?,?,?,?,?,?,false,false)";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++, customer.getCustomerId());
            ps.setString(i++, customer.getFirstname());
            ps.setString(i++, customer.getSurname());
            ps.setString(i++, customer.getUsername());
            ps.setString(i++, customer.getPassword());
            ps.setString(i++, customer.getEmail());

            ps.executeUpdate();
            ps.close();
            resultSet.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Customer customer) throws DuplicatedUsernameException {

        PreparedStatement ps;

        try {

            String sql
                    = "SELECT customer_id "
                    + "FROM customer "
                    + "WHERE"
                    + " username = ? AND deleted = false" +
                    " AND customer_id <> ? ";

            ps=conn.prepareStatement(sql);

            ps.setString(1, customer.getUsername());
            ps.setLong(2,customer.getCustomerId());

            ResultSet resultSet = ps.executeQuery();

            boolean exist;
            exist = resultSet.next();

            if (exist) {
                throw new DuplicatedUsernameException("Username already exists");
            }

            sql
                    = " UPDATE customer "
                    + " SET firstname= ? , "
                    + " surname= ? ,username= ? , "
                    + " password= ? ,email= ? "
                    + " WHERE customer_id = ? ";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setString(i++, customer.getFirstname());
            ps.setString(i++, customer.getSurname());
            ps.setString(i++, customer.getUsername());
            ps.setString(i++, customer.getPassword());
            ps.setLong(i++, customer.getCustomerId());

            ps.executeUpdate();
            ps.close();
            resultSet.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void changePasword(Customer customer, String newPassword) {

        PreparedStatement ps;

        try {

            String sql
                    = " UPDATE customer "
                    + " SET password= ? "
                    + " WHERE customer_id= ? ";

            ps = conn.prepareStatement(sql);

            int i = 1;

            ps.setString(i++, customer.getPassword());
            ps.setLong(i++, customer.getCustomerId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void delete(long customerId) {

        PreparedStatement ps;

        try {

            String sql = "UPDATE customer SET"
                    + " deleted=true "
                    + " WHERE customer_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customerId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override public Customer findCustomerbyId(long customer_id) {
        PreparedStatement ps;
        Customer customer = null;

        try {
            String sql
                    = "SELECT * FROM customer WHERE customer_id = ? AND deleted = false;";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customer_id);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                customer = read(resultSet);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return customer;
    }



    @Override
    public Customer findByUsername(String username) {
        PreparedStatement ps;
        Customer customer = null;

        try {
            String sql
                    = "select * from customer where username = ? and deleted = false;";

            ps = conn.prepareStatement(sql);
            ps.setString(1, username);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                customer = read(resultSet);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return customer;
    }


    private Customer read(ResultSet rs) {

        Customer customer = new Customer();
        try {
            customer.setCustomerId(rs.getLong("customer_id"));
        } catch (SQLException e) {
        }

        try {
            customer.setFirstname(rs.getString("firstname"));
        } catch (SQLException e) {
        }

        try {
            customer.setSurname(rs.getString("surname"));
        } catch (SQLException e) {
        }
        try {
            customer.setPassword(rs.getString("password"));
        } catch (SQLException e) {
        }
        try {
            customer.setEmail(rs.getString("e-mail"));
        } catch (SQLException e) {
        }
        try {
            customer.setUsername(rs.getString("username"));
        } catch (SQLException e) {
        }
        try {
            customer.setBanned(rs.getBoolean("banned"));
        } catch (SQLException e) {
        }
        try {
            customer.setDeleted(rs.getBoolean("deleted"));
        } catch (SQLException e) {
        }

        return customer;
    }
    @Override
    public Customer updateCustomerInfo(Customer customer) throws DuplicatedUsernameException {

        PreparedStatement ps;

        try {

            String sql
                    = "SELECT customer_id "
                    + "FROM customer "
                    + "WHERE"
                    + " username = ? AND deleted = false" +
                    " AND customer_id <> ? ";

            ps=conn.prepareStatement(sql);

            ps.setString(1, customer.getUsername());
            ps.setLong(2,customer.getCustomerId());

            ResultSet resultSet = ps.executeQuery();

            boolean exist;
            exist = resultSet.next();

            if (exist) {
                throw new DuplicatedUsernameException("Username already exists");
            }

             sql
                    = " UPDATE customer "
                    + " SET firstname= ?, "
                    + " surname= ?, username= ?, "
                    + " email= ?"
                    + " WHERE customer_id= ?";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setString(i++, customer.getFirstname());
            ps.setString(i++, customer.getSurname());
            ps.setString(i++, customer.getUsername());
            ps.setString(i++, customer.getEmail());
            ps.setLong(i++, customer.getCustomerId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return customer;
    }

    public Customer findCustomerbyIdDeletedEven(long customer_id) {
        PreparedStatement ps;
        Customer customer = null;

        try {
            String sql
                    = "SELECT * FROM customer WHERE customer_id = ? ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customer_id);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                customer = read(resultSet);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return customer;
    }

    @Override
    public void undelete(long customerId) {

        PreparedStatement ps;

        try {

            String sql
                    = "UPDATE customer SET"
                    + " deleted=false "
                    + " WHERE customer_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customerId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void ban(long customerId) {
        PreparedStatement ps;

        try {

            String sql
                    = " UPDATE customer SET"
                    + " banned=true "
                    + " WHERE customer_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customerId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void unBan(long customerId) {
        PreparedStatement ps;

        try {

            String sql
                    = " UPDATE customer SET"
                    + " banned=false "
                    + " WHERE customer_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customerId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> findInitial() {
        PreparedStatement ps;
        String initial;
        List<String> initials = new ArrayList<>();

        try {

            String sql
                    = " SELECT DISTINCT UCase(Left(surname,1)) AS initial "
                    + " FROM customer "
                    + " ORDER BY UCase(Left(surname,1))";

            ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                initial = resultSet.getString("initial");
                initials.add(initial);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return initials;
    }

    @Override
    public List<Customer> findByInitialAndSearchString(String initial, String searchString) {
        PreparedStatement ps;
        Customer customer;
        List<Customer> customers = new ArrayList<>();

        try {

            String sql
                    = " SELECT * FROM customer "
                    + " WHERE true";
            if (initial != null) {
                sql += " AND UCASE(LEFT(surname,1)) = ? ";
            }
            if (searchString != null) {
                sql += " AND ( INSTR(surname,?)>0 ";
                sql += " OR INSTR(firstname,?)>0)";
            }
            sql += " ORDER BY surname, firstname";

            ps = conn.prepareStatement(sql);
            int i = 1;
            if (initial != null) {
                ps.setString(i++, initial);
            }
            if (searchString != null) {
                ps.setString(i++, searchString);
                ps.setString(i++, searchString);
            }

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                customer = read(resultSet);
                customers.add(customer);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return customers;
    }


}
