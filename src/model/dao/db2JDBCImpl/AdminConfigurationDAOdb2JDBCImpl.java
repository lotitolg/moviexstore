package model.dao.db2JDBCImpl;

import model.dao.AdminConfigurationDAO;
import model.mo.AdminConfiguration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdminConfigurationDAOdb2JDBCImpl implements AdminConfigurationDAO {

    Connection conn;

    public AdminConfigurationDAOdb2JDBCImpl(Connection conn) {
        this.conn = conn;
    }


    @Override
    public String read(String configName) {

        PreparedStatement ps;
        String valueConfig="";
        try {
            String sql =
                    "SELECT config_value " +
                            "from configuration" +
                            " WHERE config_name = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, configName);
            ResultSet resultSet=ps.executeQuery();
            if(resultSet.next()) {
                valueConfig= resultSet.getString("config_value");
            }
            ps.close();
            resultSet.close();
        }catch (SQLException e){
            throw new RuntimeException(e);
        }
        return valueConfig;
    }

    @Override
    public void update(String configName,String configValue) {

        PreparedStatement ps;

        try {
            String sql =
                    "UPDATE configuration " +
                            "SET config_value = ?" +
                            " WHERE config_name = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, configValue);
            ps.setString(2,configName);

            ps.executeUpdate();
            ps.close();

        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<AdminConfiguration> readAll() {

        List<AdminConfiguration> adminConfigurationList = new ArrayList<>();
        PreparedStatement ps;

        try {
            String sql =
                    "SELECT * " +
                            "from configuration";

            ps = conn.prepareStatement(sql);
            ResultSet resultSet=ps.executeQuery();

            while(resultSet.next()) {
                AdminConfiguration adminConfiguration = new AdminConfiguration();
                adminConfiguration.setName(resultSet.getString("config_name"));
                adminConfiguration.setValue(resultSet.getString("config_value"));
                adminConfigurationList.add(adminConfiguration);
            }
            ps.close();
            resultSet.close();
        }catch (SQLException e){
            throw new RuntimeException(e);
        }
        return adminConfigurationList;
    }
}


