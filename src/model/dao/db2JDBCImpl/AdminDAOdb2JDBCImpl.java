package model.dao.db2JDBCImpl;

import model.dao.AdminDAO;
import model.dao.exception.DuplicatedUsernameException;
import model.mo.Admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdminDAOdb2JDBCImpl implements AdminDAO {

    private final String COUNTER_ID= "admin_id";
    Connection conn;

    public AdminDAOdb2JDBCImpl(Connection conn){
        this.conn=conn;
    }

    @Override
    public Admin insert(String firstname, String surname, String username, String password)
         throws DuplicatedUsernameException {

            PreparedStatement ps;
            Admin admin = new Admin();

            admin.setUsername(username);
            admin.setPassword(password);
            admin.setFirstname(firstname);
            admin.setSurname(surname);
            admin.setDeleted(false);
            admin.setApproved(false);

            try {

                String sql
                        = "SELECT admin_id "
                        + "FROM admin "
                        + "WHERE"
                        + " username = ? AND deleted = false ";

                ps=conn.prepareStatement(sql);
                ps.setString(1, username);

                ResultSet resultSet = ps.executeQuery();

                boolean exist;
                exist = resultSet.next();

                if (exist) {
                    throw new DuplicatedUsernameException("Username already exists");
                }

                sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

                ps = conn.prepareStatement(sql);
                ps.executeUpdate();

                sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

                ps = conn.prepareStatement(sql);
                resultSet = ps.executeQuery();
                resultSet.next();
                admin.setAdminId(resultSet.getLong("counter_value"));
                resultSet.close();

                sql
                        = " INSERT INTO admin "
                        + "   ( admin_id,"
                        + "     firstname, "
                        + "     surname, "
                        + "     username, "
                        + "     password, "
                        + "     approved,"
                        + "     deleted "
                        + "   ) "
                        + " VALUES (?,?,?,?,?,false,false)";

                ps = conn.prepareStatement(sql);

                int i = 1;
                ps.setLong(i++, admin.getAdminId());
                ps.setString(i++, admin.getFirstname());
                ps.setString(i++, admin.getSurname());
                ps.setString(i++, admin.getUsername());
                ps.setString(i++, admin.getPassword());

                ps.executeUpdate();

                resultSet.close();
                ps.close();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            return admin;
    }

    @Override
    public void update(Admin admin) throws DuplicatedUsernameException {

        PreparedStatement ps;

        try {

            String sql
                    = "SELECT admin_id "
                    + "FROM admin "
                    + "WHERE"
                    + " username = ? AND deleted = false" +
                    " AND admin_id <> ? ";

            ps=conn.prepareStatement(sql);

            ps.setString(1, admin.getUsername());
            ps.setLong(2,admin.getAdminId());

            ResultSet resultSet = ps.executeQuery();

            boolean exist;
            exist = resultSet.next();

            if (exist) {
                throw new DuplicatedUsernameException("Username already exists");
            }

            sql
                    = " UPDATE admin "
                    + " SET firstname= ? , "
                    + " surname= ? ,username= ? , "
                    + " password = ? "
                    + " WHERE admin_id= ? ";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setString(i++, admin.getFirstname());
            ps.setString(i++, admin.getSurname());
            ps.setString(i++, admin.getUsername());
            ps.setString(i++, admin.getPassword());
            ps.setLong(i++, admin.getAdminId());

            ps.executeUpdate();
            ps.close();
            resultSet.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Admin admin) {

        PreparedStatement ps;

        try {

            String sql = "UPDATE admin SET"
                    + " deleted=true "
                    + " WHERE admin_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, admin.getAdminId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void approve(Admin admin) {
        PreparedStatement ps;

        try {

            String sql = "UPDATE admin SET"
                    + " approved=true "
                    + " WHERE admin_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, admin.getAdminId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> findInitial() {

        PreparedStatement ps;
        String initial;
        List<String> initials = new ArrayList<>();

        try {

            String sql
                    = " SELECT DISTINCT UCase(Left(surname,1)) AS initial "
                    + " FROM admin "
                    + " WHERE "
                    + "  deleted = false "
                    + " ORDER BY UCase(Left(surname,1))";

            ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                initial = resultSet.getString("initial");
                initials.add(initial);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return initials;
    }

    @Override
    public List<Admin> findByInitialAndSearchString(String initial, String searchString,Boolean waitApprove) {

        PreparedStatement ps;
        Admin admin;
        List<Admin> admins = new ArrayList<>();

        try {

            String sql
                    = " SELECT * FROM admin "
                    + " WHERE "
                    + " deleted=false";
            if(waitApprove!=null && waitApprove)
                sql += " AND approved=false";
            if (initial != null) {
                sql += " AND UCASE(LEFT(surname,1)) = ? ";
            }
            if (searchString != null) {
                sql += " AND ( INSTR(surname,?)>0 ";
                sql += " OR INSTR(firstname,?)>0)";
            }
            sql += " ORDER BY surname, firstname";

            ps = conn.prepareStatement(sql);
            int i = 1;
            if (initial != null) {
                ps.setString(i++, initial);
            }
            if (searchString != null) {
                ps.setString(i++, searchString);
                ps.setString(i++, searchString);
            }

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                admin = read(resultSet);
                admins.add(admin);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return admins;
    }

    @Override
    public Admin findbyAdminId(Long adminId) {
        PreparedStatement ps;
        Admin admin = null;
        try{

            String sql=
                    " SELECT *" +
                            " FROM admin" +
                            " WHERE admin_id = ?" +
                            " AND deleted = false";

            ps=conn.prepareStatement(sql);
            ps.setLong(1,adminId);

            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next())
                admin = read(resultSet);
            ps.close();
            resultSet.close();

        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return admin;
    }

    @Override
    public Admin findbyUsername(String username) {

        PreparedStatement ps;
        Admin admin = null;

        try {
            String sql
                    = "select * from admin " +
                    " where username = ? " +
                    " and deleted = false;";

            ps = conn.prepareStatement(sql);
            ps.setString(1, username);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                admin = read(resultSet);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return admin;
    }

    @Override
    public void changePasword(Admin customer, String newPassword) {

    }
    private Admin read(ResultSet rs) {

        Admin admin = new Admin();
        try {
            admin.setAdminId(rs.getLong("admin_id"));
        } catch (SQLException e) {
        }

        try {
            admin.setFirstname(rs.getString("firstname"));
        } catch (SQLException e) {
        }

        try {
            admin.setSurname(rs.getString("surname"));
        } catch (SQLException e) {
        }
        try {
            admin.setPassword(rs.getString("password"));
        } catch (SQLException e) {
        }
        try {
            admin.setUsername(rs.getString("username"));
        } catch (SQLException e) {
        }
        try {
            admin.setDeleted(rs.getBoolean("deleted"));
        } catch (SQLException e) {
        }
        try{
            admin.setApproved(rs.getBoolean("approved"));
        }catch (SQLException e){}

        return admin;
    }
}
