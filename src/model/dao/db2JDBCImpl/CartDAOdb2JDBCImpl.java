package model.dao.db2JDBCImpl;

import model.dao.CartDAO;
import model.mo.Cart;
import model.mo.Customer;
import model.mo.HomevideoProduct;
import model.mo.ProductInCart;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CartDAOdb2JDBCImpl implements CartDAO {

    private final String COUNTER_ID = "cart_id";
    Connection conn;

    public CartDAOdb2JDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public List<ProductInCart> findProductsInCartByCartId(long cartId) {

        PreparedStatement ps;
        List<ProductInCart> productsInCart = new ArrayList<>();

        try {
            String sql
                    = " SELECT *"
                    + " FROM product_in_cart INNER JOIN homevideo_product on PRODUCT_IN_CART.PRODUCT_ID = HOMEVIDEO_PRODUCT.PRODUCT_ID"
                    + " WHERE cart_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, cartId);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                productsInCart.add(read(resultSet));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return productsInCart;
    }



    @Override
    public List<ProductInCart> findCartWithProductbyUserId(long user_id) {

        PreparedStatement ps;
        List<ProductInCart> productInCart = new ArrayList<>();

        try {
            String sql
                    = " SELECT *"
                    + " FROM product_in_cart INNER JOIN cart on PRODUCT_IN_CART.CART_ID = CART.CART_ID" +
                    "  INNER JOIN homevideo_product on PRODUCT_IN_CART.PRODUCT_ID = HOMEVIDEO_PRODUCT.PRODUCT_ID"
                    + " WHERE customer_id = ? AND"
                    + " checkedout = 0 ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, user_id);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                productInCart.add(read(resultSet));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return productInCart;
    }

    @Override
    public Cart assignNewCart(Customer customer) {

        PreparedStatement ps;
        Cart cart = new Cart();

        cart.setCustomer(customer);


        try {

            String sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();
            cart.setCartId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql
                    = " INSERT INTO cart "
                    + "   ( cart_id," //1
                    + "     customer_id, " //2
                    + "     checkedout " //3
                    + "   ) "
                    + " VALUES (?,?,0)";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++, cart.getCartId());
            ps.setLong(i++, cart.getCustomer().getCustomerId());

            ps.executeUpdate();

            ps.close();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return cart;
    }

    @Override
    public void checkOutCart(Customer customer) {

        PreparedStatement ps;

        try {

            String sql
                    = "UPDATE cart SET"
                    + " checkedout = 1"
                    + " WHERE customer_id = ?";

            ps = conn.prepareStatement(sql);

            ps.setLong(1, customer.getCustomerId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }



    @Override
    public Cart findCartInfobyUserId(long userId) {

        PreparedStatement ps;
        Cart cart = new Cart();

        try {
            String sql
                    = " SELECT *"
                    + " FROM cart"
                    + " WHERE customer_id = ? AND checkedout = false";

            ps = conn.prepareStatement(sql);

            ps.setLong(1, userId);

            ResultSet resultSet = ps.executeQuery();
            
            if(resultSet.next()){
                cart = readCart(resultSet);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return cart;
    }

    @Override
    public Integer countItemsInCart(Cart cart) {

        PreparedStatement ps;
        Integer count = 0;

        try{
            String sql=" SELECT SUM(item_quantity) as count" +
                    " FROM product_in_cart" +
                    " WHERE cart_id = ?";

            ps=conn.prepareStatement(sql);
            ps.setLong(1,cart.getCartId());

            ResultSet resultSet = ps.executeQuery();

            if(resultSet.next())
                count = resultSet.getInt("count");

            ps.close();
            resultSet.close();

        }catch (SQLException e){
            throw  new RuntimeException(e);
        }
        return count;
    }

    @Override
    public void addProduct(Long productId, int quantity,Long cartId)  {

        PreparedStatement ps;
        int i;

        try{

            String sql="SELECT *" +
                    " FROM product_in_cart" +
                    " WHERE product_id = ?" +
                    " AND cart_id = ?";

            ps=conn.prepareStatement(sql);
            ps.setLong(1,productId);
            ps.setLong(2,cartId);

            ResultSet resultSet = ps.executeQuery();

            if(!resultSet.next()) {

                sql =
                        "INSERT INTO product_in_cart" +
                                " (cart_id, product_id, " +
                                " item_quantity) " +
                                " VALUES (?,?,?)";

                ps = conn.prepareStatement(sql);

                i = 1;
                ps.setLong(i++, cartId);
                ps.setLong(i++, productId);
                ps.setInt(i++, quantity);

                ps.executeUpdate();

            } else {

                sql = "UPDATE  product_in_cart" +
                        " SET item_quantity = item_quantity + ?" +
                        " WHERE cart_id = ?" +
                        " AND product_id = ?";

                ps = conn.prepareStatement(sql);

                i = 1;
                ps.setLong(i++, quantity);
                ps.setLong(i++, cartId);
                ps.setLong(i++, productId);

                ps.executeUpdate();

            }

            resultSet.close();
            ps.close();

        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void updateItemQuantity(Long productId,int quantity,Long cartId){

        PreparedStatement ps;
        int i;
        try{
            String sql = "UPDATE  product_in_cart" +
                    " SET item_quantity = ?" +
                    " WHERE cart_id = ?" +
                    " AND product_id = ?";

            ps = conn.prepareStatement(sql);

            i = 1;
            ps.setLong(i++, quantity);
            ps.setLong(i++, cartId);
            ps.setLong(i++, productId);

            ps.executeUpdate();

            ps.close();

        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void removeItem(Long productId,Long cartId){

        PreparedStatement ps;
        int i;
        try{

            String sql =
                    "DELETE FROM product_in_cart" +
                            " WHERE product_id = ?" +
                            " AND cart_id = ?;";

            ps = conn.prepareStatement(sql);

            i = 1;
            ps.setLong(i++, productId);
            ps.setLong(i++, cartId);

            ps.executeUpdate();
            ps.close();

        }  catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    ProductInCart read(ResultSet rs) {

        ProductInCart productInCart = new ProductInCart();
        Cart cart = new Cart();
        HomevideoProduct homevideoProduct = new HomevideoProduct();

        //productIncart
        try {
            productInCart.setItemQuantity(rs.getInt("item_quantity"));
        } catch (SQLException e) {
        }

        //homevideo_Product
        try {
            homevideoProduct.setProductId(rs.getLong("product_id"));
        } catch (SQLException e) {
        }

        try {
            homevideoProduct.setName(rs.getString("name"));
        } catch (SQLException e) {
        }

        try {
            homevideoProduct.setPrice(rs.getDouble("price"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setPicture_url(rs.getString("picture_url"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setFormat(rs.getString("format"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setNumberSupport(rs.getInt("number_support"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setAvailable(rs.getInt("available"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setDeleted(rs.getBoolean("deleted"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setReleaseDate(rs.getDate("release_date"));
        } catch (SQLException e) {
        }
        //Cart
        try {
            cart.setCartId(rs.getLong("cart_id"));
        } catch (SQLException e) { }

        try {
            cart.setCheckedOut(rs.getString("cheackedout"));
        } catch (SQLException e) {
        }

        productInCart.setHomevideoProducts(homevideoProduct);
        productInCart.setCart(cart);

        return productInCart;
    }

    Cart readCart(ResultSet rs) {

        Cart cart = new Cart();

        //Cart
        try {
            cart.setCartId(rs.getLong("cart_id"));
        } catch (SQLException e) {
        }
        try {
            cart.setCheckedOut(rs.getString("cheackedout"));
        } catch (SQLException e) {
        }

        return cart;
        
    }

}
