package model.dao;

import model.dao.exception.DuplicatedObjectException;
import model.mo.AudiovisionWork;
import model.mo.Director;

import java.util.List;

public interface DirectorDAO {


    public Director insert(String directorName) throws DuplicatedObjectException;

    public void delete(Director director);

    public void update(Director director);

    public List<Director> extractDirectors();

    public List<Director> findByAudiovisionWork(AudiovisionWork audiovisionWork);

    public List<Director> findBySearchString(String searchString);
}
