package model.dao;

import java.util.List;

import model.dao.exception.DuplicatedObjectException;
import model.mo.Genre;

public interface GenreDAO {
    
    public Genre insert(
            String genreName)throws DuplicatedObjectException;
    
    public void delete(Genre genre);
    
    public void update(Genre genre)throws DuplicatedObjectException;;

    public List<Genre> extractGenres();

    public List<Genre> findBySearchString(String searchString,int thisPage,int multipleView);
        
    
    
}
