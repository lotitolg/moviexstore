package model.dao.mySQLJDBCImpl;

import model.dao.DeliveryAddressDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.mo.Customer;
import model.mo.DeliveryAddress;

public class DeliveryAddressDAOMySQLJDBCImpl implements DeliveryAddressDAO {

    private final String COUNTER_ID = "address_id";
    Connection conn;

    public DeliveryAddressDAOMySQLJDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public DeliveryAddress insert(Customer customer, String country, String city, String address, String postalcode) {

        PreparedStatement ps;
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setCountry(country);
        deliveryAddress.setCity(city);
        deliveryAddress.setAddress(address);
        deliveryAddress.setPostalcode(postalcode);
        deliveryAddress.setCustomer(customer);

        try {

            String sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            deliveryAddress.setAddressId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql
                    = "INSERT INTO delivery_address"
                    + " (address_id,"
                    + " customer_id,"
                    + " country,"
                    + " city,"
                    + " address,"
                    + " postalcode," +
                    "   active,"+
                    "   used"
                    + " )    "
                    + " VALUES(?,?,?,?,?,?,true,false)";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++, deliveryAddress.getAddressId());
            ps.setLong(i++, customer.getCustomerId());
            ps.setString(i++, deliveryAddress.getCountry());
            ps.setString(i++, deliveryAddress.getCity());
            ps.setString(i++, deliveryAddress.getAddress());
            ps.setString(i++, deliveryAddress.getPostalcode());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return deliveryAddress;

    }

    @Override
    public DeliveryAddress getActiveAddressbyCustomer(long customerId) {

        PreparedStatement ps;
        DeliveryAddress deliveryAddress = null;

        try {

            String sql
                    = " SELECT * "
                    + " FROM delivery_address"
                    + " WHERE customer_id = ?" +
                    "   AND active = true";

            ps = conn.prepareStatement(sql);

            ps.setLong(1, customerId);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                deliveryAddress = readDeliveryAddress(resultSet);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return deliveryAddress;
    }



    @Override
    public DeliveryAddress update(DeliveryAddress deliveryAddress) {

        PreparedStatement ps;

        try {

            String sql
                    = " UPDATE delivery_address"
                    + " SET country = ? ,"
                    + " city= ? ,"
                    + " address= ? , "
                    + " postalcode= ? "
                    + " WHERE address_id = ? ";

            ps = conn.prepareStatement(sql);

            int i = 1;

            ps.setString(i++, deliveryAddress.getCountry());
            ps.setString(i++, deliveryAddress.getCity());
            ps.setString(i++, deliveryAddress.getAddress());
            ps.setString(i++, deliveryAddress.getPostalcode());
            ps.setLong(i++, deliveryAddress.getAddressId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return deliveryAddress;
    }

    @Override
    public List<DeliveryAddress> getAddressesByCustomer(Customer customer) {
        PreparedStatement ps;
        List<DeliveryAddress> deliveryAddresses = new ArrayList<>();

        try {

            String sql
                    = " SELECT * "
                    + " FROM delivery_address"
                    + " WHERE customer_id = ?";

            ps = conn.prepareStatement(sql);

            ps.setLong(1, customer.getCustomerId());

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                deliveryAddresses.add(readDeliveryAddress(resultSet));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return deliveryAddresses;
    }

    @Override
    public void setInactive(DeliveryAddress deliveryAddress){

        PreparedStatement ps;

        try {
            String sql =
                    " UPDATE delivery_address" +
                            " SET active=false" +
                            " WHERE address_id = ?;";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, deliveryAddress.getAddressId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setUsed(DeliveryAddress deliveryAddress) {
        PreparedStatement ps;

        try {
            String sql =
                    " UPDATE delivery_address" +
                            " SET used=true" +
                            " WHERE address_id = ?;";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, deliveryAddress.getAddressId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static DeliveryAddress readDeliveryAddress(ResultSet rs) {

        DeliveryAddress deliveryAddress = new DeliveryAddress();

        try {
            deliveryAddress.setAddressId(rs.getLong("address_id"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setCountry(rs.getString("country"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setCity(rs.getString("city"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setAddress(rs.getString("address"));
        } catch (SQLException e) {
        }

        try {
            deliveryAddress.setPostalcode(rs.getString("postalcode"));
        } catch (SQLException e) {
        }
        try {
            deliveryAddress.setActive(rs.getBoolean("active"));
        } catch (SQLException e) {
        }
        try {
            deliveryAddress.setUsed(rs.getBoolean("used"));
        } catch (SQLException e) {
        }

        return deliveryAddress;

    }
}

