package model.dao.mySQLJDBCImpl;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import model.dao.*;

import services.config.Configuration;

public class MySQLJDBCDAOFactory extends DAOFactory {

    private Connection connection;

    @Override
    public void beginTransaction() {

        try {
            Class.forName(Configuration.DATABASE_DRIVER);
            this.connection = DriverManager.getConnection(Configuration.DATABASE_URL);
            this.connection.setAutoCommit(false);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void commitTransaction() {
        try {
            this.connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void rollbackTransaction() {

        try {
            this.connection.rollback();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void closeTransaction() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CustomerDAO getCustomerDAO() {
        return new CustomerDAOMySQLJDBCImpl(connection);
    }

    @Override
    public AudiovisionWorkDAO getAudiovisionWorkDAO() {
        return new AudiovisionWorkDAOMySQLJDBCImpl(connection);
    }

    @Override
    public GenreDAO getGenreDAO() {
        return new GenreDAOMySQLJDBCImpl(connection);
    }

    @Override
    public ActorDAO getActorDAO() {
        return new ActorDAOMySQLJDBCImpl(connection);
    }

    @Override
    public AdminDAO getAdminDAO() {
        return new AdminDAOMySQLJDBCImpl(connection);
    }

    @Override
    public BillingInfoDAO getBillingInfoDAO() {
        return new BillingInfoDAOMySQLJDBCImpl(connection);
    }

    @Override
    public CartDAO getCartDAO() {
        return new CartDAOMySQLJDBCImpl(connection);
    }

    @Override
    public CouponDAO getCouponDAO() {
        return new CouponDAOMySQLJDBCImpl(connection);
    }

    @Override
    public DeliveryAddressDAO getDeliveryAddressDAO() {
        return new DeliveryAddressDAOMySQLJDBCImpl(connection);
    }

    @Override
    public DirectorDAO getDirectorDAO() {
        return new DirectorDAOMySQLJDBCImpl(connection);
    }

    @Override
    public OrderDAO getOrderDAO() {
        return new OrderDAOMySQLJDBCImpl(connection);
    }

    @Override
    public AdminConfigurationDAO getAdminConfigurationDAO() {
        return new AdminConfigurationDAOMySQLJDBCImpl(connection);
    }

    @Override
    public HomevideoProductDAO getHomevideoProductDAO() {
        return new HomevideoProductDAOMySQLJDBCImpl(connection);
    }


}
