package model.dao.mySQLJDBCImpl;

import model.dao.AudiovisionWorkDAO;

import model.dao.exception.DuplicatedObjectException;
import model.mo.Actor;
import model.mo.AudiovisionWork;
import model.mo.Director;
import model.mo.Genre;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AudiovisionWorkDAOMySQLJDBCImpl implements AudiovisionWorkDAO {

    private final String COUNTER_ID = "work_id";
    Connection conn;

    public AudiovisionWorkDAOMySQLJDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public AudiovisionWork insert(String title,
                                  String type,
                                  int duration,
                                  float rating,
                                  String yearProduction,
                                  String plot,
                                  String trailerUrl,
                                  String studioProduction,
                                  String imageWork,
                                  Genre[] genres,
                                  Actor[] actors,
                                  Director[] directors)
            throws DuplicatedObjectException {

        PreparedStatement ps;

        AudiovisionWork audiovisionWork = new AudiovisionWork();
        audiovisionWork.setOriginalTitle(title);
        audiovisionWork.setType(type);
        audiovisionWork.setDuration(duration);
        audiovisionWork.setRating(rating);
        audiovisionWork.setYearProdction(yearProduction);
        audiovisionWork.setPlot(plot);
        audiovisionWork.setTrailerUrl(trailerUrl);
        audiovisionWork.setStudioProduction(studioProduction);
        audiovisionWork.setImageWork(imageWork);

        audiovisionWork.setGenres(genres);
        audiovisionWork.setActors(actors);
        audiovisionWork.setDirectors(directors);


        try {

            String sql =
                    "SELECT work_id" +
                            " FROM audiovision_work" +
                            " WHERE original_title = ?" +
                            " AND duration = ?" +
                            " AND studio_production = ?" +
                            " AND year_production =  ?" +
                            " AND deleted = false";

            ps = conn.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, audiovisionWork.getOriginalTitle());
            ps.setInt(i++, audiovisionWork.getDuration());
            ps.setString(i++, audiovisionWork.getStudioProduction());
            ps.setString(i++, audiovisionWork.getYearProdction());

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                throw new DuplicatedObjectException("AudiovisionWorkDAOMySQLJDBCImpl: Audiovision Work already exists");
            }

            sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.next();

            audiovisionWork.setWorkId(rs.getLong("counter_value"));

            rs.close();

            sql =
                    " INSERT INTO audiovision_work" +
                            "(work_id, original_title, type, studio_production, year_production, plot, duration, rating, trailer_url, image_work,deleted)" +
                            " VALUES (?,?,?,?,?,?,?,?,?,?,false);";
            ps = conn.prepareStatement(sql);

            i = 1;
            ps.setLong(i++, audiovisionWork.getWorkId());
            ps.setString(i++, audiovisionWork.getOriginalTitle());
            ps.setString(i++, audiovisionWork.getType());
            ps.setString(i++, audiovisionWork.getStudioProduction());
            ps.setString(i++, audiovisionWork.getYearProdction());
            ps.setString(i++, audiovisionWork.getPlot());
            ps.setInt(i++, audiovisionWork.getDuration());
            ps.setDouble(i++, audiovisionWork.getRating());
            ps.setString(i++, audiovisionWork.getTrailerUrl());
            ps.setString(i++, audiovisionWork.getImageWork());
            ps.executeUpdate();
            int k;

            if (audiovisionWork.getGenres() != null) {

                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO work_genre" +
                                " (work_id, genre_id) " +
                                " VALUES (?,?)");
                for (k = 1; k < audiovisionWork.getGenres().length; k++) {
                    sqlBuilder.append(", (?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);
                i = 1;
                for (k = 0; k < audiovisionWork.getGenres().length; k++) {
                    ps.setLong(k + i++, audiovisionWork.getWorkId());
                    ps.setLong(k + i, audiovisionWork.getGenre(k).getGenreId());
                }

                ps.executeUpdate();

            }
            if (audiovisionWork.getActors() != null) {

                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO work_actor" +
                                " (work_id, actor_id) " +
                                " VALUES (?,?)");
                for (k = 1; k < audiovisionWork.getActors().length; k++) {
                    sqlBuilder.append(",(?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);

                i = 1;
                for (k = 0; k < audiovisionWork.getActors().length; k++) {
                    ps.setLong(k + i++, audiovisionWork.getWorkId());
                    ps.setLong(k + i, audiovisionWork.getActor(k).getActorId());
                }
                ps.executeUpdate();
            }
            if (audiovisionWork.getDirectors() != null) {

                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO work_director" +
                                " (work_id, director_id) " +
                                " VALUES (?,?)");
                for (k = 1; k < audiovisionWork.getDirectors().length; k++) {
                    sqlBuilder.append(",(?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);
                i = 1;
                for (k = 0; k < audiovisionWork.getDirectors().length; k++) {
                    ps.setLong(k + i++, audiovisionWork.getWorkId());
                    ps.setLong(k + i, audiovisionWork.getDirector(k).getDirectorId());
                }
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return audiovisionWork;
    }

    @Override
    public void delete(AudiovisionWork audiovisionWork) {

        PreparedStatement ps;

        try {
            String sql =
                    " UPDATE  audiovision_work" +
                            " SET deleted = true" +
                            " WHERE work_id = ?";


            ps = conn.prepareStatement(sql);
            ps.setLong(1, audiovisionWork.getWorkId());
            ps.executeUpdate();

            sql =
                    "UPDATE homevideo_product" +
                            " SET deleted = true," +
                            " available = 0" +
                            " WHERE  work_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, audiovisionWork.getWorkId());
            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(AudiovisionWork audiovisionWork) throws DuplicatedObjectException {

        PreparedStatement ps;

        try {

            String sql =
                    "SELECT work_id" +
                            " FROM audiovision_work" +
                            " WHERE original_title = ?" +
                            " AND duration = ?" +
                            " AND studio_production = ?" +
                            " AND year_production =  ?" +
                            " AND deleted = false" +
                            " AND work_id <> ?;";

            ps = conn.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, audiovisionWork.getOriginalTitle());
            ps.setInt(i++, audiovisionWork.getDuration());
            ps.setString(i++, audiovisionWork.getStudioProduction());
            ps.setString(i++, audiovisionWork.getYearProdction());
            ps.setLong(i++, audiovisionWork.getWorkId());

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                throw new DuplicatedObjectException("AudiovisionWorkDAOMySQLJDBCImpl: Audiovision Work already exists");
            }

            rs.close();

            sql =
                    "UPDATE audiovision_work" +
                            " SET" +
                            " original_title = ?," +
                            " type = ?," +
                            " studio_production = ?," +
                            " year_production = ?," +
                            " plot = ?," +
                            " duration = ?," +
                            " rating = ?," +
                            " trailer_url = ?," +
                            " image_work = ?" +
                            " WHERE work_id = ?;";

            ps = conn.prepareStatement(sql);
            i = 1;
            ps.setString(i++, audiovisionWork.getOriginalTitle());
            ps.setString(i++, audiovisionWork.getType());
            ps.setString(i++, audiovisionWork.getStudioProduction());
            ps.setString(i++, audiovisionWork.getYearProdction());
            ps.setString(i++, audiovisionWork.getPlot());
            ps.setInt(i++, audiovisionWork.getDuration());
            ps.setFloat(i++, audiovisionWork.getRating());
            ps.setString(i++, audiovisionWork.getTrailerUrl());
            ps.setString(i++, audiovisionWork.getImageWork());
            ps.setLong(i++, audiovisionWork.getWorkId());

            ps.executeUpdate();

            int k;

            if (audiovisionWork.getGenres() != null) {

                sql =
                        "DELETE FROM work_genre" +
                                " WHERE work_id = ?;";

                ps = conn.prepareStatement(sql);
                ps.setLong(1, audiovisionWork.getWorkId());
                ps.executeUpdate();

                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO work_genre" +
                                " (work_id, genre_id) " +
                                " VALUES (?,?)");
                for (k = 1; k < audiovisionWork.getGenres().length; k++) {
                    sqlBuilder.append(",(?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);
                i = 1;
                for (k = 0; k < audiovisionWork.getGenres().length; k++) {
                    ps.setLong(k + i++, audiovisionWork.getWorkId());
                    ps.setLong(k + i, audiovisionWork.getGenre(k).getGenreId());
                }

                ps.executeUpdate();
            }
            if (audiovisionWork.getActors() != null) {

                sql =
                        "DELETE FROM work_actor" +
                                " WHERE work_id = ?;";

                ps = conn.prepareStatement(sql);
                ps.setLong(1, audiovisionWork.getWorkId());
                ps.executeUpdate();


                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO work_actor" +
                                " (work_id, actor_id) " +
                                " VALUES (?,?)");
                for (k = 1; k < audiovisionWork.getActors().length; k++) {
                    sqlBuilder.append(", (?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);

                i = 1;
                for (k = 0; k < audiovisionWork.getActors().length; k++) {
                    ps.setLong(k + i++, audiovisionWork.getWorkId());
                    ps.setLong(k + i, audiovisionWork.getActor(k).getActorId());
                }
                ps.executeUpdate();
            }


            for (k = 0; k < audiovisionWork.getDirectors().length; k++) {

                sql =
                        "DELETE FROM work_director" +
                                " WHERE work_id = ?;";

                ps = conn.prepareStatement(sql);
                ps.setLong(1, audiovisionWork.getWorkId());
                ps.executeUpdate();

                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO work_director" +
                                " (work_id, director_id) " +
                                " VALUES (?,?)");
                for (k = 1; k < audiovisionWork.getDirectors().length; k++) {
                    sqlBuilder.append(",(?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);
                i = 1;
                for (k = 0; k < audiovisionWork.getDirectors().length; k++) {
                    ps.setLong(k + i++, audiovisionWork.getWorkId());
                    ps.setLong(k + i, audiovisionWork.getDirector(k).getDirectorId());
                }
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<AudiovisionWork> findNTopRated(int N) {

        PreparedStatement ps;
        List<AudiovisionWork> audiovisionWorks = new ArrayList<>();


        try {
            String sql =
                    "select work_id,original_title,image_work,rating " +
                            "from audiovision_work" +
                            " WHERE deleted = false" +
                            " order by rating desc " +
                            "LIMIT ?;";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, N);

            ResultSet resultSet = ps.executeQuery();


            while (resultSet.next()) {
                audiovisionWorks.add(read(resultSet));
            }

            resultSet.close();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return audiovisionWorks;
    }

    @Override
    public List<AudiovisionWork> findByFilters(
            String[] typeFilter,
            String[] formatFilter,
            String[] genreFilter,
            String orderFilter,
            Boolean descendant,
            int thisPage, final int multipleView, String searchString) {

        PreparedStatement ps;
        List<AudiovisionWork> audiovisionWorks = new ArrayList<>();

        int i;

        try {

            String sql =
                    " SELECT DISTINCT work_id,original_title,image_work,rating" +
                            " FROM audiovision_work" +
                            " WHERE deleted = false";

            StringBuilder sqlBuilder = new StringBuilder(sql);

            if (formatFilter != null) {
                sqlBuilder.append(" AND work_id IN (");
                sqlBuilder.append(" SELECT work_id FROM homevideo_product");
                sqlBuilder.append(" WHERE (format = ? ");

                for (i = 1; i < formatFilter.length; i++) {
                    sqlBuilder.append(" OR format = ? ");
                }
                sqlBuilder.append(") AND deleted = false)");
            }

            if (typeFilter != null) {
                sqlBuilder.append(" AND (type = ?");
                for (i = 1; i < typeFilter.length; i++) {
                    sqlBuilder.append(" OR type = ?");
                }
                sqlBuilder.append(")");
            }

            if (genreFilter != null) {
                sqlBuilder.append(" AND work_id IN (");
                sqlBuilder.append(" SELECT work_id");
                sqlBuilder.append(" FROM work_genre NATURAL JOIN genre");
                sqlBuilder.append(" WHERE (genre_name = ?");
                for (i = 1; i < genreFilter.length; i++) {
                    sqlBuilder.append(" OR genre_name = ?");
                }
                sqlBuilder.append(") AND deleted = false)");
            }

            if (searchString != null) {

                sqlBuilder.append(" AND ( INSTR(original_title,?)>0");

                sqlBuilder.append(" OR work_id IN (");
                sqlBuilder.append(" SELECT work_id");
                sqlBuilder.append(" FROM work_actor NATURAL JOIN actor");
                sqlBuilder.append(" WHERE INSTR(actor_name,?)>0");
                sqlBuilder.append(" AND deleted = false)");

                sqlBuilder.append(" OR work_id IN (");
                sqlBuilder.append(" SELECT work_id");
                sqlBuilder.append(" FROM work_director NATURAL JOIN director");
                sqlBuilder.append(" WHERE INSTR(director_name,?)>0");
                sqlBuilder.append(" AND deleted = false)");

                sqlBuilder.append(")");

            }

            if (orderFilter != null) {
                if (orderFilter.equals("original_title"))
                    sqlBuilder.append(" ORDER BY original_title");
                else if (orderFilter.equals("rating"))
                    sqlBuilder.append(" ORDER BY rating");
                else if (orderFilter.equals("year_production"))
                    sqlBuilder.append(" ORDER BY year_production");
            } else {
                sqlBuilder.append(" ORDER BY original_title");
            }

            if (descendant)
                sqlBuilder.append(" DESC");
            sqlBuilder.append(";");
            sql = sqlBuilder.toString();

            int k = 1;
            ps = conn.prepareStatement(sql);
            if (formatFilter != null) {
                for (i = 0; i < formatFilter.length; i++)
                    ps.setString(k++, formatFilter[i]);
            }
            if (typeFilter != null) {
                for (i = 0; i < typeFilter.length; i++)
                    ps.setString(k++, typeFilter[i]);
            }
            if (genreFilter != null) {
                for (i = 0; i < genreFilter.length; i++)
                    ps.setString(k++, genreFilter[i]);
            }
            if (searchString != null) {
                ps.setString(k++, searchString);
                ps.setString(k++, searchString);
                ps.setString(k++, searchString);
            }

            ResultSet resultSet = ps.executeQuery();


            while (resultSet.next()) {
                if (resultSet.getRow() > (thisPage - 1) * multipleView && resultSet.getRow() <= (thisPage * multipleView) + 1)
                    audiovisionWorks.add(read(resultSet));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return audiovisionWorks;
    }

    @Override
    public AudiovisionWork findWithGenresbyId(Long workid) {

        PreparedStatement ps;
        AudiovisionWork audiovisionWork = null;

        try {

            String sql =
                    "SELECT * " +
                            " FROM audiovision_work AS aw" +
                            " LEFT OUTER JOIN work_genre as wg" +
                            " ON aw.work_id = wg.work_id" +
                            " LEFT OUTER JOIN genre as g" +
                            " ON wg.genre_id = g.genre_id" +
                            " WHERE aw.work_id = ? " +
                            " AND aw.deleted = false" +
                            " AND g.deleted = false";

            ps = conn.prepareStatement(sql);

            ps.setLong(1, workid);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                audiovisionWork = read(resultSet);
                List<Genre> genres = new ArrayList<>();
                resultSet.previous();
                while (resultSet.next()) {
                    Genre genre = new Genre();
                    try {
                        genre.setGenreName(resultSet.getString("genre_name"));
                    } catch (SQLException e) {
                    }
                    genres.add(genre);
                }
                Genre[] array = new Genre[genres.size()];
                array = genres.toArray(array);
                audiovisionWork.setGenres(array);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return audiovisionWork;
    }

    AudiovisionWork read(ResultSet rs) {

        AudiovisionWork audiovisionWork = new AudiovisionWork();

        try {
            audiovisionWork.setWorkId(rs.getLong("work_id"));
        } catch (SQLException e) {
        }

        try {
            audiovisionWork.setOriginalTitle(rs.getString("original_title"));
        } catch (SQLException e) {
        }

        try {
            audiovisionWork.setType(rs.getString("type"));
        } catch (SQLException e) {
        }
        try {
            audiovisionWork.setStudioProduction(rs.getString("studio_production"));
        } catch (SQLException e) {
        }
        try {
            audiovisionWork.setYearProdction(rs.getString("year_production"));
        } catch (SQLException e) {
        }
        try {
            audiovisionWork.setPlot(rs.getString("plot"));
        } catch (SQLException e) {
        }
        try {
            audiovisionWork.setDuration(rs.getInt("duration"));
        } catch (SQLException e) {
        }
        try {
            audiovisionWork.setRating(rs.getFloat("rating"));
        } catch (SQLException e) {
        }
        try {
            audiovisionWork.setTrailerUrl(rs.getString("trailer_url"));
        } catch (SQLException e) {
        }

        try {
            audiovisionWork.setImageWork(rs.getString("image_work"));
        } catch (SQLException e) {
        }


        return audiovisionWork;

    }


}
