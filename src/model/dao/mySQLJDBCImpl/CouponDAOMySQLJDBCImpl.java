package model.dao.mySQLJDBCImpl;

import model.dao.CouponDAO;
import model.dao.exception.DuplicatedObjectException;
import model.mo.Coupon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.dao.exception.DuplicatedUsernameException;
import model.mo.Order;

public class CouponDAOMySQLJDBCImpl implements CouponDAO {

    private final String COUNTER_ID = "coupon_id";
    Connection conn;

    public CouponDAOMySQLJDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(String code, int discountPercent) throws DuplicatedObjectException {

        PreparedStatement ps;
        Coupon coupon = new Coupon();

        coupon.setCode(code);
        coupon.setDiscountPercent(discountPercent);

        try {

            String sql =
                    "SELECT *" +
                            " FROM coupon" +
                            " WHERE code = ? ";

            ps=conn.prepareStatement(sql);
            ps.setString(1,coupon.getCode());
            ResultSet resultSet = ps.executeQuery();

            if(resultSet.next())
                throw new DuplicatedObjectException("Duplicated Coupon");


            sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            resultSet.next();
            coupon.setCouponId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql
                    = " INSERT INTO coupon "
                    + " (coupon_id,"
                    + " code, "
                    + " discount_percent)"
                    + " VALUES (?,?,?)";

            ps = conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++, coupon.getCouponId());
            ps.setString(i++, coupon.getCode());
            ps.setInt(i++, coupon.getDiscountPercent());

            ps.executeUpdate();

            ps.close();
            resultSet.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Coupon coupon) {

        PreparedStatement ps;

        try {

            String sql
                    = " UPDATE coupon set"
                    + " discount_percent = ? , "
                    + " code = ?"
                    + " WHERE coupon_id = ? ";

            ps = conn.prepareStatement(sql);

            ps.setInt(1, coupon.getDiscountPercent());
            ps.setString(1, coupon.getCode());
            ps.setLong(1, coupon.getCouponId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Coupon findCouponbyCode(String couponCode) {

        PreparedStatement ps;
        Coupon coupon = null;

        try {
            String sql
                    = " SELECT *"
                    + " FROM coupon"
                    + " WHERE code = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, couponCode);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                coupon = read(resultSet);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
        }
        return coupon;

    }

    public static Coupon read(ResultSet rs) {

        Coupon coupon = new Coupon();

        try {
            coupon.setCouponId(rs.getLong("coupon_id"));
        } catch (SQLException e) {
        }

        try {
            coupon.setCode(rs.getString("code"));
        } catch (SQLException e) {
        }

        try {
            coupon.setDiscountPercent(rs.getInt("discount_percent"));
        } catch (SQLException e) {
        }

        try {
            coupon.setDisabled(rs.getBoolean("disabled"));
        } catch (SQLException e) {
        }

        return coupon;
    }

    @Override
    public ArrayList<Coupon> findAllCoupons() {

        ArrayList<Coupon> ListOfCoupons = new ArrayList<>();
        PreparedStatement ps;

        try {

            String sql
                    = " SELECT * "
                    + " FROM `coupon`"
                    + " ORDER BY code ";

            ps = conn.prepareStatement(sql);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                ListOfCoupons.add(read(resultSet));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ListOfCoupons;
    }

    @Override
    public void disableCoupon(long couponId) {

        PreparedStatement ps;

        try {

            String sql
                    = " UPDATE coupon SET"
                    + " disabled=true "
                    + " WHERE coupon_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, couponId);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void unDisableCoupon(long couponId) {

        PreparedStatement ps;

        try {

            String sql
                    = " UPDATE coupon SET"
                    + " disabled=false "
                    + " WHERE coupon_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, couponId);

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
