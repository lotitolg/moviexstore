package model.dao.mySQLJDBCImpl;

import model.dao.HomevideoProductDAO;
import model.dao.exception.AvailabilityOutOfStockException;
import model.dao.exception.DuplicatedObjectException;
import model.mo.AudiovisionWork;
import model.mo.Customer;
import model.mo.HomevideoProduct;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.mo.ProductInCart;

public class HomevideoProductDAOMySQLJDBCImpl implements HomevideoProductDAO {

    private final String COUNTER_ID = "product_id";
    Connection conn;

    public HomevideoProductDAOMySQLJDBCImpl(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(AudiovisionWork audiovisionWork,
                       String name,
                       Double price,
                       String picture_url,
                       String format,
                       int numberSupport,
                       int available,
                       Date releaseDate,
                       String[] languages) throws DuplicatedObjectException {

        PreparedStatement ps;

        HomevideoProduct homevideoProduct = new HomevideoProduct();
        homevideoProduct.setAudiovisionWork(audiovisionWork);
        homevideoProduct.setName(name);
        homevideoProduct.setPrice(price);
        homevideoProduct.setPicture_url(picture_url);
        homevideoProduct.setFormat(format);
        homevideoProduct.setNumberSupport(numberSupport);
        homevideoProduct.setAvailable(available);
        homevideoProduct.setReleaseDate(releaseDate);
        homevideoProduct.setLanguages(languages);

        try {

            String sql =
                    "SELECT product_id" +
                            " FROM homevideo_product" +
                            " WHERE deleted = false" +
                            " AND name = ?" +
                            " AND format = ?" +
                            " AND work_id = ?;";

            ps = conn.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, homevideoProduct.getName());
            ps.setString(i++, homevideoProduct.getFormat());
            ps.setLong(i++, homevideoProduct.getAudiovisionWork().getWorkId());

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                throw new DuplicatedObjectException("HomevideoProductDAOMySQLJDBCImpl: Homevideo already exists");
            }

            sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.next();

            homevideoProduct.setProductId(rs.getLong("counter_value"));

            rs.close();

            sql =
                    " INSERT INTO homevideo_product" +
                            "( product_id, work_id, name, price, picture_url, format, number_support, release_date, available, deleted)" +
                            " VALUES (?,?,?,?,?,?,?,?,?,false);";
            ps = conn.prepareStatement(sql);

            i = 1;
            ps.setLong(i++, homevideoProduct.getProductId());
            ps.setLong(i++, homevideoProduct.getAudiovisionWork().getWorkId());
            ps.setString(i++, homevideoProduct.getName());
            ps.setDouble(i++, homevideoProduct.getPrice());
            ps.setString(i++, homevideoProduct.getPicture_url());
            ps.setString(i++, homevideoProduct.getFormat());
            ps.setInt(i++, homevideoProduct.getNumberSupport());
            ps.setDate(i++, homevideoProduct.getReleaseDate() != null ?
                    new java.sql.Date(homevideoProduct.getReleaseDate().getTime()) : null);
            ps.setInt(i++, homevideoProduct.getAvailable());

            ps.executeUpdate();

            if (homevideoProduct.getLanguages() != null) {

                int k;
                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO homevideo_language" +
                                " (product_id, language) " +
                                " VALUES (?,?)");
                for (k = 1; k < homevideoProduct.getLanguages().length; k++) {
                    sqlBuilder.append(",(?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);
                i = 1;
                for (k = 0; k < homevideoProduct.getLanguages().length; k++) {
                    ps.setLong(k + i++, homevideoProduct.getProductId());
                    ps.setString(k + i, homevideoProduct.getLanguage(k));
                }
                ps.executeUpdate();
            }

        } catch (
                SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void update(HomevideoProduct homevideoProduct) throws DuplicatedObjectException {

        PreparedStatement ps;

        try {

            String sql =
                    "SELECT product_id" +
                            " FROM homevideo_product" +
                            " WHERE deleted = false" +
                            " AND name = ?" +
                            " AND format = ?" +
                            " AND work_id = ?" +
                            " AND product_id <> ?;";

            ps = conn.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, homevideoProduct.getName());
            ps.setString(i++, homevideoProduct.getFormat());
            ps.setLong(i++, homevideoProduct.getAudiovisionWork().getWorkId());
            ps.setLong(i++, homevideoProduct.getProductId());

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                throw new DuplicatedObjectException("HomevideoProductDAOMySQLJDBCImpl: Homevideo already exists");
            }

            sql =
                    " UPDATE  homevideo_product" +
                            " SET" +
                            " work_id = ?," +
                            " name=?," +
                            " picture_url = ?," +
                            " release_date = ?," +
                            " price= ?," +
                            " format=?," +
                            " number_support=?," +
                            " available=?" +
                            " WHERE product_id=?;";

            ps = conn.prepareStatement(sql);

            i = 1;
            ps.setLong(i++, homevideoProduct.getAudiovisionWork().getWorkId());
            ps.setString(i++, homevideoProduct.getName());
            ps.setString(i++, homevideoProduct.getPicture_url());
            ps.setDate(i++, new java.sql.Date(homevideoProduct.getReleaseDate().getTime()));
            ps.setDouble(i++, homevideoProduct.getPrice());
            ps.setString(i++, homevideoProduct.getFormat());
            ps.setInt(i++, homevideoProduct.getNumberSupport());
            ps.setInt(i++, homevideoProduct.getAvailable());
            ps.setLong(i++, homevideoProduct.getProductId());

            ps.executeUpdate();

            if (homevideoProduct.getLanguages() != null) {

                sql =
                        "DELETE FROM homevideo_language" +
                                " WHERE product_id = ?;";

                ps = conn.prepareStatement(sql);
                ps.setLong(1, homevideoProduct.getProductId());
                ps.executeUpdate();

                int k;
                StringBuilder sqlBuilder = new StringBuilder
                        ("INSERT INTO homevideo_language" +
                                " (product_id, language) " +
                                " VALUES (?,?)");
                for (k = 1; k < homevideoProduct.getLanguages().length; k++) {
                    sqlBuilder.append(", (?,?)");
                }
                sql = sqlBuilder.toString();
                ps = conn.prepareStatement(sql);
                i = 1;
                for (k = 0; k < homevideoProduct.getLanguages().length; k++) {
                    ps.setLong(k + i++, homevideoProduct.getProductId());
                    ps.setString(k + i, homevideoProduct.getLanguage(k));
                }
                ps.executeUpdate();
            }

            ps.close();
            rs.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(HomevideoProduct product) {

        PreparedStatement ps;

        try {
            String sql =
                    " UPDATE homevideo_product" +
                            " SET deleted=true," +
                            " available = 0" +
                            " WHERE product_id = ?;";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, product.getProductId());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<HomevideoProduct> findNMostRecent(int N) {

        PreparedStatement ps;
        List<HomevideoProduct> homevideoProducts = new ArrayList<>();
        HomevideoProduct homevideoProduct;

        try {
            String sql
                    = " SELECT *"
                    + " FROM homevideo_product"
                    + " WHERE"
                    + " available>0 AND"
                    + " deleted=false"
                    + " ORDER BY release_date DESC"
                    + " LIMIT ?;";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, N);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                homevideoProduct = read(resultSet);
                homevideoProducts.add(homevideoProduct);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return homevideoProducts;
    }

    @Override
    public List<HomevideoProduct> findNBestSeller(int N) {

        PreparedStatement ps;
        List<HomevideoProduct> homevideoProducts = new ArrayList<>();

        try {
            String sql
                    = "SELECT product_id,name,price,picture_url,SUM(item_quantity)"
                    + " FROM homevideo_product"
                    + " NATURAL JOIN product_in_cart"
                    + " NATURAL JOIN cart"
                    + " WHERE checkedout=true"
                    + " AND available > 0"
                    + " AND deleted = false"
                    + " GROUP BY product_id,name,price,picture_url"
                    + " ORDER BY SUM(item_quantity) DESC"
                    + " LIMIT ?;";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, N);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                homevideoProducts.add(read(resultSet));
            }

            resultSet.close();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return homevideoProducts;
    }

    @Override
    public List<HomevideoProduct> findByFilters(
            String[] typeFilter,
            String[] formatFilter,
            String[] genreFilter,
            String orderFilter,
            Boolean descendant,
            int thisPage, final int multipleView, String searchString) {

        PreparedStatement ps;
        List<HomevideoProduct> homevideoProducts = new ArrayList<>();

        int i;

        try {

            String sql
                    = " SELECT DISTINCT product_id,name,price,picture_url,price,release_date"
                    + " FROM homevideo_product"
                    + " WHERE deleted = false";

            StringBuilder sqlBuilder = new StringBuilder(sql);

            if (formatFilter != null) {
                sqlBuilder.append(" AND (format = ? ");
                for (i = 1; i < formatFilter.length; i++) {
                    sqlBuilder.append(" OR format = ? ");
                }
                sqlBuilder.append(")");
            }

            if (typeFilter != null) {
                sqlBuilder.append(" AND product_id IN (");
                sqlBuilder.append(" SELECT product_id FROM homevideo_product as hp");
                sqlBuilder.append(" NATURAL JOIN audiovision_work as aw");
                sqlBuilder.append(" WHERE (type = ?");
                for (i = 1; i < typeFilter.length; i++) {
                    sqlBuilder.append(" OR type = ?");
                }
                sqlBuilder.append(") AND hp.deleted = false AND aw.deleted = false )");
            }

            if (genreFilter != null) {
                sqlBuilder.append(" AND work_id IN (");
                sqlBuilder.append(" SELECT work_id FROM audiovision_work as aw");
                sqlBuilder.append(" NATURAL JOIN work_genre NATURAL JOIN genre as g");
                sqlBuilder.append(" WHERE (genre_name = ?");
                for (i = 1; i < genreFilter.length; i++) {
                    sqlBuilder.append(" OR genre_name = ?");
                }
                sqlBuilder.append(") AND aw.deleted = false AND g.deleted = false)");
            }

            if (searchString != null) {

                sqlBuilder.append(" AND (INSTR(name,?)>0");

                sqlBuilder.append(" OR work_id IN (");
                sqlBuilder.append(" SELECT work_id");
                sqlBuilder.append(" FROM work_actor NATURAL JOIN actor");
                sqlBuilder.append(" WHERE INSTR(actor_name,?)>0");
                sqlBuilder.append(" AND deleted = false)");

                sqlBuilder.append(" OR work_id IN (");
                sqlBuilder.append(" SELECT work_id");
                sqlBuilder.append(" FROM work_director NATURAL JOIN director");
                sqlBuilder.append(" WHERE INSTR(director_name,?)>0");
                sqlBuilder.append(" AND deleted = false)");

                sqlBuilder.append(")");

            }

            if (orderFilter != null) {
                if (orderFilter.equals("name")) {
                    sqlBuilder.append(" ORDER BY name");
                } else if (orderFilter.equals("price")) {
                    sqlBuilder.append(" ORDER BY price");
                } else if (orderFilter.equals("release_date")) {
                    sqlBuilder.append(" ORDER BY release_date");
                }
            } else {
                sqlBuilder.append(" ORDER BY name");
            }

            if (descendant) {
                sqlBuilder.append(" DESC");
            }
            sqlBuilder.append(";");
            sql = sqlBuilder.toString();

            int k = 1;
            ps = conn.prepareStatement(sql);

            if (formatFilter != null) {
                for (i = 0; i < formatFilter.length; i++) {
                    ps.setString(k++, formatFilter[i]);
                }
            }
            if (typeFilter != null) {
                for (i = 0; i < typeFilter.length; i++) {
                    ps.setString(k++, typeFilter[i]);
                }
            }
            if (genreFilter != null) {
                for (i = 0; i < genreFilter.length; i++) {
                    ps.setString(k++, genreFilter[i]);
                }
            }
            if (searchString != null) {
                ps.setString(k++, searchString);
                ps.setString(k++, searchString);
                ps.setString(k++, searchString);
            }

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                if (resultSet.getRow() > (thisPage - 1) * multipleView && resultSet.getRow() <= (thisPage * multipleView) + 1) {
                    homevideoProducts.add(read(resultSet));
                }
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return homevideoProducts;
    }

    @Override
    public List<HomevideoProduct> findByAudiovisionWork(AudiovisionWork audiovisionWork) {

        PreparedStatement ps;
        List<HomevideoProduct> homevideoProducts = new ArrayList<>();
        try {
            String sql
                    = " SELECT *"
                    + " FROM homevideo_product"
                    + " WHERE work_id = ?" +
                    "  AND deleted = false";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, audiovisionWork.getWorkId());

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                homevideoProducts.add(read(resultSet));
            }

            ps.close();
            resultSet.close();

            return homevideoProducts;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public HomevideoProduct findWithLanguagesbyId(Long productId) {

        PreparedStatement ps;
        HomevideoProduct homevideoProduct = null;

        try {

            String sql
                    = "SELECT * "
                    + " FROM homevideo_product AS hv"
                    + " LEFT OUTER JOIN homevideo_language AS hl "
                    + " ON hv.product_id = hl.product_id"
                    + " WHERE hv.product_id = ? " +
                    "  AND deleted = false";

            ps = conn.prepareStatement(sql);

            ps.setLong(1, productId);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                homevideoProduct = read(resultSet);
                List<String> languages = new ArrayList<>();
                resultSet.previous();
                while (resultSet.next()) {
                    String language = "";
                    try {
                        language = resultSet.getString("language");
                    } catch (SQLException e) {
                    }
                    languages.add(language);
                }
                String[] array = new String[languages.size()];
                array = languages.toArray(array);
                homevideoProduct.setLanguages(array);
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return homevideoProduct;
    }

    @Override
    public void updateProductsPurchase(List<ProductInCart> productInCarts) throws AvailabilityOutOfStockException {

        PreparedStatement ps;
        Long productId;
        int proudctQuantity;
        String sql;
        ResultSet rs;

        try {

            StringBuilder sqlBuilder = new StringBuilder(

                    "SELECT product_id,name,available" +
                            " FROM homevideo_product" +
                            " WHERE" +
                            "( product_id=?");

            for (int i = 1; i < productInCarts.size(); i++) {
                sqlBuilder.append(" OR product_id = ? ");
            }
            sqlBuilder.append(") FOR UPDATE");
            sql = sqlBuilder.toString();
            ps = conn.prepareStatement(sql);

            int k=1;
            for (int i = 0; i < productInCarts.size(); i++) {
                ps.setLong(k++, productInCarts.get(i).getHomevideoProducts().getProductId());
            }

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                HomevideoProduct homevideoProduct = read(resultSet);
                for(ProductInCart productInCart : productInCarts) {
                    if (productInCart.getHomevideoProducts().getProductId().equals(homevideoProduct.getProductId())
                     && homevideoProduct.getAvailable() < productInCart.getItemQuantity())
                        throw new AvailabilityOutOfStockException("Out of stock product:" + homevideoProduct.getName());
                }
            }


            for (ProductInCart productInCart : productInCarts) {

                productId = productInCart.getHomevideoProducts().getProductId();
                proudctQuantity = productInCart.getItemQuantity();

                sql
                        = " UPDATE homevideo_product SET"
                        + " available = available - ? "
                        + " WHERE product_id = ? ";

                ps = conn.prepareStatement(sql);

                ps.setInt(1, proudctQuantity);
                ps.setLong(2, productId);

                ps.executeUpdate();
            }




        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insertInWishlistByCustomer(Customer customer, HomevideoProduct homevideoProduct) {

        PreparedStatement ps;

        try {
            String sql = "SELECT product_id" +
                    " FROM wishlist" +
                    " WHERE product_id = ?" +
                    " AND customer_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, homevideoProduct.getProductId());
            ps.setLong(2, customer.getCustomerId());
            ResultSet resultSet = ps.executeQuery();

            boolean exists = resultSet.next();
            resultSet.close();

            if (!exists) {

                sql =
                        "INSERT INTO wishlist" +
                                " (customer_id, product_id)" +
                                " VALUES (?,?);";
                ps = conn.prepareStatement(sql);
                ps.setLong(1, customer.getCustomerId());
                ps.setLong(2, homevideoProduct.getProductId());
                ps.executeUpdate();

            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<HomevideoProduct> findWishlistbyCustomerId(Customer customer) {

        PreparedStatement ps;
        List<HomevideoProduct> homevideoProducts = new ArrayList<>();

        try {
            String sql =
                    " SELECT product_id,name,available,price,picture_url" +
                            " FROM homevideo_product" +
                            " WHERE product_id IN " +
                            "(  SELECT product_id" +
                            "   FROM wishlist" +
                            "   WHERE customer_id = ?)";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customer.getCustomerId());
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next())
                homevideoProducts.add(read(resultSet));

            ps.close();
            resultSet.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return homevideoProducts;
    }

    @Override
    public void removeFromWishlistByCustomer(Customer customer, HomevideoProduct homevideoProduct) {

        PreparedStatement ps;
        try {
            String sql =
                    " DELETE FROM wishlist" +
                            " WHERE customer_id = ?" +
                            " AND product_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, customer.getCustomerId());
            ps.setLong(2, homevideoProduct.getProductId());
            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public static HomevideoProduct read(ResultSet rs) {

        HomevideoProduct homevideoProduct = new HomevideoProduct();

        try {
            homevideoProduct.setProductId(rs.getLong("product_id"));
        } catch (SQLException e) {
        }

        try {
            homevideoProduct.setName(rs.getString("name"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setPrice(rs.getDouble("price"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setPicture_url(rs.getString("picture_url"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setFormat(rs.getString("format"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setNumberSupport(rs.getInt("number_support"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setReleaseDate(rs.getDate("release_date"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setAvailable(rs.getInt("available"));
        } catch (SQLException e) {
        }
        try {
            homevideoProduct.setDeleted(rs.getBoolean("deleted"));
        } catch (SQLException e) {
        }

        AudiovisionWork audiovisionWork = new AudiovisionWork();

        try {
            audiovisionWork.setWorkId(rs.getLong("work_id"));
        } catch (SQLException e) {
        }

        homevideoProduct.setAudiovisionWork(audiovisionWork);

        return homevideoProduct;

    }

}
