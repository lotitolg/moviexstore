package model.dao.mySQLJDBCImpl;

import model.dao.ActorDAO;
import model.dao.exception.DuplicatedObjectException;
import model.mo.Actor;
import model.mo.AudiovisionWork;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ActorDAOMySQLJDBCImpl implements ActorDAO {

    private final String COUNTER_ID= "actor_id";
    Connection conn;

    public ActorDAOMySQLJDBCImpl(Connection conn){
        this.conn=conn;
    }

    @Override
    public Actor insert(String actorName) throws DuplicatedObjectException {

        PreparedStatement ps;
        Actor actor = new Actor();
        try{

            String sql=
                    " SELECT *" +
                            " FROM actor" +
                            " WHERE actor_name = ?" +
                            " AND deleted = false;";

            ps=conn.prepareStatement(sql);
            ps.setString(1,actorName);

            ResultSet resultSet = ps.executeQuery();

            if(resultSet.next())
                throw new DuplicatedObjectException("ActorDAOMySQLJDBCImpl: Actor already exists");

            sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            resultSet.next();

            actor.setActorName(actorName);
            actor.setActorId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql=
                    " INSERT INTO actor" +
                            "(actor_id, actor_name,deleted) " +
                            "VALUES (?,?,false);" ;

            ps=conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++,actor.getActorId());
            ps.setString(i++,actor.getActorName());

            ps.executeUpdate();

        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return actor;
    }

    @Override
    public void delete(Actor actor) {

    }

    @Override
    public void update(Actor actor) {

    }

    @Override
    public List<Actor> findByAudiovisionWork(AudiovisionWork audiovisionWork) {

        PreparedStatement ps;
        List<Actor> actors = new ArrayList<>();

        try{
            String sql =
                    " SELECT *" +
                            " FROM actor" +
                            " NATURAL JOIN work_actor" +
                            " WHERE work_id = ? " +
                            " AND deleted = false;";

            ps=conn.prepareStatement(sql);
            ps.setLong(1,audiovisionWork.getWorkId());

            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                actors.add(read(resultSet));
            }

            ps.close();
            resultSet.close();

            return actors;

        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Actor> extractActors() {

        PreparedStatement ps;
        List<Actor> actors = new ArrayList<>();

        try{

            String sql
                    ="SELECT *" +
                    " FROM actor " +
                    " WHERE deleted=false" +
                    " ORDER BY actor_name;";

            ps=conn.prepareStatement(sql);

            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                actors.add(read(resultSet));
            }
            resultSet.close();
            ps.close();


        }catch(SQLException e){
            throw new RuntimeException(e);
        }

        return actors;
    }

    @Override
    public List<Actor> findBySearchString(String searchString) {

        PreparedStatement ps;
        List<Actor> actors = new ArrayList<>();

        try{
            String sql="SELECT actor_name" +
                    " FROM actor" +
                    " WHERE INSTR(actor_name,?)>0" +
                    " AND deleted=false" +
                    " ORDER BY actor_name;";

            ps=conn.prepareStatement(sql);
            ps.setString(1,searchString);

            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next())
                actors.add(read(resultSet));

            ps.close();
            resultSet.close();

        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return  actors;
    }



    Actor read(ResultSet rs){

        Actor actor = new Actor();

        try{
            actor.setActorId(rs.getLong("actor_id"));
        }catch (SQLException e) {}
        try{
            actor.setActorName(rs.getString("actor_name"));
        }catch (SQLException e) {}

        return actor;
    }
}
