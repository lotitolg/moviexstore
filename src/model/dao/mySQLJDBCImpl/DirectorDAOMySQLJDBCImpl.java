package model.dao.mySQLJDBCImpl;

import model.dao.DirectorDAO;
import model.dao.exception.DuplicatedObjectException;
import model.mo.AudiovisionWork;
import model.mo.Director;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DirectorDAOMySQLJDBCImpl implements DirectorDAO {

    private final String COUNTER_ID= "director_id";
    Connection conn;

    public DirectorDAOMySQLJDBCImpl(Connection conn){
        this.conn=conn;
    }

    @Override
    public Director insert(String directorName) throws DuplicatedObjectException {

        PreparedStatement ps;
        Director director = new Director();
        try{

            String sql=
                    " SELECT *" +
                            " FROM director" +
                            " WHERE director_name = ?" +
                            " AND deleted = false;";

            ps=conn.prepareStatement(sql);
            ps.setString(1,directorName);

            ResultSet resultSet = ps.executeQuery();

            if(resultSet.next())
                throw new DuplicatedObjectException("GenreDAOMySQLJDBCImpl: Genre already exists");

            sql = "update counter set counter_value=counter_value+1 where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            sql = "SELECT counter_value FROM counter where counter_id='" + COUNTER_ID + "'";

            ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            resultSet.next();

            director.setDirectorName(directorName);
            director.setDirectorId(resultSet.getLong("counter_value"));
            resultSet.close();

            sql=
                    " INSERT INTO director" +
                            "(director_id, director_name,deleted) " +
                            "VALUES (?,?,false)" ;

            ps=conn.prepareStatement(sql);

            int i = 1;
            ps.setLong(i++,director.getDirectorId());
            ps.setString(i++,director.getDirectorName());

            ps.executeUpdate();

        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return director;
    }

    @Override
    public void delete(Director director) {

    }

    @Override
    public void update(Director director) {

    }

    @Override
    public List<Director> extractDirectors() {

        PreparedStatement ps;
        List<Director> directors = new ArrayList<>();

        try{

            String sql
                    ="SELECT * "
                    +"FROM director " +
                    " WHERE deleted=false" +
                    " ORDER BY director_name";

            ps=conn.prepareStatement(sql);

            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                directors.add(read(resultSet));
            }
            resultSet.close();
            ps.close();


        }catch(SQLException e){
            throw new RuntimeException(e);
        }

        return directors;
    }

    @Override
    public List<Director> findByAudiovisionWork(AudiovisionWork audiovisionWork) {

        PreparedStatement ps;
        List<Director> directors = new ArrayList<>();

        try{
            String sql =
                    " SELECT *" +
                    " FROM director" +
                    " NATURAL JOIN work_director" +
                    " WHERE work_id = ?" +
                            " AND deleted = false;";

            ps=conn.prepareStatement(sql);
            ps.setLong(1,audiovisionWork.getWorkId());

            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                directors.add(read(resultSet));
            }

            ps.close();
            resultSet.close();

            return directors;

        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Director> findBySearchString(String searchString) {

        PreparedStatement ps;
        List<Director> directors = new ArrayList<>();

        try{
            String sql="SELECT director_name" +
                    " FROM director" +
                    " WHERE INSTR(director_name,?)>0" +
                    " AND deleted=false" +
                    " ORDER BY director_name";

            ps=conn.prepareStatement(sql);
            ps.setString(1,searchString);

            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next())
                directors.add(read(resultSet));

            ps.close();
            resultSet.close();

        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return  directors;
    }


    Director read(ResultSet rs){

        Director director = new Director();

        try{
            director.setDirectorId(rs.getLong("director_id"));
        }catch (SQLException e) {}
        try{
            director.setDirectorName(rs.getString("director_name"));
        }catch (SQLException e) {}

        return director;
    }
}

