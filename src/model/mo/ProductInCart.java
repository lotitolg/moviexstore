package model.mo;


public class ProductInCart {

    private Cart cart;
    private HomevideoProduct homevideoProducts;

    private int itemQuantity;


    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }


    public HomevideoProduct getHomevideoProducts() {
        return homevideoProducts;
    }

    public void setHomevideoProducts(HomevideoProduct homevideoProducts) {
        this.homevideoProducts = homevideoProducts;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

}
