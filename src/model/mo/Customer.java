package model.mo;

public class Customer {

    private long customerId;
    private String firstname;
    private String surname;
    private String username;
    private String password;
    private String email;
    private boolean banned;
    private boolean deleted;

    private Cart[] cart;
    private BillingInfo[] billingInfos;
    private Order[] orders;
    private DeliveryAddress[] deliveryAddresses;

    /*Prodotti nella wishlist*/
    private HomevideoProduct[] wishlist;

    /*Prodotti nello storico*/
    private HistoryProduct[] historyProducts;



    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

// Relazioni
    //Cart
    public Cart[] getCart() {
        return cart;
    }

    public void setCart(Cart[] cart) {
        this.cart = cart;
    }

    public Cart getCart(int index) {
        return this.cart[index];
    }

    public void setCart(int index, Cart cart) {
        this.cart[index] = cart;
    }

    //orders
    public Order[] getOrders() {
        return orders;
    }

    public void setOrders(Order[] orders) {
        this.orders = orders;
    }

    public Order getOrder(int index) {
        return this.orders[index];
    }

    public void setOrder(int index, Order order) {
        this.orders[index] = order;
    }
    
    //deliveryAddresses
    public DeliveryAddress[] getDeliveryAddresses() {
        return deliveryAddresses;
    }

    public void setDeliveryAddresses(DeliveryAddress[] deliveryAddresses) {
        this.deliveryAddresses = deliveryAddresses;
    }

    public DeliveryAddress getDeliveryAddress(int index) {
        return this.deliveryAddresses[index];
    }

    public void setDeliveryAddress(int index, DeliveryAddress delivery_address) {
        this.deliveryAddresses[index] = delivery_address;
    }
    
    //BillingInfo
    public BillingInfo[] getBillingInfos() {
        return billingInfos;
    }

    public void setBillingInfos(BillingInfo[] billingInfos) {
        this.billingInfos = billingInfos;
    }

    public BillingInfo getBillingInfo(int index) {
        return this.billingInfos[index];
    }

    public void setBillingInfo(int index, BillingInfo billing_info) {
        this.billingInfos[index] = billing_info;
    }

    /*Wishlist*/

    public HomevideoProduct[] getWishlist() {
        return wishlist;
    }

    public void setWishlist(HomevideoProduct[] wishlist) {
        this.wishlist = wishlist;
    }

    public HomevideoProduct getHomevideoProduct(int index) {
        return wishlist[index];
    }

    public void setHomevideoProduct(int index,HomevideoProduct homevideoProduct) {
        this.wishlist[index] = homevideoProduct;
    }

    public HistoryProduct[] getHistoryProducts() {
        return historyProducts;
    }

    public void setHistoryProducts(HistoryProduct[] historyProducts) {
        this.historyProducts = historyProducts;
    }

    public HistoryProduct getHistoryProduct(int index) {
        return historyProducts[index];
    }

    public void setHistoryProduct (int index, HistoryProduct historyProduct){
        this.historyProducts[index]=historyProduct;
    }

}
