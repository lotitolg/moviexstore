package model.mo;

public class Coupon {

    private Long couponId;
    private int discountPercent;
    private String code;
    private boolean disabled;

    private Order[] orders;

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public int getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(int discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }



    public Order[] getOrders() {
        return orders;
    }

    public void setOrders(Order[] orders) {
        this.orders = orders;
    }

    public Order getOrder(int index) {
        return this.orders[index];
    }

    public void setOrder(int index, Order order) {
        this.orders[index] = order;
    }

}
