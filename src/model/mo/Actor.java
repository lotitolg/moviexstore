package model.mo;

public class Actor{

    private Long actorId;
    private String actorName;

    private AudiovisionWork[] audiovisionWorks;


    public Long getActorId() {

        return actorId;
    }

    public void setActorId(Long actorId) {

        this.actorId = actorId;
    }

    public String getActorName() {

        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public AudiovisionWork[] getAudiovisionWorks() {
        return audiovisionWorks;
    }

    public void setAudiovisionWorks(AudiovisionWork[] audiovisionWorks) {
        this.audiovisionWorks = audiovisionWorks;
    }

    public AudiovisionWork getAudiovisionWork(int index) {
        return this.audiovisionWorks[index];
    }

    public void setAudiovisionWork(int index, AudiovisionWork audiovisionWork) {
        this.audiovisionWorks[index] = audiovisionWork;
    }

}
