package model.mo;

public class Genre {

    private Long genreId;
    private String genreName;

    private AudiovisionWork[] audiovisionWorks;

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public AudiovisionWork[] getAudiovisionWorks() {
        return audiovisionWorks;
    }

    public void setAudiovisionWorks(AudiovisionWork[] audiovisionWorks) {
        this.audiovisionWorks = audiovisionWorks;
    }

    public AudiovisionWork getAudiovisionWork(int index) {
        return this.audiovisionWorks[index];
    }

    public void setAudiovisionWork(int index, AudiovisionWork audiovisionWork) {
        this.audiovisionWorks[index] = audiovisionWork;
    }
}
