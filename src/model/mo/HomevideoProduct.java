package model.mo;

import java.util.Date;

public class HomevideoProduct {

    private Long productId;
    private String name;
    private Double price;
    private String picture_url;
    private String format;
    private int numberSupport;
    private int available;
    private boolean deleted;
    private Date releaseDate;

    private String[] languages;
    private AudiovisionWork audiovisionWork;

    private ProductInCart[] productInCarts;




    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getNumberSupport() {
        return numberSupport;
    }

    public void setNumberSupport(int numberSupport) {
        this.numberSupport = numberSupport;
    }



    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }



    public ProductInCart[] getProductInCarts() {
        return productInCarts;
    }

    public void setProductInCarts(ProductInCart[] productInCarts) {
        this.productInCarts = productInCarts;
    }

    public ProductInCart getProductInCart(int index) {
        return this.productInCarts[index];
    }

    public void setProductInCart(int index, ProductInCart productInCart) {
        this.productInCarts[index] = productInCart;
    }

    //languages
    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public String getLanguage(int index){
        return languages[index];
    }

    public void setLanguage(int index,String language){
        this.languages[index]=language;
    }

    public AudiovisionWork getAudiovisionWork() {
        return audiovisionWork;
    }

    public void setAudiovisionWork(AudiovisionWork audiovisionWork) {
        this.audiovisionWork = audiovisionWork;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }
}
