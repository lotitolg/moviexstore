package model.mo;


import java.util.Date;

public class Cart {

    private Long cartId;
    private Date dateCreated;
    private String checkedOut;

    /*Attributi derivati*/
    private Double totalPrice;
    private Integer numberOfItem;

    private Order order;
    private Customer customer;
    private ProductInCart[] productInCart;


    public ProductInCart getProductInCart(int index) {
        return this.productInCart[index];
    }

    public void setProductInCart(int index, ProductInCart productInCart) {
        this.productInCart[index] = productInCart;
    }


    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(String checkedOut) {
        this.checkedOut = checkedOut;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getNumberOfItem() {
        return numberOfItem;
    }

    public void setNumberOfItem(Integer numberOfItem) {
        this.numberOfItem = numberOfItem;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ProductInCart[] getProductInCart() {
        return productInCart;
    }

    public void setProductInCart(ProductInCart[] productInCart) {
        this.productInCart = productInCart;
    }
}
