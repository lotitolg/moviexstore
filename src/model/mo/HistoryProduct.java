package model.mo;

import java.util.GregorianCalendar;

public class HistoryProduct {

    private Customer customer;
    private HomevideoProduct homevideoProduct;
    private GregorianCalendar dateAdded;


    public HomevideoProduct getHomevideoProduct() {
        return homevideoProduct;
    }

    public void setHomevideoProduct(HomevideoProduct homevideoProduct) {
        this.homevideoProduct = homevideoProduct;
    }

    public GregorianCalendar getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(GregorianCalendar dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
