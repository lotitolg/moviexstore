package model.mo;

import java.sql.Date;
import java.util.GregorianCalendar;

public class BillingInfo {

    private Long billingId;
    private String cardNum;
    private Date cardexpdate;
    private String networkCard;
    private boolean deleted;
    private boolean active;
    private boolean used;

    private Order[] orders;
    private Customer customer;



    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public Date getCardexpdate() {
        return cardexpdate;
    }

    public void setCardexpdate(Date cardexpdate) {
        this.cardexpdate = cardexpdate;
    }

    public String getNetworkCard() {
        return networkCard;
    }

    public void setNetworkCard(String networkCard) {
        this.networkCard = networkCard;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Order[] getOrders() {
        return orders;
    }

    public void setOrders(Order[] orders) {
        this.orders = orders;
    }

    public Order getOrder(int index) {
        return this.orders[index];
    }

    public void setOrder(int index, Order order) {
        this.orders[index] = order;
    }

    public Long getBillingId() {
        return billingId;
    }

    public void setBillingId(Long billingId) {
        this.billingId = billingId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
