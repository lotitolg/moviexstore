package model.mo;

public class AudiovisionWork {

    private Long workId;
    private String originalTitle;
    private int duration;
    private float rating;
    private String type;
    private String yearProdction;
    private String plot;
    private String trailerUrl;
    private String studioProduction;
    private String imageWork;

    private Genre[] genres;
    private Director[] directors;
    private Actor[] actors;

    private HomevideoProduct[] homevideoProducts;

    public Long getWorkId() {
        return workId;
    }

    public void setWorkId(Long workId) {
        this.workId = workId;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYearProdction() {
        return yearProdction;
    }

    public void setYearProdction(String yearProdction) {
        this.yearProdction = yearProdction;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public String getStudioProduction() {
        return studioProduction;
    }

    public void setStudioProduction(String studioProduction) {
        this.studioProduction = studioProduction;
    }

    //Actor
    public Actor[] getActors() {
        return actors;
    }

    public void setActors(Actor[] actors) {
        this.actors = actors;
    }

    public Actor getActor(int index) {
        return this.actors[index];
    }

    public void setActor(int index, Actor actor) {
        this.actors[index] = actor;
    }

    //directors
    public Director[] getDirectors() {
        return directors;
    }

    public void setDirectors(Director[] directors) {
        this.directors = directors;
    }

    public Director getDirector(int index) {
        return this.directors[index];
    }

    public void setDirector(int index, Director director) {
        this.directors[index] = director;
    }

    //genres
    public Genre[] getGenres() {
        return genres;
    }

    public void setGenres(Genre[] genres) {
        this.genres = genres;
    }

    public Genre getGenre(int index) {
        return this.genres[index];
    }

    public void setGenre(int index, Genre genre) {
        this.genres[index] = genre;
    }

    public HomevideoProduct[] getHomevideoProducts() {
        return homevideoProducts;
    }

    public void setHomevideoProducts(HomevideoProduct[] homevideoProducts) {
        this.homevideoProducts = homevideoProducts;
    }

    public HomevideoProduct getHomevideoProduct(int index) {
        return this.homevideoProducts[index];
    }

    public void setHomevideoProduct(int index, HomevideoProduct homevideoProduct) {
        this.homevideoProducts[index] = homevideoProduct;
    }

    public String getImageWork() {
        return imageWork;
    }

    public void setImageWork(String imageWork) {
        this.imageWork = imageWork;
    }
}
