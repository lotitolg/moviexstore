package model.mo;

public class Director {

    private Long directorId;
    private String directorName;

    private AudiovisionWork[] audiovisionWorks;

    public Long getDirectorId() {
        return directorId;
    }

    public void setDirectorId(Long directorId) {
        this.directorId = directorId;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public AudiovisionWork[] getAudiovisionWorks() {
        return audiovisionWorks;
    }

    public void setAudiovisionWorks(AudiovisionWork[] audiovisionWorks) {
        this.audiovisionWorks = audiovisionWorks;
    }

    public AudiovisionWork getAudiovisionWork(int index) {
        return this.audiovisionWorks[index];
    }

    public void setAudiovisionWork(int index, AudiovisionWork audiovisionWork) {
        this.audiovisionWorks[index] = audiovisionWork;
    }

}
