package model.session.mo;

import model.mo.Customer;

public class LoggedUser {
  
  private long userId;
  private String firstname;
  private String surname;
  private Long cartId;
  private int itemsNumberInCart;

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public Long getCartId() {
    return cartId;
  }

  public void setCartId(Long cartId) {
    this.cartId = cartId;
  }

  public int getItemsNumberInCart() {
    return itemsNumberInCart;
  }

  public void setItemsNumberInCart(int itemsNumberInCart) {
    this.itemsNumberInCart = itemsNumberInCart;
  }
}
