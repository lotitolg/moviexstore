package model.session.dao;

import model.session.mo.LoggedAdmin;


public interface LoggedAdminDAO {

    public LoggedAdmin create(
            Long adminId,
            String firstname,
            String surname);

    public void update(LoggedAdmin loggedAdmin);

    public void destroy();

    public LoggedAdmin find();
}
