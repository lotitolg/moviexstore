package model.session.dao;

import model.mo.Customer;
import model.session.mo.LoggedUser;

public interface LoggedUserDAO {

  public LoggedUser create(
          long userId,
          String firstname,
          String surname,
          Long cartId,
          int itemsNumberInCart);

  public void update(LoggedUser loggedUser);

  public void destroy();

  public LoggedUser find();
  
}
