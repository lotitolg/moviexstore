package model.session.dao.CookieImpl;


import model.session.dao.LoggedAdminDAO;
import model.session.mo.LoggedAdmin;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggedAdminDAOCookieImpl implements LoggedAdminDAO {

    HttpServletRequest request;
    HttpServletResponse response;

    public LoggedAdminDAOCookieImpl(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public LoggedAdmin create(
            Long userId,
            String firstname,
            String surname) {

        LoggedAdmin loggedAdmin = new LoggedAdmin();
        loggedAdmin.setAdminId(userId);
        loggedAdmin.setFirstname(firstname);
        loggedAdmin.setSurname(surname);


        Cookie cookie;
        cookie = new Cookie("loggedAdmin", encode(loggedAdmin));
        cookie.setPath("/");
        response.addCookie(cookie);

        return loggedAdmin;

    }

    @Override
    public void update(LoggedAdmin loggedAdmin) {

        Cookie cookie;
        cookie = new Cookie("loggedAdmin", encode(loggedAdmin));
        cookie.setPath("/");
        response.addCookie(cookie);

    }

    @Override
    public void destroy() {

        Cookie cookie;
        cookie = new Cookie("loggedAdmin", "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);

    }

    @Override
    public LoggedAdmin find() {

        Cookie[] cookies = request.getCookies();
        LoggedAdmin loggedAdmin = null;

        if (cookies != null) {
            for (int i = 0; i < cookies.length && loggedAdmin == null; i++) {
                if (cookies[i].getName().equals("loggedAdmin")) {
                    loggedAdmin = decode(cookies[i].getValue());
                }
            }
        }

        return loggedAdmin;

    }

    private String encode(LoggedAdmin loggedAdmin) {

        String encodedLoggedUser;
        encodedLoggedUser = loggedAdmin.getAdminId() +
                "#" + loggedAdmin.getFirstname() +
                "#" + loggedAdmin.getSurname();

        return encodedLoggedUser;

    }

    private LoggedAdmin decode(String encodedLoggedUser) {

        LoggedAdmin loggedUser = new LoggedAdmin();

        String[] values = encodedLoggedUser.split("#");

        loggedUser.setAdminId(Long.parseLong(values[0]));
        loggedUser.setFirstname(values[1]);
        loggedUser.setSurname(values[2]);

        return loggedUser;

    }

}
