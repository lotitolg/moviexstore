package controller;

import model.dao.*;
import model.mo.BillingInfo;
import model.mo.Customer;
import model.mo.DeliveryAddress;
import model.mo.Order;
import model.session.dao.LoggedUserDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedUser;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InfoUserManagement {

    private InfoUserManagement() {
    }

    public static void profileUser(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            /*_________DAO________*/
            CustomerDAO customerDAO = daoFactory.getCustomerDAO();


            /*_________MO__________*/
            Customer customer = customerDAO.findCustomerbyId(loggedUser.getUserId());


            daoFactory.commitTransaction();

            customer.setPassword(null);     //per non passare la password al cliente
            request.setAttribute("customer", customer);

            request.setAttribute("object", "Customer");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyCustomer(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            /*_________DAO________*/
            CustomerDAO customerDAO = daoFactory.getCustomerDAO();

            /*_________MO__________*/
            Customer customer = customerDAO.findCustomerbyId(loggedUser.getUserId());
            customer.setFirstname(request.getParameter("firstName"));
            customer.setSurname(request.getParameter("surname"));
            customer.setEmail(request.getParameter("email"));
            customer.setUsername(request.getParameter("username"));
            customer = customerDAO.updateCustomerInfo(customer);

            loggedUser.setFirstname(customer.getFirstname());
            loggedUser.setSurname(customer.getSurname());
            loggedUserDAO.update(loggedUser);

            daoFactory.commitTransaction();

            request.setAttribute("object", "Customer");
            request.setAttribute("customer", customer);
            request.setAttribute("completedOperationMessage", "Update succeful");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void changePasswordview(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/changePassword");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void changePassword(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            /*_________DAO________*/
            CustomerDAO customerDAO = daoFactory.getCustomerDAO();

            /*_________MO__________*/
            Customer customer = customerDAO.findCustomerbyId(loggedUser.getUserId());
            if (request.getParameter("password") != null) {
                customer.setPassword(request.getParameter("password"));
                customerDAO.changePasword(customer, request.getParameter("password"));
            }
            daoFactory.commitTransaction();

            customer.setPassword(null);     //per non passare la password al cliente
            request.setAttribute("customer", customer);

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/changePassword");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void deliveryAddressView(HttpServletRequest request, HttpServletResponse response) {
        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            /*_________DAO________*/
            DeliveryAddressDAO deliveryAddressDAO = daoFactory.getDeliveryAddressDAO();

            /*_________MO__________*/
            DeliveryAddress deliveryAddress = deliveryAddressDAO.getActiveAddressbyCustomer(loggedUser.getUserId());

            daoFactory.commitTransaction();

            request.setAttribute("deliveryAddress", deliveryAddress);
            request.setAttribute("object", "Address");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyAddress(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;
        DeliveryAddress deliveryAddress;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            String addressId = request.getParameter("AddressId");

            DeliveryAddressDAO deliveryAddressDAO = daoFactory.getDeliveryAddressDAO();
            DeliveryAddress oldDeliveryAddress = deliveryAddressDAO.getActiveAddressbyCustomer(loggedUser.getUserId());

            if (oldDeliveryAddress.isUsed()) {

                deliveryAddressDAO.setInactive(oldDeliveryAddress);

                Customer customer = new Customer();
                customer.setCustomerId(loggedUser.getUserId());

                deliveryAddress = deliveryAddressDAO.insert(customer,
                        request.getParameter("country"),
                        request.getParameter("city"),
                        request.getParameter("address"),
                        request.getParameter("postalcode"));
            } else {

                deliveryAddress = new DeliveryAddress();
                deliveryAddress.setAddressId(Long.parseLong(addressId));
                deliveryAddress.setAddress(request.getParameter("address"));
                deliveryAddress.setCity(request.getParameter("city"));
                deliveryAddress.setCountry(request.getParameter("country"));
                deliveryAddress.setPostalcode(request.getParameter("postalcode"));
                deliveryAddress = deliveryAddressDAO.update(deliveryAddress);
            }

            daoFactory.commitTransaction();

            request.setAttribute("deliveryAddress", deliveryAddress);
            request.setAttribute("object", "Address");
            request.setAttribute("completedOperationMessage", "Update successful");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void insertAddress(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();


            DeliveryAddressDAO deliveryAddressDAO = daoFactory.getDeliveryAddressDAO();

            Customer customer = new Customer();
            customer.setCustomerId(loggedUser.getUserId());
            DeliveryAddress deliveryAddress = deliveryAddressDAO.insert(customer,
                    request.getParameter("country"),
                    request.getParameter("city"),
                    request.getParameter("address"),
                    request.getParameter("postalcode"));

            daoFactory.commitTransaction();

            request.setAttribute("deliveryAddress", deliveryAddress);
            request.setAttribute("object", "Address");
            request.setAttribute("completedOperationMessage", "Update successful");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void changeBillingInfoView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            BillingInfoDAO billingInfoDAO = daoFactory.getBillingInfoDAO();
            BillingInfo billingInfo = billingInfoDAO.findActiveBillingInfobyCustomerId(loggedUser.getUserId());

            if (billingInfo != null)
                billingInfo.setCardNum(billingInfo.getCardNum().substring(billingInfo.getCardNum().length() - 4));

            daoFactory.commitTransaction();

            request.setAttribute("billingInfo", billingInfo);
            request.setAttribute("object", "Billing");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void insertBilling(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            BillingInfoDAO billingInfoDAO = daoFactory.getBillingInfoDAO();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInserted = request.getParameter("cardexpdate");
            java.util.Date date = formatter.parse(dateInserted);

            if(date.before(new Date())){
                applicationMessage = "The card is expired!";
                request.setAttribute("billingInfo", null);
                request.setAttribute("applicationMessage", applicationMessage);
            }
            else {

                java.sql.Date sDate = new java.sql.Date(date.getTime());

                Customer customer = new Customer();
                customer.setCustomerId(loggedUser.getUserId());

                BillingInfo billingInfo = billingInfoDAO.insert(customer,
                        request.getParameter("card_num"),
                        sDate,request.getParameter("cardNetwork"));

                billingInfo.setCardNum(billingInfo.getCardNum().substring(billingInfo.getCardNum().length() - 4));
                request.setAttribute("billingInfo", billingInfo);
                request.setAttribute("completedOperationMessage", "Update successful");
            }

            daoFactory.commitTransaction();

            request.setAttribute("object", "Billing");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyBilling(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        String applicationMessage=null;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        BillingInfo newBillingInfo;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            BillingInfoDAO billingInfoDAO = daoFactory.getBillingInfoDAO();
            BillingInfo oldBilling = billingInfoDAO.findActiveBillingInfobyCustomerId(loggedUser.getUserId());

            String billingId = request.getParameter("BillingId");
            String card_num = request.getParameter("card_num");
            String card_network = request.getParameter("cardNetwork");

            /* gettin card exp date */
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInserted = request.getParameter("cardexpdate");
            java.util.Date date = formatter.parse(dateInserted);

            if(date.before(new Date())){
                applicationMessage = "The card is expired!";
                request.setAttribute("applicationMessage", applicationMessage);
                oldBilling.setCardNum(oldBilling.getCardNum().substring(oldBilling.getCardNum().length() - 4));
                request.setAttribute("billingInfo", oldBilling);
            }
            else {

                java.sql.Date sDate = new java.sql.Date(date.getTime());

                if(oldBilling.isUsed()){

                    billingInfoDAO.setInactive(oldBilling);

                    Customer customer = new Customer();
                    customer.setCustomerId(loggedUser.getUserId());

                    newBillingInfo = billingInfoDAO.insert(customer,card_num,sDate,card_network);

                }else{

                    newBillingInfo = new BillingInfo();
                    newBillingInfo.setBillingId(Long.parseLong(billingId));
                    newBillingInfo.setCardexpdate(sDate);
                    newBillingInfo.setCardNum(card_num);
                    newBillingInfo.setNetworkCard(card_network);
                    billingInfoDAO.update(loggedUser.getUserId(), newBillingInfo);
                }

                newBillingInfo.setCardNum(newBillingInfo.getCardNum().substring(newBillingInfo.getCardNum().length() - 4));
                request.setAttribute("billingInfo", newBillingInfo);
                request.setAttribute("completedOperationMessage", "Update successful");
            }

            daoFactory.commitTransaction();

            request.setAttribute("object", "Billing");
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/profileUser");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void ordersInfoView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;

        List<Order> orderList;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            OrderDAO orderDAO = daoFactory.getOrderDAO();
            orderList = orderDAO.ListOfOrdersForCustomer(loggedUser.getUserId());// lista delle info di tutti gli ordini

            daoFactory.commitTransaction();

            request.setAttribute("orderList", orderList);
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "infoUserManagement/ordersView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }

    }

}

