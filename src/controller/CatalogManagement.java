package controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.dao.*;
import model.mo.*;
import model.session.dao.LoggedUserDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedUser;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CatalogManagement {

    private CatalogManagement(){
    }

    public static void catalogView(HttpServletRequest request, HttpServletResponse response){

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        List<Genre> genres;
        List<HomevideoProduct> homevideoProducts = new ArrayList<>();
        List<AudiovisionWork> audiovisionWorks = new ArrayList<>();
        DAOFactory daoFactory = null;
        boolean descendant;
        int thispage;
        Boolean isThisLastPage = Boolean.FALSE;
        int multipleView;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            String entityFilter= request.getParameter("entityFilter");
            String[] typeFilter = request.getParameterValues("typeFilter");
            String[] formatFilter = request.getParameterValues("formatFilter");
            String[] genreFilter = request.getParameterValues("genreFilter");
            String orderFilter =  request.getParameter("orderFilter");
            String descendantString = request.getParameter("descendant");
            String thisPageString = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");

            /*Filter default*/
            if(entityFilter==null)
                entityFilter="Homevideo";

            if(orderFilter == null){
                if(entityFilter.equals("Homevideo"))
                    orderFilter = "name";
                else orderFilter = "original_title";
            }

            if(descendantString!=null) {
                descendant=descendantString.equals("true");
            }else descendant=false;

            if(thisPageString==null) {
                thispage = 1;
            }else thispage=Integer.parseInt(thisPageString);

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AdminConfigurationDAO adminConfigurationDAO = daoFactory.getAdminConfigurationDAO();
            multipleView = Integer.parseInt(adminConfigurationDAO.read(Configuration.CUSTOMER_MULTIPLE_VIEW_CONFIG_NAME));

            /*Query generi*/
            GenreDAO genreDAO = daoFactory.getGenreDAO();
            genres=genreDAO.extractGenres();

            if(entityFilter.equals("Homevideo")){
                HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
                homevideoProducts=homevideoProductDAO.findByFilters(typeFilter,formatFilter,genreFilter,orderFilter,descendant,thispage,multipleView,searchString);
                if (homevideoProducts.size()<multipleView+1)
                    isThisLastPage=true;
                else homevideoProducts.remove(homevideoProducts.size()-1);
            }
            else {
                AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();
                audiovisionWorks = audiovisionWorkDAO.findByFilters(typeFilter,formatFilter,genreFilter,orderFilter,descendant,thispage,multipleView,searchString);
                if (audiovisionWorks.size()<multipleView+1)
                    isThisLastPage=true;
                else audiovisionWorks.remove(audiovisionWorks.size()-1);
            }

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);

            request.setAttribute("genres",genres);

            request.setAttribute("entityFilter",entityFilter);
            request.setAttribute("checkedList",getCheckedList(typeFilter,formatFilter,genreFilter));
            request.setAttribute("orderFilter",orderFilter);
            request.setAttribute("descendant",descendant);
            request.setAttribute("searchString",searchString);
            request.setAttribute("thisPage",thispage);
            request.setAttribute("isThisLastPage",isThisLastPage);
            request.setAttribute("multipleView",multipleView);

            request.setAttribute("works",audiovisionWorks);
            request.setAttribute("products",homevideoProducts);

            request.setAttribute("viewUrl", "catalogManagement/catalogView");

            
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }
        }



    }

    public static void workView(HttpServletRequest request, HttpServletResponse response){

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;

        AudiovisionWork audiovisionWork = null;
        List<Actor> actors = new ArrayList<>();
        List<Director> directors = new ArrayList<>();
        List<HomevideoProduct> homevideoProducts = new ArrayList<>();

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String workId = request.getParameter("workId");

            String entityFilter= request.getParameter("entityFilter");
            String[] typeFilter = request.getParameterValues("typeFilter");
            String[] formatFilter = request.getParameterValues("formatFilter");
            String[] genreFilter = request.getParameterValues("genreFilter");
            String orderFilter =  request.getParameter("orderFilter");
            String descendant = request.getParameter("descendant");
            String thisPage = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");

            if(workId!=null) {

                AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();
                ActorDAO actorDAO = daoFactory.getActorDAO();
                DirectorDAO directorDAO = daoFactory.getDirectorDAO();
                HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();

                 audiovisionWork = audiovisionWorkDAO.findWithGenresbyId(Long.parseLong(workId));
                 actors = actorDAO.findByAudiovisionWork(audiovisionWork);
                 directors = directorDAO.findByAudiovisionWork(audiovisionWork);
                 homevideoProducts = homevideoProductDAO.findByAudiovisionWork(audiovisionWork);


                 Actor[] actors1 = new Actor[actors.size()];
                 Director[] directors1 = new Director[directors.size()];
                 HomevideoProduct[] homevideoProducts1 = new HomevideoProduct[homevideoProducts.size()];

                 homevideoProducts1=homevideoProducts.toArray(homevideoProducts1);
                 actors1=actors.toArray(actors1);
                 directors1 = directors.toArray(directors1);

                 audiovisionWork.setActors(actors1);
                 audiovisionWork.setDirectors(directors1);
                 audiovisionWork.setHomevideoProducts(homevideoProducts1);

            }

            daoFactory.commitTransaction();

            request.setAttribute("audiovisionWork",audiovisionWork);

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "catalogManagement/workView");

            request.setAttribute("entityFilter",entityFilter);
            request.setAttribute("typeFilter",typeFilter);
            request.setAttribute("formatFilter",formatFilter);
            request.setAttribute("genreFilter",genreFilter);
            request.setAttribute("orderFilter",orderFilter);
            request.setAttribute("descendant",descendant);
            request.setAttribute("searchString",searchString);
            request.setAttribute("thisPage",thisPage);




        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }
        }
    }

    public static void productView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            assert sessionDAOFactory != null;
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            assert daoFactory != null;
            daoFactory.beginTransaction();

            String productId = request.getParameter("productId");

            commonProductView(request,daoFactory,productId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "catalogManagement/productView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }
        }

    }

    public static void addToCart(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        HomevideoProduct homevideoProduct = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String productId = request.getParameter("productId");

            if (loggedUser == null) {

                applicationMessage = "LOG IN or SIGN IN to ADD TO CART";
                request.setAttribute("applicationMessage", applicationMessage);

            } else {
                String quantity = request.getParameter("quantity");

                CartDAO cartDAO = daoFactory.getCartDAO();
                cartDAO.addProduct(Long.parseLong(productId), Integer.parseInt(quantity), loggedUser.getCartId());

                /*update itemsNumberInCart*/
                int itemNumber = loggedUser.getItemsNumberInCart();
                loggedUser.setItemsNumberInCart(itemNumber + Integer.parseInt(quantity));
                loggedUserDAO.update(loggedUser);

                String completedOPerationMessage = "Added to Cart";
                request.setAttribute("completedOperationMessage",completedOPerationMessage);

            }

            commonProductView(request,daoFactory,productId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedUser!=null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "catalogManagement/productView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }
        }
    }

    public static void addtoWishlist(HttpServletRequest request, HttpServletResponse response){

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;
        List<HomevideoProduct> homevideoProducts;
        String applicationMessage;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String productId = request.getParameter("productId");

            if (loggedUser == null) {
                applicationMessage = "LOG IN or SIGN IN to ADD TO WISHLIST";
                request.setAttribute("applicationMessage", applicationMessage);

            } else {
                HomevideoProduct homevideoProduct = new HomevideoProduct();
                homevideoProduct.setProductId(Long.parseLong(productId));

                Customer customer = new Customer();
                customer.setCustomerId(loggedUser.getUserId());

                HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
                homevideoProductDAO.insertInWishlistByCustomer(customer, homevideoProduct);

                String completedOPerationMessage = "Added to Wishlist";
                request.setAttribute("completedOperationMessage",completedOPerationMessage);

            }
            commonProductView(request, daoFactory,productId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "catalogManagement/productView");


        }catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }

        }
    }

    private static void commonProductView(HttpServletRequest request, DAOFactory daoFactory,String productId){

        HomevideoProduct homevideoProduct = null;

        String entityFilter= request.getParameter("entityFilter");
        String[] typeFilter = request.getParameterValues("typeFilter");
        String[] formatFilter = request.getParameterValues("formatFilter");
        String[] genreFilter = request.getParameterValues("genreFilter");
        String orderFilter =  request.getParameter("orderFilter");
        String descendant = request.getParameter("descendant");
        String thisPage = request.getParameter("thisPage");
        String searchString = request.getParameter("searchString");

        if(productId!=null){
            HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
            homevideoProduct = homevideoProductDAO.findWithLanguagesbyId(Long.parseLong(productId));
        }

        request.setAttribute("homevideoProduct",homevideoProduct);

        request.setAttribute("entityFilter",entityFilter);
        request.setAttribute("typeFilter",typeFilter);
        request.setAttribute("formatFilter",formatFilter);
        request.setAttribute("genreFilter",genreFilter);
        request.setAttribute("orderFilter",orderFilter);
        request.setAttribute("descendant",descendant);
        request.setAttribute("searchString",searchString);
        request.setAttribute("thisPage",thisPage);

    }
    private static List<String> getCheckedList(String[] typeFilter, String[] formatFilter, String[] genreFilter) {

        List<String> checkedlist = new ArrayList<>();
        if(typeFilter != null) {
            checkedlist.addAll(Arrays.asList(typeFilter));
        }
        if(formatFilter != null) {
            checkedlist.addAll(Arrays.asList(formatFilter));
        }
        if(genreFilter != null) {
            checkedlist.addAll(Arrays.asList(genreFilter));
        }
        if(checkedlist.size()>0)
            return checkedlist;
        else return null;
    }
}
