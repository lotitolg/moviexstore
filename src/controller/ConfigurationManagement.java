package controller;

import model.dao.AdminConfigurationDAO;
import model.dao.DAOFactory;
import model.mo.AdminConfiguration;
import model.session.dao.LoggedAdminDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedAdmin;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigurationManagement {

    public static void view(HttpServletRequest request, HttpServletResponse response) {
        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();

        List<AdminConfiguration> adminConfigurationList;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AdminConfigurationDAO adminConfigurationDAO = daoFactory.getAdminConfigurationDAO();
            adminConfigurationList = adminConfigurationDAO.readAll();

            daoFactory.commitTransaction();

            request.setAttribute("adminConfigurationList",adminConfigurationList);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "configManagement/modConfigView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modify(HttpServletRequest request, HttpServletResponse response){

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AdminConfigurationDAO adminConfigurationDAO = daoFactory.getAdminConfigurationDAO();
            List<AdminConfiguration> adminConfigurationList = adminConfigurationDAO.readAll();

            for(AdminConfiguration adminConfiguration : adminConfigurationList){
                adminConfigurationDAO.update(adminConfiguration.getName(),
                        request.getParameter(adminConfiguration.getName()));
                adminConfiguration.setValue(request.getParameter(adminConfiguration.getName()));
            }

            daoFactory.commitTransaction();

            request.setAttribute("adminConfigurationList",adminConfigurationList);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "configManagement/modConfigView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

}
