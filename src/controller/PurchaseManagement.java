package controller;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.*;
import model.dao.exception.AvailabilityOutOfStockException;
import model.mo.BillingInfo;
import model.mo.Cart;
import model.mo.Coupon;
import model.mo.Customer;
import model.mo.DeliveryAddress;
import model.mo.ProductInCart;
import model.session.dao.LoggedUserDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedUser;
import services.config.Configuration;
import services.logservice.LogService;

public class PurchaseManagement {

    private PurchaseManagement() {
    }

    public static void orderSummary(HttpServletRequest request, HttpServletResponse response) {

        /* Controlla sia la pagina sia l'inserimento del Coupon*/
        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        List<ProductInCart> productsInCart;
        DAOFactory daoFactory = null;
        Logger logger = LogService.getApplicationLogger();

        String applicationMessage = null;

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            /* Passo i Customer senza
            Customer customer = customerDAO.findCustomerbyId(loggedUser.getUserId());
            customer.setPassword(null);
            customer.setBanned(false);
            customer.setDeleted(false);
             */

            /* Delivery Address information*/
            DeliveryAddressDAO deliveryAddressDAO = daoFactory.getDeliveryAddressDAO();
            DeliveryAddress deliveryAddress = deliveryAddressDAO.getActiveAddressbyCustomer(loggedUser.getUserId());

            /*Billing INFO*/
            BillingInfoDAO billingInfoDAO = daoFactory.getBillingInfoDAO();
            BillingInfo billingInfo = billingInfoDAO.findActiveBillingInfobyCustomerId(loggedUser.getUserId());

            if (deliveryAddress == null) {
                applicationMessage = "Insert a delivery address and billing info payment before purchase";
                request.setAttribute("object", "Address");
                request.setAttribute("viewUrl", "infoUserManagement/profileUser");
                request.setAttribute("applicationMessage", applicationMessage);

            } else if (billingInfo == null) {
                applicationMessage = "Insert billing info payment before purchase";
                request.setAttribute("object", "Billing");
                request.setAttribute("viewUrl", "infoUserManagement/profileUser");
                request.setAttribute("applicationMessage", applicationMessage);
            } else {

                billingInfo.setCardNum(billingInfo.getCardNum().substring(billingInfo.getCardNum().length() - 4));

                CartDAO cartDAO = daoFactory.getCartDAO();
                productsInCart = cartDAO.findProductsInCartByCartId(loggedUser.getCartId());

                String couponCode = request.getParameter("couponCode");
                CouponDAO couponDAO = daoFactory.getCouponDAO();
                Coupon coupon = null;
                if(couponCode!=null && couponCode.length()>0) {
                    coupon = couponDAO.findCouponbyCode(couponCode);

                    if(coupon==null){
                        applicationMessage = "Incorrect coupon code";
                        request.setAttribute("applicationMessage", applicationMessage);
                    }
                }

                AdminConfigurationDAO adminConfigurationDAO = daoFactory.getAdminConfigurationDAO();
                double shippingPrice = Double.parseDouble(adminConfigurationDAO.read(Configuration.SHIPPING_PRICE_CONFIG_NAME));

                request.setAttribute("shippingPrice",shippingPrice);
                request.setAttribute("coupon_discount", coupon);
                request.setAttribute("productsInCart", productsInCart);
                request.setAttribute("viewUrl", "purchaseManagement/OrderSummary");
            }

            daoFactory.commitTransaction();

            request.setAttribute("deliveryAddress", deliveryAddress);
            request.setAttribute("billingInfo", billingInfo);

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void Purchase(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        String applicationMessage= null;
        DAOFactory daoFactory = null;

        List<ProductInCart> productsInCart;
        boolean completedOrder=false;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();


            /*__________I DAO CHE DEVO USARE________*/
            CartDAO cartDAO = daoFactory.getCartDAO();
            OrderDAO orderDAO = daoFactory.getOrderDAO();
            HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
            CouponDAO couponDAO = daoFactory.getCouponDAO();
            DeliveryAddressDAO deliveryAddressDAO = daoFactory.getDeliveryAddressDAO();
            BillingInfoDAO billingInfoDAO = daoFactory.getBillingInfoDAO();


            /*Prelevo prodotti dal carrello*/
            productsInCart = cartDAO.findProductsInCartByCartId(loggedUser.getCartId());

            /*Prelevo indirizzo di consegna*/
            DeliveryAddress deliveryAddress = deliveryAddressDAO.getActiveAddressbyCustomer(loggedUser.getUserId());

            /*Prelevo dati pagamento*/
            BillingInfo billingInfo = billingInfoDAO.findActiveBillingInfobyCustomerId(loggedUser.getUserId());

            /*controllo che la carta non sia scaduta*/
            if(billingInfo.getCardexpdate().before(new Date())){
                applicationMessage = "The card is expired!";
                request.setAttribute("applicationMessage", applicationMessage);
            }
            else {

                Coupon coupon = null;
                String couponId = request.getParameter("couponId");

                if (couponId.length() > 0) {
                    coupon = new Coupon();
                    coupon.setCouponId(Long.parseLong(couponId));
                }

                try {

                    homevideoProductDAO.updateProductsPurchase(productsInCart);

                    Cart cart = new Cart();
                    cart.setCartId(loggedUser.getCartId());

                    Customer customer = new Customer();
                    customer.setCustomerId(loggedUser.getUserId());

                    AdminConfigurationDAO adminConfigurationDAO = daoFactory.getAdminConfigurationDAO();
                    double shippingPrice = Double.parseDouble(adminConfigurationDAO.read(Configuration.SHIPPING_PRICE_CONFIG_NAME));

                    orderDAO.ConnectOrdertoCart(cart, coupon,
                            deliveryAddress, customer,
                            billingInfo,Double.parseDouble(request.getParameter("totalSpending")),shippingPrice);
                    cartDAO.checkOutCart(customer);
                    cart = cartDAO.assignNewCart(customer);

                    loggedUser.setCartId(cart.getCartId());
                    loggedUser.setItemsNumberInCart(0);
                    loggedUserDAO.update(loggedUser);

                    billingInfoDAO.setUsed(billingInfo);
                    deliveryAddressDAO.setUsed(deliveryAddress);

                    completedOrder=true;

                } catch (AvailabilityOutOfStockException e) {
                    applicationMessage = e.getMessage();
                    logger.log(Level.WARNING, e.getMessage());
                    request.setAttribute("applicationMessage", applicationMessage);

                }
            }

            daoFactory.commitTransaction(); //Commit

            request.setAttribute("completedOrder",completedOrder);
            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "purchaseManagement/orderOutcome");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }


    /*    public static void couponControl(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            List<ProductInCart> productsInCart = new ArrayList<>();

            CartDAO cartDAO = daoFactory.getCartDAO();

            productsInCart = cartDAO.findCartWithProductbyUserId(loggedUser.getUserId());

            //controllo deil cuopon inserito
            CouponDAO couponDAO = daoFactory.getCouponDAO();
            Coupon coupon = couponDAO.findCouponbyCode(request.getParameter("CodeId"));
            //Coupon coupon = couponDAO.findCouponbyCode("1234567890");

            daoFactory.commitTransaction();

            request.setAttribute("productsInCart", productsInCart);

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("HO", coupon.getDiscountPercent());
            //coupon = null;
            if (coupon == null) {
                request.setAttribute("viewUrl", "purchaseManagement/OrderSummary");
            } else {
                request.setAttribute("coupon_discount", coupon);
                request.setAttribute("viewUrl", "purchaseManagement/OrderSummary");
            }

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }*/
}
