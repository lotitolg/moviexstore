
package controller;

import model.dao.*;
import model.dao.exception.DuplicatedUsernameException;
import model.mo.*;
import model.session.dao.LoggedUserDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedUser;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HomeManagement {

    private HomeManagement() {
    }

    public static void view(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;

        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            commonView(request,daoFactory);

            daoFactory.commitTransaction();


            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "homeManagement/view");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void logon(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedUser loggedUser;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            /* loggedUser = loggedUserDAO.find(); */

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String username = request.getParameter("username");
            String password = request.getParameter("password");

            CustomerDAO customerDAO = daoFactory.getCustomerDAO();
            Customer customer = customerDAO.findByUsername(username);

            if (customer == null || !customer.getPassword().equals(password)) {
                loggedUserDAO.destroy();
                applicationMessage = "Incorrect username or password";
                loggedUser = null;
            } else if (customer.isBanned()) {
                applicationMessage = "Unable to logon, you were banned";
                loggedUserDAO.destroy();
                loggedUser = null;
            }else {
                CartDAO cartDAO = daoFactory.getCartDAO();
                Cart cart = cartDAO.findCartInfobyUserId(customer.getCustomerId());
                cart.setNumberOfItem(cartDAO.countItemsInCart(cart));
                loggedUser = loggedUserDAO.create(customer.getCustomerId(),
                        customer.getFirstname(),
                        customer.getSurname(),
                        cart.getCartId(),
                        cart.getNumberOfItem());
            }
            commonView(request,daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedUser != null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("viewUrl", "homeManagement/view");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void logout(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUserDAO.destroy();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            commonView(request,daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", false);
            request.setAttribute("loggedUser", null);
            request.setAttribute("viewUrl", "homeManagement/view");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }



    }

    private static void commonView(HttpServletRequest request, DAOFactory daoFactory){

        List<AudiovisionWork> topRated;
        List<HomevideoProduct> bestSellers;
        List<HomevideoProduct> latestReleases;
        List<Genre> genres;

        HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
        AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();
        GenreDAO genreDAO = daoFactory.getGenreDAO();

        latestReleases = homevideoProductDAO.findNMostRecent(8);
        bestSellers = homevideoProductDAO.findNBestSeller(8);
        topRated = audiovisionWorkDAO.findNTopRated(8);
        genres=genreDAO.extractGenres();

        request.setAttribute("latestReleases",latestReleases);
        request.setAttribute("bestSellers",bestSellers);
        request.setAttribute("topRated",topRated);
        request.setAttribute("genres",genres);

    }

    public static void subscribeView(HttpServletRequest request, HttpServletResponse response) {

        Logger logger = LogService.getApplicationLogger();
        try {
            request.setAttribute("viewUrl", "homeManagement/subscribeView");
            request.setAttribute("loggedOn", false);
            request.setAttribute("loggedUser", null);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void subscribe(HttpServletRequest request, HttpServletResponse response) {

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;
        String applicationMessage = null;
        String completedOperationMessage = null;

        try {
            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            CustomerDAO customerDAO = daoFactory.getCustomerDAO();
            CartDAO cartDAO = daoFactory.getCartDAO();
            BillingInfoDAO billingInfoDAO = daoFactory.getBillingInfoDAO();
            DeliveryAddressDAO deliveryAddressDAO = daoFactory.getDeliveryAddressDAO();

            try {
                customerDAO.insert(
                        request.getParameter("firstname"),
                        request.getParameter("surname"),
                        request.getParameter("username"),
                        request.getParameter("password"),
                        request.getParameter("email")
                );
                completedOperationMessage = "Registration successful, go to login";
                request.setAttribute("completedOperationMessage", completedOperationMessage);

            } catch (DuplicatedUsernameException e) {
                applicationMessage = e.getMessage();
                logger.log(Level.WARNING, e.getMessage());
                request.setAttribute("applicationMessage", applicationMessage);
            }

            Customer customer = customerDAO.findByUsername(request.getParameter("username"));
            cartDAO.assignNewCart(customer);

            /* String sDate1 = "31/12/1998";
            java.util.Date date = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
            java.sql.Date sdate = new java.sql.Date(date.getTime());
            billingInfoDAO.insert(customer, "0000000000000000", sdate, "Visa");
            deliveryAddressDAO.insert(customer, null, null, null, null);*/

            daoFactory.commitTransaction();

            request.setAttribute("viewUrl", "homeManagement/subscribeView");
            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("loggedOn", false);
            request.setAttribute("loggedUser", null);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }
}


