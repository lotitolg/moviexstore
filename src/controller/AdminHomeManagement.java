package controller;

import model.dao.AdminDAO;
import model.dao.DAOFactory;
import model.dao.exception.DuplicatedUsernameException;
import model.mo.Admin;
import model.session.dao.LoggedAdminDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedAdmin;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminHomeManagement {

    private AdminHomeManagement() {
    }

    public static void adminHomeView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminHomeManagement/adminHomeView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void logon(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String username = request.getParameter("username");
            String password = request.getParameter("password");

            AdminDAO adminDAO = daoFactory.getAdminDAO();
            Admin admin = adminDAO.findbyUsername(username);


            if (admin == null || !admin.getPassword().equals(password)) {
                applicationMessage = "Incorrect username or password";
                loggedAdminDAO.destroy();
                loggedAdmin = null;
            } else if (!admin.isApproved()) {
                applicationMessage = "Your request to become an admin has not yet been approved";
                loggedAdminDAO.destroy();
                loggedAdmin = null;
            } else {
                loggedAdmin = loggedAdminDAO.create(admin.getAdminId(), admin.getFirstname(), admin.getSurname());
            }

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminHomeManagement/adminHomeView");
            request.setAttribute("applicationMessage", applicationMessage);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void logout(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdminDAO.destroy();


            request.setAttribute("loggedOn", false);
            request.setAttribute("loggedAdmin", null);
            request.setAttribute("viewUrl", "adminHomeManagement/adminHomeView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void subscribeView(HttpServletRequest request, HttpServletResponse response) {

        Logger logger = LogService.getApplicationLogger();
        try {
            request.setAttribute("viewUrl", "adminHomeManagement/adminSignInModView");
            request.setAttribute("loggedOn", false);
            request.setAttribute("loggedAdmin", null);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void subscribe(HttpServletRequest request, HttpServletResponse response) {

        Logger logger = LogService.getApplicationLogger();
        DAOFactory daoFactory = null;
        String applicationMessage = null;
        String completedOperationMessage = null;

        try {
            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            AdminDAO adminDAO = daoFactory.getAdminDAO();

            try {
                adminDAO.insert(
                        request.getParameter("firstname"),
                        request.getParameter("surname"),
                        request.getParameter("username"),
                        request.getParameter("password")
                );

                completedOperationMessage = "Registration successful, wait for an admin's approval";
                request.setAttribute("completedOperationMessage", completedOperationMessage);
                request.setAttribute("viewUrl", "adminHomeManagement/adminHomeView");

            } catch (DuplicatedUsernameException e) {
                applicationMessage = e.getMessage();
                logger.log(Level.WARNING, e.getMessage());
                request.setAttribute("applicationMessage", applicationMessage);
                request.setAttribute("viewUrl", "adminHomeManagement/adminSignInModView");
            } finally {

                daoFactory.commitTransaction();
                request.setAttribute("loggedOn", false);
                request.setAttribute("loggedAdmin", null);

            }

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyProfileView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();
        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AdminDAO adminDAO = daoFactory.getAdminDAO();
            Admin admin = adminDAO.findbyAdminId(loggedAdmin.getAdminId());

            daoFactory.commitTransaction();

            request.setAttribute("admin", admin);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminHomeManagement/adminSignInModView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyProfile(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        DAOFactory daoFactory = null;
        String applicationMessage = null;
        String completedOperationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AdminDAO adminDAO = daoFactory.getAdminDAO();
            Admin admin = adminDAO.findbyAdminId(loggedAdmin.getAdminId());

            admin.setFirstname(request.getParameter("firstname"));
            admin.setSurname(request.getParameter("surname"));
            admin.setUsername(request.getParameter("username"));
            admin.setPassword(request.getParameter("password"));

            try{
                adminDAO.update(admin);
                completedOperationMessage = "Update successful";
                request.setAttribute("completedOperationMessage", completedOperationMessage);
                request.setAttribute("viewUrl", "adminHomeManagement/adminHomeView");

                loggedAdmin.setFirstname(admin.getFirstname());
                loggedAdmin.setSurname(admin.getSurname());
                loggedAdminDAO.update(loggedAdmin);

            }catch (DuplicatedUsernameException e)
            {
                applicationMessage = e.getMessage();
                logger.log(Level.WARNING, e.getMessage());
                request.setAttribute("applicationMessage", applicationMessage);
                request.setAttribute("viewUrl", "adminHomeManagement/adminSignInModView");
            } finally {

                daoFactory.commitTransaction();
                request.setAttribute("loggedOn", loggedAdmin != null);
                request.setAttribute("loggedAdmin", loggedAdmin);
            }

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

}
