package controller;

import java.util.ArrayList;
import java.util.List;

import model.dao.*;
import model.mo.*;
import model.session.dao.LoggedAdminDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedAdmin;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminUserManagement {

    private AdminUserManagement() {
    }

    /*---CUSTOMER---*/

    //Estrae lista clienti
    public static void customerView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        List<String> initials;
        List<Customer> customers;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            CustomerDAO customerDAO = daoFactory.getCustomerDAO();

            initials = customerDAO.findInitial();

            String selectedInitial = request.getParameter("selectedInitial");
            String searchString = request.getParameter("searchString");

            if (selectedInitial == null || (!selectedInitial.equals("*") && !initials.contains(selectedInitial))) {
                if (initials.size() > 0) {
                    selectedInitial = initials.get(0);
                } else {
                    selectedInitial = "*";
                }
            }

            customers = customerDAO.findByInitialAndSearchString((selectedInitial.equals("*") ? null : selectedInitial), searchString);

            daoFactory.commitTransaction();

            request.setAttribute("customersList", customers);

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);

            request.setAttribute("selectedInitial", selectedInitial);
            request.setAttribute("initials", initials);

            request.setAttribute("viewUrl", "adminUserManagement/userView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    //Estrae informazione di un cliente
    public static void customerInfoView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            long customerId = Long.parseLong(request.getParameter("customerId"));

            daoFactory.beginTransaction();

            commonCustomerInfoView(request, daoFactory, customerId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/customerInfoView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void deleteCustomer(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            /* il customer da cancellare*/
            Customer customer = new Customer();
            long customerId = Long.parseLong(request.getParameter("customerId"));
            customer.setCustomerId(customerId);

            daoFactory.beginTransaction();

            CustomerDAO customerDAO = daoFactory.getCustomerDAO();
            customerDAO.delete(customerId);

            commonCustomerInfoView(request,daoFactory,customerId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);

            request.setAttribute("viewUrl", "adminUserManagement/customerInfoView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void unDeleteCustomer(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            // il customer da cancellare
            Customer customer = new Customer();
            long customerId = Long.parseLong(request.getParameter("customerId"));
            customer.setCustomerId(customerId);

            daoFactory.beginTransaction();

            CustomerDAO customerDAO = daoFactory.getCustomerDAO();
            customerDAO.undelete(customerId);

            commonCustomerInfoView(request,daoFactory,customerId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/customerInfoView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void banCustomer(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            /* il customer da cancellare*/
            Customer customer = new Customer();
            long customerId = Long.parseLong(request.getParameter("customerId"));
            customer.setCustomerId(customerId);

            daoFactory.beginTransaction();

            CustomerDAO customerDAO = daoFactory.getCustomerDAO();
            customerDAO.ban(customerId);

            commonCustomerInfoView(request, daoFactory, customerId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/customerInfoView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void unBanCustomer(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            /* il customer da cancellare*/
            Customer customer = new Customer();
            long customerId = Long.parseLong(request.getParameter("customerId"));
            customer.setCustomerId(customerId);

            daoFactory.beginTransaction();

            CustomerDAO customerDAO = daoFactory.getCustomerDAO();
            customerDAO.unBan(customerId);

            commonCustomerInfoView(request, daoFactory, customerId);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);

            request.setAttribute("viewUrl", "adminUserManagement/customerInfoView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    private static void commonCustomerInfoView(HttpServletRequest request, DAOFactory daoFactory, Long customerId) {

        CustomerDAO customerDAO = daoFactory.getCustomerDAO();
        DeliveryAddressDAO deliveryAddressDAO = daoFactory.getDeliveryAddressDAO();

        Customer customer = customerDAO.findCustomerbyIdDeletedEven(customerId);
        DeliveryAddress deliveryAddress = deliveryAddressDAO.getActiveAddressbyCustomer(customerId);
        deliveryAddress.setCustomer(customer);

        customer.setPassword(null);     //per non passare la password al client
        request.setAttribute("customer", customer);
        request.setAttribute("deliveryAddress", deliveryAddress);

    }

    //Ordini di un cliente
    public static void showCustomerOrders(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            commonShowCustomerOrders(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/ordersViewByCustomer");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyCustomerOrderStatus(HttpServletRequest request, HttpServletResponse response) {

        /* La devi Modificare non è ancora finita e c'è anche la jsp da fare e copiare e incollare da ordersInfor che è già pronta*/
        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            long orderId = Long.parseLong(request.getParameter("orderId"));

            daoFactory.beginTransaction();

            OrderDAO orderDAO = daoFactory.getOrderDAO();
            orderDAO.changeOrderStatus(orderId);// cambio lo status dell'ordine

            commonShowCustomerOrders(request,daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/ordersViewByCustomer");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    private static void commonShowCustomerOrders(HttpServletRequest request, DAOFactory daoFactory) {

        List<Order> orderList = new ArrayList<>();

        Customer customer = new Customer();
        long customerId = Long.parseLong(request.getParameter("customerId"));
        customer.setCustomerId(customerId);

        CustomerDAO customerDAO = daoFactory.getCustomerDAO();
        customer = customerDAO.findCustomerbyIdDeletedEven(customerId);

        OrderDAO orderDAO = daoFactory.getOrderDAO();
        orderList = orderDAO.ListOfOrdersForCustomer(customer.getCustomerId());// lista delle info di tutti gli ordini

        customer.setPassword(null);     //per non passare la password al cliente
        request.setAttribute("customer", customer);
        request.setAttribute("orderList", orderList);
    }

    /*---ADMIN---*/

    public static void adminView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            commonAdminView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/userView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void approveAdmin(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String adminId = request.getParameter("adminId");

            Admin admin = new Admin();
            admin.setAdminId(Long.parseLong(adminId));

            AdminDAO adminDAO = daoFactory.getAdminDAO();
            adminDAO.approve(admin);

            commonAdminView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/userView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void deleteAdmin(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String adminId = request.getParameter("adminId");

            Admin admin = new Admin();
            admin.setAdminId(Long.parseLong(adminId));

            AdminDAO adminDAO = daoFactory.getAdminDAO();
            adminDAO.delete(admin);

            commonAdminView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminUserManagement/userView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    private static void commonAdminView(HttpServletRequest request, DAOFactory daoFactory) {

        List<String> initials;
        List<Admin> admins;

        AdminDAO adminDAO = daoFactory.getAdminDAO();

        initials = adminDAO.findInitial();

        String selectedInitial = request.getParameter("selectedInitial");
        String searchString = request.getParameter("searchString");
        String waitApproveString = request.getParameter("waitApprove");

        if (selectedInitial == null || (!selectedInitial.equals("*") && !initials.contains(selectedInitial))) {
            if (initials.size() > 0) {
                selectedInitial = initials.get(0);
            } else {
                selectedInitial = "*";
            }
        }
        Boolean waitApprove;
        if (waitApproveString != null)
            waitApprove = waitApproveString.equals("true");
        else waitApprove = false;

        admins = adminDAO.findByInitialAndSearchString((selectedInitial.equals("*") ? null : selectedInitial), searchString, waitApprove);

        request.setAttribute("selectedInitial", selectedInitial);
        request.setAttribute("initials", initials);
        request.setAttribute("admins", admins);
        request.setAttribute("searchString", searchString);
        request.setAttribute("waitApprove", waitApprove);

    }

}
