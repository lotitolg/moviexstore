package controller;

import java.util.ArrayList;
import java.util.List;
import model.dao.DAOFactory;
import model.session.dao.LoggedAdminDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedAdmin;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.dao.OrderDAO;
import model.mo.Order;

public class AdminTrackingManagement {

    private AdminTrackingManagement() {
    }

    public static void trackingView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            ArrayList<Order> orderList = new ArrayList<>();

            daoFactory.beginTransaction();

            OrderDAO orderDAO = daoFactory.getOrderDAO();
            orderList = orderDAO.listOfAllOrdersNotDelivered();

            daoFactory.commitTransaction();

            request.setAttribute("orderList", orderList);

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);

            request.setAttribute("viewUrl", "adminTrackingManagement/productsTracking");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyCustomerOrderStatus(HttpServletRequest request, HttpServletResponse response) {

        /* La devi Modificare non è ancora finita e c'è anche la jsp da fare e copiare e incollare da ordersInfor che è già pronta*/
        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            long orderId = Long.parseLong(request.getParameter("orderId"));

            List<Order> orderList = new ArrayList<>();

            daoFactory.beginTransaction();

            OrderDAO orderDAO = daoFactory.getOrderDAO();
            
            orderDAO.changeOrderStatus(orderId);

            orderList = orderDAO.listOfAllOrdersNotDelivered();

            daoFactory.commitTransaction();

            request.setAttribute("orderList", orderList);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminTrackingManagement/productsTracking");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {}
        }
    }
}
