package controller;

import model.dao.DAOFactory;
import model.dao.HomevideoProductDAO;
import model.mo.Customer;
import model.mo.HomevideoProduct;
import model.session.dao.LoggedUserDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedUser;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WishlistManagement {

    private WishlistManagement(){}

    public static void wishlistView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;
        List<HomevideoProduct> homevideoProducts;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            commonView(request, daoFactory, sessionDAOFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedUser!=null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "wishlistManagement/wishlistView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }

        }
    }

    public static void removeItem(HttpServletRequest request, HttpServletResponse response){

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;
        List<HomevideoProduct> homevideoProducts;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            String productId = request.getParameter("productId");

            HomevideoProduct homevideoProduct = new HomevideoProduct();
            homevideoProduct.setProductId(Long.parseLong(productId));

            Customer customer = new Customer();
            customer.setCustomerId(loggedUser.getUserId());

            HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
            homevideoProductDAO.removeFromWishlistByCustomer(customer, homevideoProduct);

            commonView(request, daoFactory, sessionDAOFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedUser!=null);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "wishlistManagement/wishlistView");


        }catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }

        }
    }

    private static void commonView(HttpServletRequest request, DAOFactory daoFactory, SessionDAOFactory sessionDAOFactory) {

        List<HomevideoProduct> homevideoProducts;

        LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
        LoggedUser loggedUser = loggedUserDAO.find();

        Customer customer = new Customer();
        customer.setCustomerId(loggedUser.getUserId());

        HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();

        homevideoProducts = homevideoProductDAO.findWishlistbyCustomerId(customer);

        request.setAttribute("homevideoProducts", homevideoProducts);
    }
}
