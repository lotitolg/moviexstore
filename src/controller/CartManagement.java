package controller;

import model.dao.CartDAO;
import model.dao.DAOFactory;
import model.mo.ProductInCart;
import model.session.dao.LoggedUserDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedUser;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CartManagement {

    private CartManagement() {
    }

    public static void cartView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;
        List<ProductInCart> productsInCart;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();


            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            commonView(request, daoFactory, sessionDAOFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", true);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "cartManagement/cartView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }

        }
    }

    public static void updateItemQuantity(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;


        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String productId = request.getParameter("productId");
            String quantity = request.getParameter("quantity");
            String oldQuantity = request.getParameter("oldQuantity");

            CartDAO cartDAO = daoFactory.getCartDAO();
            cartDAO.updateItemQuantity(Long.parseLong(productId), Integer.parseInt(quantity), loggedUser.getCartId());

            int itemNumber = loggedUser.getItemsNumberInCart();
            loggedUser.setItemsNumberInCart(itemNumber - Integer.parseInt(oldQuantity) + Integer.parseInt(quantity));
            loggedUserDAO.update(loggedUser);

            commonView(request, daoFactory, sessionDAOFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", true);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "cartManagement/cartView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }
        }


    }

    public static void removeItem(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedUser loggedUser;
        DAOFactory daoFactory = null;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
            loggedUser = loggedUserDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String productId = request.getParameter("productId");
            String oldQuantity = request.getParameter("oldQuantity");

            CartDAO cartDAO = daoFactory.getCartDAO();
            cartDAO.removeItem(Long.parseLong(productId),loggedUser.getCartId());

            int itemNumber = loggedUser.getItemsNumberInCart();
            loggedUser.setItemsNumberInCart(itemNumber - Integer.parseInt(oldQuantity));
            loggedUserDAO.update(loggedUser);

            commonView(request, daoFactory, sessionDAOFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", true);
            request.setAttribute("loggedUser", loggedUser);
            request.setAttribute("viewUrl", "cartManagement/cartView");


        }catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable ignored) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable ignored) {
            }
        }
    }

    private static void commonView(HttpServletRequest request, DAOFactory daoFactory, SessionDAOFactory sessionDAOFactory) {

        List<ProductInCart> productsInCart;

        LoggedUserDAO loggedUserDAO = sessionDAOFactory.getLoggedUserDAO();
        LoggedUser loggedUser = loggedUserDAO.find();

        CartDAO cartDAO = daoFactory.getCartDAO();
        productsInCart = cartDAO.findProductsInCartByCartId(loggedUser.getCartId());

        Boolean[] warningsAvailability = checkAvailability(productsInCart);

        request.setAttribute("warningsAvailability",warningsAvailability);
        request.setAttribute("productsInCart", productsInCart);
    }

    private static Boolean[] checkAvailability(List<ProductInCart> productsInCart){

        Boolean[] warnings = new Boolean[productsInCart.size()];
        for(int i=0;i<productsInCart.size();i++)
            warnings[i] = productsInCart.get(i).getHomevideoProducts().getAvailable() < productsInCart.get(i).getItemQuantity();
        return warnings;
    }

}



