package controller;

import model.dao.CouponDAO;
import model.dao.DAOFactory;
import model.dao.exception.DuplicatedObjectException;
import model.mo.Coupon;
import model.session.dao.LoggedAdminDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedAdmin;
import services.config.Configuration;
import services.logservice.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminCouponManagement {

    private AdminCouponManagement() {
    }

    public static void couponViews(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            daoFactory.beginTransaction();

            commonCouponView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCouponManagement/couponsView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void couponDisable(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            long couponDis = Long.parseLong(request.getParameter("couponId"));

            daoFactory.beginTransaction();

            CouponDAO couponDAO = daoFactory.getCouponDAO();
            couponDAO.disableCoupon(couponDis);

            commonCouponView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);


            request.setAttribute("viewUrl", "adminCouponManagement/couponsView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void couponUnDisable(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);

            long couponDis = Long.parseLong(request.getParameter("couponId"));

            daoFactory.beginTransaction();

            CouponDAO couponDAO = daoFactory.getCouponDAO();
            couponDAO.unDisableCoupon(couponDis);

            commonCouponView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCouponManagement/couponsView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void addCoupon(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            Coupon newCoupon = new Coupon();
            newCoupon.setCode(request.getParameter("couponCode"));
            newCoupon.setDiscountPercent(Integer.parseInt(request.getParameter("couponDiscount")));

            boolean exist = false;

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            CouponDAO couponDAO = daoFactory.getCouponDAO();

            try {
                couponDAO.insert(newCoupon.getCode(), newCoupon.getDiscountPercent());
            }catch(DuplicatedObjectException e){
                logger.log(Level.WARNING, e.getMessage());
                applicationMessage = "Coupon Already Exists";
                request.setAttribute("applicationMessage", applicationMessage);
            }


            daoFactory.commitTransaction();

            String completedOperationMessage = "Added Successfully";
            request.setAttribute("completedOperationMessage", completedOperationMessage);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCouponManagement/couponInsert");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void addCouponView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCouponManagement/couponInsert");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    private static void commonCouponView(HttpServletRequest request, DAOFactory daoFactory) {

        CouponDAO couponDAO = daoFactory.getCouponDAO();

        List<Coupon> couponList = couponDAO.findAllCoupons();

        request.setAttribute("couponList", couponList);

    }
}
