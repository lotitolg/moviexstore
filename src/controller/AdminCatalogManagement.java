package controller;

import model.dao.*;
import model.dao.exception.DuplicatedObjectException;
import model.mo.*;
import model.session.dao.LoggedAdminDAO;
import model.session.dao.SessionDAOFactory;
import model.session.mo.LoggedAdmin;
import services.config.Configuration;
import services.logservice.LogService;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminCatalogManagement {

    private AdminCatalogManagement() {
    }

    /*----HOMEVIDEO----*/

    public static void HomevideoManagementView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            commonHVManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void insertHomevideoView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String orderFilter = request.getParameter("orderFilter");
            String descendantString = request.getParameter("descendant");
            String thisPageString = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");
            String isThisLastPage = request.getParameter("isThisLastPage");
            String multipleView = request.getParameter("multipleView");

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/homevideoManagement/insModHomevideoView");

            request.setAttribute("orderFilter", orderFilter);
            request.setAttribute("descendant", descendantString);
            request.setAttribute("searchString", searchString);
            request.setAttribute("thisPage", thisPageString);
            request.setAttribute("isThisLastPage", isThisLastPage);
            request.setAttribute("multipleView", multipleView);


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void insertHomevideo(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String name = request.getParameter("name");
            String work = request.getParameter("workId");
            String format = request.getParameter("format");
            String price = request.getParameter("price");
            String numberSupport = request.getParameter("numberSupport");
            String[] languages = request.getParameterValues("languages");
            String releaseDate = request.getParameter("releaseDate");
            String available = request.getParameter("available");
            //String pictureUrl = request.getParameter("pictureUrl");

            /*Upload file image*/
            String fileName="";
            Part filePart = request.getPart("pictureFile"); // Retrieves <input type="file" name="file">
            if(filePart.getSize()>0) {
                fileName = Paths.get(filePart.getName()).getFileName().toString(); // MSIE fix.
                InputStream fileContent = filePart.getInputStream();
                String[] splitted = fileName.split("\\.");
                String fileExt = splitted[1];

                BufferedImage pictureFile = ImageIO.read(fileContent);
                ImageIO.write(pictureFile, fileExt, new File(Configuration.GLOBAL_UPLOAD_IMAGES_PATH + fileName));
            }

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
            AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();

            String[] workId = work.split("-");
            AudiovisionWork audiovisionWork = audiovisionWorkDAO.findWithGenresbyId(Long.parseLong(workId[0]));

            if (audiovisionWork == null) {
                applicationMessage = "Work doesn't exists";
            } else {

                Date releaseDateParsed = null;
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                /*date conversion*/
                if (releaseDate.length() > 0) {
                    releaseDateParsed = formatDate.parse(releaseDate);
                } else {
                    //releaseDateParsed = formatDate.parse(LocalDateTime.now().format(DateTimeFormatter.ofPattern(("yyyy-MM-dd"))));
                    releaseDateParsed = formatDate.parse(formatDate.format(Calendar.getInstance().getTime()));
                }
                try {

                    homevideoProductDAO.insert(audiovisionWork,
                            name, price.length() > 0 ? Double.parseDouble(price) : 0,
                            fileName, format!=null?format:"",
                            numberSupport.length() > 0 ? Integer.parseInt(numberSupport) : 0,
                            available.length() > 0 ? Integer.parseInt(available) : 0,
                            releaseDateParsed,
                            languages);

                } catch (DuplicatedObjectException e) {
                    applicationMessage = e.getMessage();
                    logger.log(Level.INFO, e.getMessage());
                }
            }

            commonHVManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }

    }

    public static void modifyHomevideoView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;
        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String orderFilter = request.getParameter("orderFilter");
            String descendantString = request.getParameter("descendant");
            String thisPageString = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");
            String isThisLastPage = request.getParameter("isThisLastPage");
            String multipleView = request.getParameter("multipleView");

            String productId = request.getParameter("Id");

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
            HomevideoProduct homevideoProduct = homevideoProductDAO.findWithLanguagesbyId(Long.parseLong(productId));

            request.setAttribute("homevideoProduct", homevideoProduct);

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/homevideoManagement/insModHomevideoView");

            request.setAttribute("orderFilter", orderFilter);
            request.setAttribute("descendant", descendantString);
            request.setAttribute("searchString", searchString);
            request.setAttribute("thisPage", thisPageString);
            request.setAttribute("isThisLastPage", isThisLastPage);
            request.setAttribute("multipleView", multipleView);


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void modifyHomevideo(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            HomevideoProduct homevideoProduct = new HomevideoProduct();

            homevideoProduct.setProductId(Long.parseLong(request.getParameter("productId")));
            homevideoProduct.setName(request.getParameter("name"));
            homevideoProduct.setFormat(request.getParameter("format"));
            homevideoProduct.setPrice(Double.parseDouble(request.getParameter("price")));
            homevideoProduct.setNumberSupport(Integer.parseInt(request.getParameter("numberSupport")));
            homevideoProduct.setLanguages(request.getParameterValues("languages"));
            homevideoProduct.setAvailable(Integer.parseInt(request.getParameter("available")));
            //homevideoProduct.setPicture_url(request.getParameter("pictureUrl"));

            String releaseDate = request.getParameter("releaseDate");


            Date releaseDateParsed;
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
            if (releaseDate.length() > 0) {
                releaseDateParsed = formatDate.parse(releaseDate);
            } else {
                releaseDateParsed = formatDate.parse(formatDate.format(Calendar.getInstance().getTime()));
            }
            homevideoProduct.setReleaseDate(releaseDateParsed);

            /*Upload file image*/
            String fileName;
            Part filePart = request.getPart("pictureFile"); // Retrieves <input type="file" name="file">
            if(filePart.getSize()>0) {
                fileName = Paths.get(filePart.getName()).getFileName().toString(); // MSIE fix.
                InputStream fileContent = filePart.getInputStream();

                String[] splitted = fileName.split("\\.");
                String fileExt = splitted[1];

                BufferedImage pictureFile = ImageIO.read(fileContent);
                ImageIO.write(pictureFile, fileExt, new File(Configuration.GLOBAL_UPLOAD_IMAGES_PATH + fileName));
            }
            else fileName = request.getParameter("pictureUrl");


            homevideoProduct.setPicture_url(fileName);

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
            AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();

            String work = request.getParameter("workId");

            String[] workId = work.split("-");
            AudiovisionWork audiovisionWork = audiovisionWorkDAO.findWithGenresbyId(Long.parseLong(workId[0]));

            if (audiovisionWork == null) {
                applicationMessage = "Work doesn't exists";
            } else {

                homevideoProduct.setAudiovisionWork(audiovisionWork);

                try {
                    homevideoProductDAO.update(homevideoProduct);
                } catch (DuplicatedObjectException e) {
                    applicationMessage = e.getMessage();
                    logger.log(Level.INFO, e.getMessage());
                }
            }

            commonHVManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }

    }

    public static void deleteHomevideo(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String productId = request.getParameter("Id");

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            HomevideoProduct homevideoProduct = new HomevideoProduct();
            homevideoProduct.setProductId(Long.parseLong(productId));

            HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
            homevideoProductDAO.delete(homevideoProduct);

            commonHVManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    private static void commonHVManView(HttpServletRequest request, DAOFactory daoFactory) {

        List<HomevideoProduct> homevideoProducts;
        int multipleView;
        boolean isThisLastPage = false;

        String orderFilter = request.getParameter("orderFilter");
        String descendantString = request.getParameter("descendant");
        String thisPageString = request.getParameter("thisPage");
        String searchString = request.getParameter("searchString");

        if (orderFilter == null) {
            orderFilter = "name";
        }

        boolean descendant;
        if (descendantString != null) {
            descendant = descendantString.equals("true");
        } else descendant = false;

        int thisPage;
        if (thisPageString == null) {
            thisPage = 1;
        } else thisPage = Integer.parseInt(thisPageString);

        AdminConfigurationDAO adminConfigurationDAO = daoFactory.getAdminConfigurationDAO();
        multipleView = Integer.parseInt(adminConfigurationDAO.read(Configuration.ADMIN_MULTIPLE_VIEW_CONFIG_NAME));

        HomevideoProductDAO homevideoProductDAO = daoFactory.getHomevideoProductDAO();
        homevideoProducts = homevideoProductDAO.findByFilters(null, null, null, orderFilter, descendant, thisPage, multipleView, searchString);

        if (homevideoProducts.size() < multipleView + 1)
            isThisLastPage = true;
        else homevideoProducts.remove(homevideoProducts.size() - 1);

        request.setAttribute("orderFilter", orderFilter);
        request.setAttribute("descendant", descendant);
        request.setAttribute("searchString", searchString);

        request.setAttribute("thisPage", thisPage);
        request.setAttribute("isThisLastPage", isThisLastPage);
        request.setAttribute("multipleView", multipleView);

        request.setAttribute("products", homevideoProducts);


    }

    /* ----AUDIOVISION WORK---- */

    public static void AWManagementView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            commonAWManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void insertAWView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String orderFilter = request.getParameter("orderFilter");
            String descendantString = request.getParameter("descendant");
            String thisPageString = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");
            String isThisLastPage = request.getParameter("isThisLastPage");
            String multipleView = request.getParameter("multipleView");

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/workManagement/insModAWView");

            request.setAttribute("orderFilter", orderFilter);
            request.setAttribute("descendant", descendantString);
            request.setAttribute("searchString", searchString);
            request.setAttribute("thisPage", thisPageString);
            request.setAttribute("isThisLastPage", isThisLastPage);
            request.setAttribute("multipleView", multipleView);


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void insertAW(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String title = request.getParameter("title");
            String duration = request.getParameter("duration");
            String rating = request.getParameter("rating");
            String type = request.getParameter("type");
            String year = request.getParameter("yearProduction");
            String plot = request.getParameter("plot");
            String trailerUrl = request.getParameter("trailerUrl");
            String studioProduction = request.getParameter("studioProduction");
            //String imageWork = request.getParameter("imageWork");

            /*Upload file image*/
            String fileName="";
            Part filePart = request.getPart("imageWork"); // Retrieves <input type="file" name="file">
            if(filePart.getSize()>0) {
                fileName = Paths.get(filePart.getName()).getFileName().toString(); // MSIE fix.
                InputStream fileContent = filePart.getInputStream();
                String[] splitted = fileName.split("\\.");
                String fileExt = splitted[1];

                BufferedImage pictureFile = ImageIO.read(fileContent);
                ImageIO.write(pictureFile, fileExt, new File(Configuration.GLOBAL_UPLOAD_IMAGES_PATH + fileName));
            }

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AudiovisionWork audiovisionWork = commonSetToAW(request, daoFactory, new AudiovisionWork());

            AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();

            try {
                audiovisionWorkDAO.insert(title, type,
                        duration.length() > 0 ? Integer.parseInt(duration) : 0,
                        rating.length() > 0 ? Float.parseFloat(rating) : 0,
                        year, plot, trailerUrl,
                        studioProduction, fileName,
                        audiovisionWork.getGenres(),
                        audiovisionWork.getActors(),
                        audiovisionWork.getDirectors());

            } catch (DuplicatedObjectException e) {
                applicationMessage = e.getMessage();
                logger.log(Level.INFO, e.getMessage());
            }

            commonAWManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }

    }

    public static void modifyAWView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;
        DAOFactory daoFactory = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String orderFilter = request.getParameter("orderFilter");
            String descendantString = request.getParameter("descendant");
            String thisPageString = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");
            String isThisLastPage = request.getParameter("isThisLastPage");
            String multipleView = request.getParameter("multipleView");

            String workId = request.getParameter("Id");

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();
            AudiovisionWork audiovisionWork = audiovisionWorkDAO.findWithGenresbyId(Long.parseLong(workId));

            ActorDAO actorDAO = daoFactory.getActorDAO();
            DirectorDAO directorDAO = daoFactory.getDirectorDAO();
            List<Actor> actors = actorDAO.findByAudiovisionWork(audiovisionWork);
            List<Director> directors = directorDAO.findByAudiovisionWork(audiovisionWork);

            Actor[] actorsArray = new Actor[actors.size()];
            Director[] directorsArray = new Director[directors.size()];
            audiovisionWork.setActors(actors.toArray(actorsArray));
            audiovisionWork.setDirectors(directors.toArray(directorsArray));

            daoFactory.commitTransaction();

            request.setAttribute("audiovisionWork", audiovisionWork);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/workManagement/insModAWView");

            request.setAttribute("orderFilter", orderFilter);
            request.setAttribute("descendant", descendantString);
            request.setAttribute("searchString", searchString);
            request.setAttribute("thisPage", thisPageString);
            request.setAttribute("isThisLastPage", isThisLastPage);
            request.setAttribute("multipleView", multipleView);


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }


    }

    public static void modifyAW(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            /*Upload file image*/
            String fileName;
            Part filePart = request.getPart("imageWork"); // Retrieves <input type="file" name="file">
            if(filePart.getSize()>0) {
                fileName = Paths.get(filePart.getName()).getFileName().toString(); // MSIE fix.
                InputStream fileContent = filePart.getInputStream();
                String[] splitted = fileName.split("\\.");
                String fileExt = splitted[1];

                BufferedImage pictureFile = ImageIO.read(fileContent);
                ImageIO.write(pictureFile, fileExt, new File(Configuration.GLOBAL_UPLOAD_IMAGES_PATH + fileName));
            }else fileName = request.getParameter("imageWorkUrl");

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AudiovisionWork audiovisionWork = commonSetToAW(request, daoFactory, new AudiovisionWork());

            audiovisionWork.setWorkId(Long.parseLong(request.getParameter("workId")));
            audiovisionWork.setOriginalTitle(request.getParameter("title"));
            audiovisionWork.setDuration(Integer.parseInt(request.getParameter("duration")));
            audiovisionWork.setRating(Float.parseFloat(request.getParameter("rating")));
            audiovisionWork.setType(request.getParameter("type"));
            audiovisionWork.setYearProdction(request.getParameter("yearProduction"));
            audiovisionWork.setStudioProduction(request.getParameter("studioProduction"));
            audiovisionWork.setImageWork(fileName);
            audiovisionWork.setPlot(request.getParameter("plot"));
            audiovisionWork.setTrailerUrl(request.getParameter("trailerUrl"));

            AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();

            try {
                audiovisionWorkDAO.update(audiovisionWork);
            } catch (DuplicatedObjectException e) {
                applicationMessage = e.getMessage();
                logger.log(Level.INFO, e.getMessage());
            }

            commonAWManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }

    }

    public static void deleteAW(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String workId = request.getParameter("Id");

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            AudiovisionWork audiovisionWork = new AudiovisionWork();
            audiovisionWork.setWorkId(Long.parseLong(workId));

            AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();
            audiovisionWorkDAO.delete(audiovisionWork);

            commonAWManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/commonView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    private static AudiovisionWork commonSetToAW(HttpServletRequest request, DAOFactory daoFactory, AudiovisionWork audiovisionWork) {

        String[] genresName = request.getParameterValues("genres");
        String[] actorsName = request.getParameterValues("actors");
        String[] directorsName = request.getParameterValues("directors");

        GenreDAO genreDAO = daoFactory.getGenreDAO();
        List<Genre> genreList = genreDAO.extractGenres();
        List<Genre> myGenreList = new ArrayList<>();

        for (String s : genresName) {
            if (!s.equals("")) {
                int index = indexOfGenreContainedInList(genreList, s);
                if (index == -1)
                    try {
                        myGenreList.add(genreDAO.insert(s));

                    } catch (Exception ignored) {
                    }
                else myGenreList.add(genreList.get(index));

            }
        }

        ActorDAO actorDAO = daoFactory.getActorDAO();
        List<Actor> actorList = actorDAO.extractActors();
        List<Actor> myActorList = new ArrayList<>();

        for (String s : actorsName) {
            if (!s.equals("")) {
                int index = indexOfActorContainedInList(actorList, s);
                if (index == -1)
                    try {
                        myActorList.add(actorDAO.insert(s));
                    } catch (Exception ignored) {
                    }
                else myActorList.add(actorList.get(index));
            }
        }
        DirectorDAO directorDAO = daoFactory.getDirectorDAO();
        List<Director> directorList = directorDAO.extractDirectors();
        List<Director> myDirectorList = new ArrayList<>();

        for (String s : directorsName) {
            if (!s.equals("")) {
                int index = indexOfDirectorContainedInList(directorList, s);
                if (index == -1)
                    try {
                        myDirectorList.add(directorDAO.insert(s));
                    } catch (Exception ignored) {
                    }
                else myDirectorList.add(directorList.get(index));
            }
        }

        Genre[] genres = null;
        if (myGenreList.size() > 0) {
            genres = new Genre[myGenreList.size()];
            genres = myGenreList.toArray(genres);
        }
        Actor[] actors = null;
        if (myActorList.size() > 0) {
            actors = new Actor[myActorList.size()];
            actors = myActorList.toArray(actors);
        }
        Director[] directors = null;
        if (myDirectorList.size() > 0) {
            directors = new Director[myDirectorList.size()];
            directors = myDirectorList.toArray(directors);
        }

        audiovisionWork.setGenres(genres);
        audiovisionWork.setActors(actors);
        audiovisionWork.setDirectors(directors);

        return audiovisionWork;
    }

    private static void commonAWManView(HttpServletRequest request, DAOFactory daoFactory) {

        List<AudiovisionWork> audiovisionWorks;
        final int multipleView;
        boolean isThisLastPage = false;

        String orderFilter = request.getParameter("orderFilter");
        String descendantString = request.getParameter("descendant");
        String thisPageString = request.getParameter("thisPage");
        String searchString = request.getParameter("searchString");

        if (orderFilter == null) {
            orderFilter = "original_title";
        }

        boolean descendant;
        if (descendantString != null) {
            descendant = descendantString.equals("true");
        } else descendant = false;

        int thisPage;
        if (thisPageString == null) {
            thisPage = 1;
        } else thisPage = Integer.parseInt(thisPageString);

        AdminConfigurationDAO adminConfigurationDAO = daoFactory.getAdminConfigurationDAO();
        multipleView = Integer.parseInt(adminConfigurationDAO.read(Configuration.ADMIN_MULTIPLE_VIEW_CONFIG_NAME));

        AudiovisionWorkDAO audiovisionWorkDAO = daoFactory.getAudiovisionWorkDAO();
        audiovisionWorks = audiovisionWorkDAO.findByFilters(null, null, null, orderFilter, descendant, thisPage, multipleView, searchString);

        if (audiovisionWorks.size() < multipleView + 1)
            isThisLastPage = true;
        else audiovisionWorks.remove(audiovisionWorks.size() - 1);

        request.setAttribute("orderFilter", orderFilter);
        request.setAttribute("descendant", descendant);
        request.setAttribute("searchString", searchString);

        request.setAttribute("thisPage", thisPage);
        request.setAttribute("isThisLastPage", isThisLastPage);
        request.setAttribute("multipleView", multipleView);

        request.setAttribute("works", audiovisionWorks);
    }

    private static int indexOfGenreContainedInList(List<Genre> genres, String s) {
        for (int i = 0; i < genres.size(); i++) {
            if (genres.get(i).getGenreName().equals(s))
                return i;
        }
        return -1;
    }

    private static int indexOfActorContainedInList(List<Actor> actors, String s) {
        for (int i = 0; i < actors.size(); i++) {
            if (actors.get(i).getActorName().equals(s))
                return i;
        }
        return -1;
    }

    private static int indexOfDirectorContainedInList(List<Director> directors, String s) {
        for (int i = 0; i < directors.size(); i++) {
            if (directors.get(i).getDirectorName().equals(s))
                return i;
        }
        return -1;
    }

    /* ------ GENRE ----- */

    public static void genreManagementView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            commonGenreManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/genreManagementt/genreView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void insertGenreView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String thisPageString = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");
            String isThisLastPage = request.getParameter("isThisLastPage");
            String multipleView = request.getParameter("multipleView");

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/genreManagementt/insModGenreView");

            request.setAttribute("searchString", searchString);
            request.setAttribute("thisPage", thisPageString);
            request.setAttribute("isThisLastPage", isThisLastPage);
            request.setAttribute("multipleView", multipleView);


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void modifyGenreView(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String thisPageString = request.getParameter("thisPage");
            String searchString = request.getParameter("searchString");
            String isThisLastPage = request.getParameter("isThisLastPage");
            String multipleView = request.getParameter("multipleView");

            String genreId = request.getParameter("id");
            String genreName = request.getParameter("genreName");

            Genre genre = new Genre();
            genre.setGenreId(Long.parseLong(genreId));
            genre.setGenreName(genreName);

            request.setAttribute("genre", genre);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/genreManagementt/insModGenreView");

            request.setAttribute("searchString", searchString);
            request.setAttribute("thisPage", thisPageString);
            request.setAttribute("isThisLastPage", isThisLastPage);
            request.setAttribute("multipleView", multipleView);


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            throw new RuntimeException(e);
        }
    }

    public static void insertGenre(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String genreName = request.getParameter("genreName");


            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            GenreDAO genreDAO = daoFactory.getGenreDAO();


            try {
                genreDAO.insert(genreName);

            } catch (DuplicatedObjectException e) {
                applicationMessage = e.getMessage();
                logger.log(Level.INFO, e.getMessage());
            }

            commonGenreManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/genreManagementt/genreView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    public static void modifyGenre(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();

        try {
            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            String genreName = request.getParameter("genreName");
            String genreId = request.getParameter("genreId");

            Genre genre = new Genre();
            genre.setGenreName(genreName);
            genre.setGenreId(Long.parseLong(genreId));

            GenreDAO genreDAO = daoFactory.getGenreDAO();
            try {
                genreDAO.update(genre);
            } catch (DuplicatedObjectException e) {
                applicationMessage = e.getMessage();
                logger.log(Level.INFO, e.getMessage());
            }

            commonGenreManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("applicationMessage", applicationMessage);
            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/genreManagementt/genreView");


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }


    }

    public static void deleteGenre(HttpServletRequest request, HttpServletResponse response) {

        SessionDAOFactory sessionDAOFactory;
        DAOFactory daoFactory = null;
        LoggedAdmin loggedAdmin;
        String applicationMessage = null;

        Logger logger = LogService.getApplicationLogger();
        try {

            sessionDAOFactory = SessionDAOFactory.getSessionDAOFactory(Configuration.SESSION_IMPL);
            sessionDAOFactory.initSession(request, response);

            LoggedAdminDAO loggedAdminDAO = sessionDAOFactory.getLoggedAdminDAO();
            loggedAdmin = loggedAdminDAO.find();

            String genreId = request.getParameter("id");

            daoFactory = DAOFactory.getDAOFactory(Configuration.DAO_IMPL);
            daoFactory.beginTransaction();

            Genre genre = new Genre();
            genre.setGenreId(Long.parseLong(genreId));

            GenreDAO genreDAO = daoFactory.getGenreDAO();
            genreDAO.delete(genre);

            commonGenreManView(request, daoFactory);

            daoFactory.commitTransaction();

            request.setAttribute("loggedOn", loggedAdmin != null);
            request.setAttribute("loggedAdmin", loggedAdmin);
            request.setAttribute("viewUrl", "adminCatalogManagement/genreManagementt/genreView");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Controller Error", e);
            try {
                if (daoFactory != null) {
                    daoFactory.rollbackTransaction();
                }
            } catch (Throwable t) {
            }
            throw new RuntimeException(e);

        } finally {
            try {
                if (daoFactory != null) {
                    daoFactory.closeTransaction();
                }
            } catch (Throwable t) {
            }
        }
    }

    private static void commonGenreManView(HttpServletRequest request, DAOFactory daoFactory) {

        List<Genre> genres;
        final int multipleView = 30;
        boolean isThisLastPage = false;


        String thisPageString = request.getParameter("thisPage");
        String searchString = request.getParameter("searchString");

        int thisPage;
        if (thisPageString == null) {
            thisPage = 1;
        } else thisPage = Integer.parseInt(thisPageString);

        GenreDAO genreDAO = daoFactory.getGenreDAO();
        genres = genreDAO.findBySearchString(searchString, thisPage, multipleView);

        if (genres.size() < multipleView + 1)
            isThisLastPage = true;
        else genres.remove(genres.size() - 1);


        request.setAttribute("searchString", searchString);

        request.setAttribute("thisPage", thisPage);
        request.setAttribute("isThisLastPage", isThisLastPage);
        request.setAttribute("multipleView", multipleView);

        request.setAttribute("genres", genres);
    }
}








